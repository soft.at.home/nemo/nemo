INSTALL ?= install

INSTALL?=install

MIBS=alias.odl atm.odl bridge.odl copy.odl dhcp.odl dhcpv6.odl dhcp-api.odl dsl.odl dslbonding.odl dslite.odl eth.odl gre.odl nat.odl dslline.odl \
	netdev-api.odl netdev.odl penable.odl ppp.odl ptm.odl switch.odl vlan.odl 6rd.odl wan.odl statmon.odl

ifeq ($(CONFIG_SAH_SERVICES_NEMO_INSTALL_GPON_MIB),y)
MIBS += gpon.odl
endif
	
ifneq ($(CONFIG_SAH_SERVICES_NEMO_VPN),)
MIBS+= vpnserver.odl vpnclient.odl
endif

ifneq ($(CONFIG_SAH_SERVICES_NEMO_SFP),)
MIBS+= sfp.odl
endif

compile: 
	$(MAKE) -C modules all
	$(MAKE) -C clients all

clean:
	$(MAKE) -C modules clean
	$(MAKE) -C clients clean

install: 
	$(INSTALL) -d $(D)/usr/lib/nemo/mibs/
	$(INSTALL) -D -m 755 modules/built-in.so $(D)/usr/lib/nemo/modules/built-in.so
	$(INSTALL) -m 755 modules/dhcp.so $(D)/usr/lib/nemo/modules/
	$(INSTALL) -m 755 modules/ppp.so $(D)/usr/lib/nemo/modules/
	$(INSTALL) -m 755 modules/xdsl.so $(D)/usr/lib/nemo/modules/
	$(INSTALL) -D -m 755 clients/built-in.so $(D)/usr/lib/nemo/clients/built-in.so
	$(foreach odl,$(MIBS),$(INSTALL) -m 644 odl/$(odl) $(D)/usr/lib/nemo/mibs/;)


.PHONY: compile clean install

