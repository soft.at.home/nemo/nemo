/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * The DSL bonding MIB is available on all NeMo Intfs that have the "dslbonding"-flag set. If there is an
 * enabled Intf with the "dslbonding"-flag set, an adsl connection will be set up and several status
 * parameters of that connection will be exposed in the data model.
 */
object NeMo {
	object extends MIB(0,"dslbonding") {
		string Flag = "dslbonding";
		object DataModel {

			/**
			 * Timestamp of the last operational state change, measured in seconds
			 * since power on.
			 * @version v7.1
			 */
			read-only uint32 LastChangeTime;

			/**
			 * The accumulated time in seconds since the Intf entered its current
			 * operational state.
			 * @version v7.1
			 */
			volatile read-only uint32 LastChange;

			/**
			 * Status of the DSL bonding link.
			 * Up: (ADSL or VDSL)Bonding is up and running,
			 * Disabled: Interface is disabled.
			 * NoSignal: no signal is detected on the lines. (not supported)
			 * Initializing, EstablishingLink, LowerLayerDown, Unknown, NotPresent: states are defined for TR181 compatibility.
			 * Error: error occured during activation.
			 * @version v7.1
			 */
			string BondingStatus {
				constraint enum [ "Up", "Initializing", "EstablishingLink", "NoSignal", "Disabled", "Error", "LowerLayerDown", "Down", "Unknown", "Dormant", "NotPresent"];
				default "Disabled";
			}

			/**
			 * The current physical layer aggregate data rate (expressed in Kbps) of
			 * the upstream DSL connection.
			 * @version v7.1
			 */
			uint32 UpstreamRate;

			/**
			 * The current physical layer aggregate data rate (expressed in Kbps) of
			 * the downstream DSL connection.
			 * @version v7.1
			 */
			uint32 DownstreamRate;

			/**
			 * Reason for Failure, currently not supported.
			 * @version v7.1
			 */
			string FailureReasons;

			/**
			 * Packet loss in upstream direction, currently not supported.
			 * @version v7.1
			 */
			uint32 UpstreamPacketLoss;

			/**
			 * Packet loss in downstream direction, currently not supported.
			 * @version v7.1
			 */
			uint32 DownstreamPacketLoss;

			/**
			 * Parameter is currently not supported.
			 * @version v7.1
			 */
			uint32 UpstreamDifferentialDelay;

			/**
			 * Parameter is currently not supported.
			 * @version v7.1
			 */
			uint32 DownstreamDifferentialDelay;

			/**
			 * Number of secs where we saw erors in. Parameter is currently not supported.
			 * @version v7.1
			 */
			uint32 ErroredSeconds;

			/**
			 * Number of secs where we saw sever errors in. Parameter is currently not supported.
			 * @version v7.1
			 */
			uint32 SeverelyErroredSeconds;

			/**
			 * Number of unavailable seconds in. Parameter is currently not supported.
			 * @version v7.1
			 */
			uint32 UnavailableSeconds;
		}
	}
}

/** @location sah_services_nemo /mibs/dslbonding.odl */
