/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * The DHCP API MIB is available on all NeMo Intfs.
 */

object NeMo { 
	object extends MIB(0,"dhcp-api") {
		string Flag = "";
		object DataModel {
			/**
			 * Finds and parses the option value given by 'type' and 'tag' of any bound
			 * DHCP client Intf occurring in the traverse tree. This function has "query
			 * support", meaning that its name can be passed successfully to the openQuery
			 * function of NeMo. This way DHCP options become eventable. 
			 *
			 * @param type The type of option to be queried. Enumeration of:
			 * 	req: consult the ReqOption table of DHCPv4 clients.
			 * 	sent: consult the SentOption table of DHCPv4 clients.
			 * 	req6: consult the ReceivedOption table of DHCPv6 clients.
			 * 	sent6: consult the SentOption table of DHCPv6 clients.
			 * @param tag The option tag 
			 * @param traverse Traverse mode to build the traverse tree. 
			 * @return A structured variant holding the option value. Its exact structure
			 * 	depends on the type and tag arguments: each option has its particular
			 * 	 encoding scheme. Example: if the type equals "req" and the tag equals
			 * 	 3 (IP Routers Option), the return value is a variant list. Each item
			 * 	 in the list is a string representing an IPv4-Address. Supported
			 * 	 options for DHCPv4 are: 1-77, 90, 120, 121, 124 and 125 and for
			 * 	 DHCPv6: 1-9, 11-34, 36-48, 51, 58 and 64. If an unsupported option
			 * 	 tag is given as input argument, a default encoding scheme is applied.
			 * 	 This encoding scheme returns an ASCII-string if the option value
			 * 	 contains valid ASCII-characters only or the raw hexbinary prefixed
			 * 	 with "0x" otherwise.
			 */
			variant getDHCPOption(string type, uint8 tag, string traverse) {
			    acl {
			        world none;
			    }
			}
		}
	}
}


/** @location sah_services_nemo /mibs/dhcp-api.odl */
