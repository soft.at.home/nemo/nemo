/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <unistd.h>
#include <errno.h>
#include <net/if.h>
#include <netinet/in.h>
#include <linux/if_bridge.h>
#include <linux/sockios.h>
#include <asm/param.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>

#include <nemo/client.h>

#define ME "bridge"
#define SYSFS_CLASS_NET_PATH "/sys/class/net/"
#define SYSFS_PATH_MAX_LEN	256
#define SYSFS_BRIDGE_FORWARD_DELAY_FILENAME "forward_delay"
#define SYSFS_BRIDGE_HELLO_TIME_FILENAME "hello_time"
#define SYSFS_BRIDGE_MAX_AGE_FILENAME "max_age"
#define SYSFS_BRIDGE_AGEING_TIME_FILENAME "ageing_time"
#define SYSFS_BRIDGE_STP_STATE_FILENAME "stp_state"
#define SYSFS_BRIDGE_PRIORITY_FILENAME "priority"
#define USER_HZ sysconf(_SC_CLK_TCK)

typedef struct _bridge {
	llist_iterator_t it;
	char name[IF_NAMESIZE];
	bool zombie;
	llist_t ports;
	nemo_query_t *portquery;
	request_t *updaterequest;
	unsigned long ageing;
	bool STPenable;
	uint16_t priority;
	uint32_t maxAge;
	uint32_t forwardDelay;
	uint32_t helloTime;
} bridge_t;

typedef struct _port {
	llist_iterator_t it;
	bridge_t *bridge;
	char name[IF_NAMESIZE];
	int index;
	bool zombie;
} port_t;

typedef enum { IOCTL_SUCCESS, IOCTL_BUSY, IOCTL_INVALID_ARG, IOCTL_ERROR_OTHER } port_ioctl_rv;

static nemo_query_t *bridgequery = NULL;

static llist_t bridges = {NULL, NULL};

static int bridgefd = -1;

static bool bridge_is_up(const char *name) {
	bool is_up = false;
	variant_t *var = nemo_getFirstParameter(name, "NetDevFlags", NULL, "this");
	const char *flag = variant_da_char(var);
	string_list_t list;
	string_list_initialize(&list);
	string_list_splitChar(&list, flag, " ", strlist_skip_empty_parts, string_case_sensitive);
	is_up = string_list_containsChar(&list, "up");
	SAH_TRACEZ_NOTICE(ME, "NetDevFlags [ %s ] - %s is %s", flag, name, is_up?"UP":"DOWN");
	string_list_cleanup(&list);
	variant_cleanup(var);
	free(var);
	return is_up;
}

static port_ioctl_rv port_ioctl(port_t *port, unsigned long command) {
	struct ifreq ifreq;
	strncpy(ifreq.ifr_name, port->bridge->name, IF_NAMESIZE);
	ifreq.ifr_name[IF_NAMESIZE - 1] = '\0';
	ifreq.ifr_ifindex = port->index;
	port_ioctl_rv rv;
	int ret = 0;

#ifdef SIOCBRADDIF
	switch (command) {
		case BRCTL_ADD_IF:
			SAH_TRACEZ_INFO(ME, "Add port \"%s\" to bridge \"%s\"", port->name, port->bridge->name);
 			ret = ioctl(bridgefd, SIOCBRADDIF, &ifreq);
			break;
		case BRCTL_DEL_IF:
			SAH_TRACEZ_INFO(ME, "Remove port \"%s\" from bridge \"%s\"", port->name, port->bridge->name);
 			ret = ioctl(bridgefd, SIOCBRDELIF, &ifreq);
			break;
	}
#else
	unsigned long arg[4] = {command, port->index, 0, 0};
	ifreq.ifr_data = (char *)arg;
	switch (command) {
		case BRCTL_ADD_IF : SAH_TRACEZ_INFO(ME, "Add port \"%s\" to bridge \"%s\"", port->name, port->bridge->name); break;
		case BRCTL_DEL_IF : SAH_TRACEZ_INFO(ME, "Remove port \"%s\" from bridge \"%s\"", port->name, port->bridge->name); break;
	}
	ret = ioctl(bridgefd, SIOCDEVPRIVATE, &ifreq);
#endif
	if (ret == 0) {
		rv = IOCTL_SUCCESS;
	}
	else if (errno == EBUSY) {
		rv = IOCTL_BUSY;
	}
	else if (errno == EINVAL) {
		rv = IOCTL_INVALID_ARG;
	}
	else {
		rv = IOCTL_ERROR_OTHER;
	}
	if (ret < 0) {
		switch (command) {
			case BRCTL_ADD_IF:
				SAH_TRACE_ERROR("Add port \"%s\" to bridge \"%s\" failed: %d(%s)",
					port->name, port->bridge->name, errno, strerror(errno));
				break;
			case BRCTL_DEL_IF:
				SAH_TRACE_ERROR("Remove port \"%s\" from bridge \"%s\" failed: %d(%s)",
					port->name, port->bridge->name, errno, strerror(errno));
				break;
		}
	}
	return rv;
}

static port_t *port_create(bridge_t *bridge, const char *name, int index) {
	port_t *port = calloc(1, sizeof(port_t));
	port->bridge = bridge;
	snprintf(port->name, IF_NAMESIZE, "%s", name);
	port->index = index;
	llist_append(&bridge->ports, &port->it);
	port_ioctl_rv rv = port_ioctl(port, BRCTL_ADD_IF);
	//IOCTL_BUSY=16(Device or resource busy) interface is already in the bridge
	if (rv == IOCTL_SUCCESS || rv == IOCTL_BUSY) {
		nemo_setFlag(port->name, "inbridge", NULL, "this"); // inbridge
	}
	// This is a hack to enforce a refresh of the LLAddress of the bridge which might have changed,
	// but the kernel didn't send an update.
	char path[64];
	snprintf(path, 64, "NeMo.Intf.%s", bridge->name);
	request_t *request = request_create_executeFunction(path, "refreshNetDev", request_common_path_key_notation);
	if (!pcb_sendRequest(nemo_client_pcb(), nemo_client_peer(), request)) {
		SAH_TRACE_ERROR("Failed to send request");
	}
	request_destroy(request);
	return port;
}

static port_t *port_find(bridge_t *bridge, const char *name) {
	port_t *port;
	for (port = (port_t *)llist_first(&bridge->ports); port; port = (port_t *)llist_iterator_next(&port->it))
		if (!strcmp(port->name, name))
			return port;
	return NULL;
}

static void port_destroy(port_t *port) {
	if (!port)
		return;
	port_ioctl_rv rv = port_ioctl(port, BRCTL_DEL_IF);
	//IOCTL_INVALID_ARG=22(Invalid argument) interface may have been already removed
	if (rv == IOCTL_SUCCESS || rv == IOCTL_INVALID_ARG) {
		nemo_clearFlag(port->name, "inbridge", NULL, "this");
	}
	llist_iterator_take(&port->it);
	free(port);
}

static void bridge_portHandler(const variant_t *result, void *userdata) {
	bridge_t *bridge = (bridge_t *)userdata;
	variant_map_t *intfs = result?variant_da_map(result):NULL;
	variant_map_iterator_t *it;
	port_t *port, *portnext;
	for (port = (port_t *)llist_first(&bridge->ports); port; port = (port_t *)llist_iterator_next(&port->it))
		port->zombie = true;
	if (intfs) variant_map_for_each(it, intfs) {
		const char *name = variant_map_iterator_key(it);
		int index = (int)variant_int32(variant_map_iterator_data(it));
		if (!name)
			continue;
		port = port_find(bridge, name);
		if (port && port->index != index) {
			port_destroy(port);
			port = NULL;
		}
		if (!port && index > 0)
			port = port_create(bridge, name, index);
		if (port)
			port->zombie = false;
	}
	for (port = (port_t *)llist_first(&bridge->ports); port; port = portnext) {
		portnext = (port_t *)llist_iterator_next(&port->it);
		if (port->zombie)
			port_destroy(port);
	}
}

static void bridge_brioctl(bridge_t *bridge, unsigned long command) {
#ifdef SIOCBRADDBR
	int ret = 0;
	switch (command) {
		case BRCTL_ADD_BRIDGE:
			SAH_TRACEZ_INFO(ME, "Create bridge \"%s\"", bridge->name);
			ret = ioctl(bridgefd, SIOCBRADDBR, bridge->name);
			break;
		case BRCTL_DEL_BRIDGE:
			SAH_TRACEZ_INFO(ME, "Destroy bridge \"%s\"", bridge->name);
			ret = ioctl(bridgefd, SIOCBRDELBR, bridge->name);
			break;
	}
	if (ret < 0) {
		switch (command) {
			case BRCTL_ADD_BRIDGE: SAH_TRACE_ERROR("Create bridge \"%s\" failed: %d (%s)",
					bridge->name, errno, strerror(errno)); break;
			case BRCTL_DEL_BRIDGE: SAH_TRACE_ERROR("Destroy bridge \"%s\" failed: %d (%s)",
					bridge->name, errno, strerror(errno)); break;
		}
	}
#else
	unsigned long arg[3] = {command, (unsigned long)bridge->name, 0};
	switch (command) {
		case BRCTL_ADD_BRIDGE: SAH_TRACEZ_INFO(ME, "Create bridge \"%s\"", bridge->name); break;
		case BRCTL_DEL_BRIDGE: SAH_TRACEZ_INFO(ME, "Destroy bridge \"%s\"", bridge->name); break;
	}
	if (ioctl(bridgefd, SIOCGIFBR, arg) == -1) {
		switch (command) {
			case BRCTL_ADD_BRIDGE: SAH_TRACE_ERROR("Create bridge \"%s\" failed: %d (%s)",
					bridge->name, errno, strerror(errno)); break;
			case BRCTL_DEL_BRIDGE: SAH_TRACE_ERROR("Destroy bridge \"%s\" failed: %d (%s)",
					bridge->name, errno, strerror(errno)); break;
		}
	}
#endif
}

static inline unsigned long __tv_to_jiffies(const struct timeval *tv)
{
	unsigned long long jif;
	jif = 1000000ULL * tv->tv_sec + tv->tv_usec;
	return jif/10000;
}

static int bridge_set_sysfs(const char *path, unsigned long value)
{
	int fd, ret = 0, cc;
	char buf[32];

	fd = open(path, O_WRONLY);
	if (fd < 0)
		return -1;

	cc = snprintf(buf, sizeof(buf), "%lu\n", value);
	if (write(fd, buf, cc) < 0)
		ret = -1;
	close(fd);

	return ret;
}

static int bridge_set(const char *bridge, const char *name, unsigned long value)
{
	char path[SYSFS_PATH_MAX_LEN];
	int ret;

	snprintf(path, SYSFS_PATH_MAX_LEN, SYSFS_CLASS_NET_PATH "%s/bridge/%s",
		 bridge, name);

	ret = bridge_set_sysfs(path, value);
	return ret < 0 ? errno : 0;
}

int bridge_set_bridge_forward_delay(const char *br, struct timeval *tv)
{
	return bridge_set(br, SYSFS_BRIDGE_FORWARD_DELAY_FILENAME, __tv_to_jiffies(tv));
}

int bridge_set_bridge_hello_time(const char *br, struct timeval *tv)
{
	return bridge_set(br, SYSFS_BRIDGE_HELLO_TIME_FILENAME, __tv_to_jiffies(tv));
}

int bridge_set_bridge_max_age(const char *br, struct timeval *tv)
{
	return bridge_set(br, SYSFS_BRIDGE_MAX_AGE_FILENAME, __tv_to_jiffies(tv));
}

int bridge_set_ageing_time(const char *br, struct timeval *tv)
{
	return bridge_set(br, SYSFS_BRIDGE_AGEING_TIME_FILENAME, __tv_to_jiffies(tv));
}

int bridge_set_stp_state(const char *br, int stp_state)
{
	return bridge_set(br, SYSFS_BRIDGE_STP_STATE_FILENAME, stp_state);
}

int bridge_set_bridge_priority(const char *br, int bridge_priority)
{
	return bridge_set(br, SYSFS_BRIDGE_PRIORITY_FILENAME, bridge_priority);
}

static int ulongToTimeval(struct timeval *tv, unsigned long ulValueSeconds)
{
	tv->tv_sec = ulValueSeconds;
	tv->tv_usec = 0;
	return 0;
}

static void bridge_devioctl(bridge_t *bridge, unsigned long command, unsigned long value) {
	unsigned long arg[4] = {command, value, 0, 0};
	struct ifreq ifreq;
	strncpy(ifreq.ifr_name, bridge->name, IF_NAMESIZE);
	ifreq.ifr_name[IF_NAMESIZE - 1] = '\0';
	ifreq.ifr_data = (char *)arg;
	int iRet = 0;
	struct timeval sTv;
	switch (command) {
		case BRCTL_SET_AGEING_TIME: SAH_TRACEZ_INFO(ME, "Set ageing time of bridge \"%s\" to %lu", bridge->name, value); break;
		case BRCTL_SET_BRIDGE_STP_STATE: SAH_TRACEZ_INFO(ME, "Set stp state of bridge \"%s\" to %lu", bridge->name, value); break;
	}

#ifdef SIOCBRADDIF
		SAH_TRACEZ_INFO(ME, "Trying with sysfs. value: %ld", value);
		
		ulongToTimeval(&sTv, value / USER_HZ); //Undo multiplication by USER_HZ
		switch (command)
		{
			case BRCTL_SET_AGEING_TIME:
				iRet = bridge_set_ageing_time(bridge->name, &sTv);
			break;
			case BRCTL_SET_BRIDGE_STP_STATE:
				iRet = bridge_set_stp_state(bridge->name, value);
			break;
			case BRCTL_SET_BRIDGE_PRIORITY:
				iRet = bridge_set_bridge_priority(bridge->name, value);
			break;
			case BRCTL_SET_BRIDGE_MAX_AGE:
				iRet = bridge_set_bridge_max_age(bridge->name, &sTv);
			break;
			case BRCTL_SET_BRIDGE_FORWARD_DELAY:
				iRet = bridge_set_bridge_forward_delay(bridge->name, &sTv);
			break;
			case BRCTL_SET_BRIDGE_HELLO_TIME:
				iRet = bridge_set_bridge_hello_time(bridge->name, &sTv);
			break;
		}
		if (iRet != 0)
		{
			SAH_TRACE_ERROR("Set state of parameter %lu to bridge \"%s\" to %lu failed: %d(%s)",
				command, bridge->name, value, errno, strerror(errno));
		}
#else
	if (ioctl(bridgefd, SIOCDEVPRIVATE, &ifreq) == -1) {
		switch (command) {
			case BRCTL_SET_AGEING_TIME:
				SAH_TRACE_ERROR("Set ageing time of bridge \"%s\" to %lu failed: %d(%s)",
					bridge->name, value, errno, strerror(errno));
				break;
			case BRCTL_SET_BRIDGE_STP_STATE:
				SAH_TRACE_ERROR("Set stp state of bridge \"%s\" to %lu failed: %d(%s)",
					bridge->name, value, errno, strerror(errno));
				break;
		}
	}
#endif
}

static bool bridge_replyHandler(request_t *request, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)pcb; (void)from;
	bridge_t *bridge = (bridge_t *)userdata;
	reply_t *reply = request_reply(request);
	if (!reply)
		return true;
	reply_item_t *item;
	for(item = reply_firstItem(reply); item; item = reply_nextItem(item)) {
		if (reply_item_type(item) != reply_type_object)
			continue;
		object_t *object = reply_item_object(item);

		unsigned long ageing = object_parameterUInt32Value(object, "Ageing");
		if (ageing && ageing != bridge->ageing) {
			bridge_devioctl(bridge, BRCTL_SET_AGEING_TIME, USER_HZ * ageing);
			bridge->ageing = ageing;
		}

		bool STPenable = object_parameterBoolValue(object, "STPEnable");
		if (STPenable != bridge->STPenable) {
			bridge_devioctl(bridge, BRCTL_SET_BRIDGE_STP_STATE, STPenable);
			bridge->STPenable = STPenable;
		}

		uint16_t priority = object_parameterUInt16Value(object, "Priority");
		if (priority && priority != bridge->priority) {
			bridge_devioctl(bridge, BRCTL_SET_BRIDGE_PRIORITY, priority);
			bridge->priority = priority;
		}

		uint32_t maxAge = object_parameterUInt32Value(object, "MaxAge");
		if (maxAge && maxAge != bridge->maxAge) {
			bridge_devioctl(bridge, BRCTL_SET_BRIDGE_MAX_AGE, USER_HZ * maxAge);
			bridge->maxAge = maxAge;
		}

		uint32_t forwardDelay = object_parameterUInt32Value(object, "ForwardDelay");
		if (forwardDelay && forwardDelay != bridge->forwardDelay) {
			bridge_devioctl(bridge, BRCTL_SET_BRIDGE_FORWARD_DELAY, USER_HZ * forwardDelay);
			bridge->forwardDelay = forwardDelay;
		}

		uint32_t helloTime = object_parameterUInt32Value(object, "HelloTime");
		if (helloTime && helloTime != bridge->helloTime) {
			bridge_devioctl(bridge, BRCTL_SET_BRIDGE_HELLO_TIME, USER_HZ * helloTime);
			bridge->helloTime = helloTime;
		}
	}
	return true;
}


static bridge_t *bridge_create(const char *name) {
	bridge_t *bridge = calloc(1, sizeof(bridge_t));
	snprintf(bridge->name, IF_NAMESIZE, "%s", name);
	llist_append(&bridges, &bridge->it);
	bool isup = bridge_is_up(name);
	if (isup) {
		nemo_clearNetDevFlag(name, "up", "this");
	}
	bridge_brioctl(bridge, BRCTL_ADD_BRIDGE);
	bridge->portquery = nemo_openQuery_getParameters(bridge->name, ME,
			"NetDevIndex", "netdev-bound", "one level down", bridge_portHandler, bridge);
	char path[64];
	snprintf(path, 64, "NeMo.Intf.%s", name);
	bridge->updaterequest = request_create_getObject(path, 0, request_common_path_key_notation | request_notify_values_changed | request_no_object_caching | request_getObject_parameters);
	request_setReplyHandler(bridge->updaterequest, bridge_replyHandler);
	request_setData(bridge->updaterequest, bridge);
	if (!pcb_sendRequest(nemo_client_pcb(), nemo_client_peer(), bridge->updaterequest)) {
		SAH_TRACE_ERROR("Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
	}
	if (isup) {
		nemo_setNetDevFlag(name, "up", "this");
	}
	nemo_setFlag(name, "up", NULL, "this");
	return bridge;
}

static bridge_t *bridge_find(const char *name) {
	bridge_t *bridge;
	for (bridge = (bridge_t *)llist_first(&bridges); bridge; bridge = (bridge_t *)llist_iterator_next(&bridge->it))
		if (!strcmp(bridge->name, name))
			return bridge;
	return NULL;
}

static void bridge_setDown(bridge_t *bridge) {
	nemo_clearFlag(bridge->name, "up", NULL, "this");
	struct ifreq ifreq;
	strncpy(ifreq.ifr_name, bridge->name, IF_NAMESIZE);
	ifreq.ifr_name[IF_NAMESIZE - 1] = '\0';
	SAH_TRACEZ_INFO(ME, "Set bridge \"%s\" down", bridge->name);
	if (ioctl(bridgefd, SIOCGIFFLAGS, &ifreq) == -1) {
		SAH_TRACE_ERROR("Get flags of bridge \"%s\" failed: %d (%s)", bridge->name, errno, strerror(errno));
		return;
	}
	ifreq.ifr_flags &= ~IFF_UP;
	if (ioctl(bridgefd, SIOCSIFFLAGS, &ifreq) == -1) {
		SAH_TRACE_ERROR("Set flags of bridge \"%s\" failed: %d (%s)", bridge->name, errno, strerror(errno));
		return;
	}
}

static void bridge_destroy(bridge_t *bridge) {
	if (!bridge)
		return;
	request_destroy(bridge->updaterequest);
	nemo_closeQuery(bridge->portquery);
	bridge_portHandler(NULL, bridge);
	bridge_setDown(bridge);
	bridge_brioctl(bridge, BRCTL_DEL_BRIDGE);
	llist_iterator_take(&bridge->it);
	free(bridge);
}

static void bridge_rootHandler(const variant_t *result, void *userdata) {
	(void)userdata;
	variant_list_t *intfs = result?variant_da_list(result):NULL;
	variant_list_iterator_t *it;
	bridge_t *bridge, *bridgenext;
	for (bridge = (bridge_t *)llist_first(&bridges); bridge; bridge = (bridge_t *)llist_iterator_next(&bridge->it))
		bridge->zombie = true;
	if (intfs) variant_list_for_each(it, intfs) {
		const char *name = string_buffer(variant_da_string(variant_list_iterator_data(it)));
		if (!name)
			continue;
		bridge = bridge_find(name);
		if (!bridge)
			bridge = bridge_create(name);
		bridge->zombie = false;
	}
	for (bridge = (bridge_t *)llist_first(&bridges); bridge; bridge = bridgenext) {
		bridgenext = (bridge_t *)llist_iterator_next(&bridge->it);
		if (bridge->zombie)
			bridge_destroy(bridge);
	}
}

static bool bridge_start(argument_value_list_t *args __attribute__((unused))) {
	bridgefd = socket(AF_INET, SOCK_STREAM, 0);
	nemo_client_initialize(mtk_getPcb(), mtk_getSysbus());
	bridgequery = nemo_openQuery_getIntfs("lo", ME, "bridge && enabled", "all", bridge_rootHandler, NULL);
	return true;
}

static void bridge_stop() {
	nemo_closeQuery(bridgequery);
	bridge_rootHandler(NULL, NULL);
	nemo_client_cleanup();
	close(bridgefd);
}

static mtk_module_info_t bridge_info = {
	.name  = ME,
	.start = bridge_start,
	.stop  = bridge_stop
};

__attribute__((constructor)) static void bridge_init() {
	mtk_module_register(&bridge_info);
}

