/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>
#include <net/if.h>
#include <linux/sockios.h>
#include <linux/if_vlan.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>

#include <nemo/client.h>

#define ME "vlan"

#ifndef VLAN_FLAG_REORDER_HDR
#define VLAN_FLAG_REORDER_HDR 1
#endif

typedef struct _vlan {
	llist_iterator_t it;
	char name[IF_NAMESIZE];
	bool zombie;
	nemo_query_t *llintfquery;
	char llintf[IF_NAMESIZE];
	request_t *updaterequest;
	unsigned short vlanid;
	char vlanpriority;
	bool up;
} vlan_t;

static nemo_query_t *vlanquery = NULL;

static llist_t vlans = {NULL, NULL};

static int vlanfd = -1;

static void vlan_setprio(vlan_t *vlan, char vlanpriority) {
	vlan->vlanpriority = vlanpriority;
	if (!vlan->up)
		return;
	SAH_TRACEZ_INFO(ME, "vlan_setprio(name=%s vlanpriority=%d)", vlan->name, vlan->vlanpriority);
	
	struct vlan_ioctl_args args;
	args.cmd = SET_VLAN_EGRESS_PRIORITY_CMD;
	snprintf(args.device1, sizeof(args.device1), "%s", vlan->name);
	int i;
	for (i=0; i<16; i++) {
		args.u.skb_priority = i;
		args.vlan_qos = (vlan->vlanpriority == (char)-1) ? i % 8 : vlan->vlanpriority;
		if (ioctl(vlanfd, SIOCSIFVLAN, &args) == -1) {
			SAH_TRACE_ERROR("SIOCSIFVLAN:SET_VLAN_EGRESS_PRIORITY(name=%s skb_priority=%d vlan_priority=%d) failed: %d(%s)", vlan->name, i, args.vlan_qos, errno, error_string(errno));
			return;
		}
	}
}

static void vlan_register(vlan_t *vlan) {
	if (vlan->up || !*vlan->llintf || !vlan->vlanid)
		return;
	SAH_TRACEZ_INFO(ME, "vlan_register(name=%s llintf=%s vlanid=%d)", vlan->name, vlan->llintf, vlan->vlanid);
	
	struct vlan_ioctl_args args;
	args.cmd = ADD_VLAN_CMD;
	snprintf(args.device1, sizeof(args.device1), "%s", vlan->llintf);
	args.u.VID = vlan->vlanid;
	if (ioctl(vlanfd, SIOCSIFVLAN, &args) == -1) {
		SAH_TRACE_ERROR("SIOCSIFVLAN:ADD_VLAN(name=%s vlanid=%d) failed: %d(%s)", vlan->llintf, vlan->vlanid, errno, error_string(errno));
	}
	
	struct ifreq ifreq; // we could also let netdev set the name, but it would introduce race conditions (when exactly would the name be changed?)
	snprintf(ifreq.ifr_name, IF_NAMESIZE, "%s.%d", vlan->llintf, vlan->vlanid);
	snprintf(ifreq.ifr_newname, IF_NAMESIZE, "%s", vlan->name);
	if (ioctl(vlanfd, SIOCSIFNAME, &ifreq) == -1) {
		SAH_TRACE_ERROR("SIOCSIFNAME(name=%s.%d newname=%s) failed: %d(%s)", vlan->llintf, vlan->vlanid, vlan->name, errno, error_string(errno));
	}
	
	args.cmd = SET_VLAN_FLAG_CMD;
	snprintf(args.device1, sizeof(args.device1), "%s", vlan->name);
	args.u.flag = VLAN_FLAG_REORDER_HDR;
	args.vlan_qos = 1;
	if (ioctl(vlanfd, SIOCSIFVLAN, &args) == -1) {
		SAH_TRACE_ERROR("SIOCSIFVLAN:SET_VLAN_FLAG(name=%s flag=%d value=%d) failed: %d(%s)", vlan->name, VLAN_FLAG_REORDER_HDR, 1, errno, error_string(errno));
	}
	
	vlan->up = true;

	vlan_setprio(vlan, vlan->vlanpriority);

	nemo_setFlag(vlan->name, "up", NULL, "this");
}

static void vlan_unregister(vlan_t *vlan) {
	if (!vlan->up)
		return;
	SAH_TRACEZ_INFO(ME, "vlan_unregister(name=%s)", vlan->name);
	
	vlan->up = false;

	nemo_clearFlag(vlan->name, "up", NULL, "this");

	// set vlan down before actually destroying it
	struct ifreq ifreq;
	strncpy(ifreq.ifr_name, vlan->name, IFNAMSIZ);
	ifreq.ifr_name[IFNAMSIZ - 1] = '\0';
	if (ioctl(vlanfd, SIOCGIFFLAGS, &ifreq) == -1) {
		SAH_TRACE_ERROR("SIOCGIFFLAGS(name=%s) failed: %d (%s)", vlan->name, errno, error_string(errno));
	} else {
		ifreq.ifr_flags &= ~IFF_UP;
		if (ioctl(vlanfd, SIOCSIFFLAGS, &ifreq) == -1) {
			SAH_TRACE_ERROR("SIOCGIFFLAGS(name=%s flags=0x%x) failed: %d (%s)", vlan->name, ifreq.ifr_flags, errno, error_string(errno));
		}
	}
	
	struct vlan_ioctl_args args;
	args.cmd = DEL_VLAN_CMD;
	snprintf(args.device1, sizeof(args.device1), "%s", vlan->name);
	if (ioctl(vlanfd, SIOCSIFVLAN, &args) == -1) {
		SAH_TRACE_ERROR("SIOCSIFVLAN:DEL_VLAN(name=%s) failed: %d(%s)", vlan->name, errno, error_string(errno));
	}
}

static void vlan_llintfHandler(const variant_t *result, void *userdata) {
	vlan_t *vlan = (vlan_t *)userdata;
	const char *llintf = result?string_buffer(variant_da_string(result)):NULL;
	vlan_unregister(vlan);
	snprintf(vlan->llintf, IF_NAMESIZE, "%s", llintf?llintf:"");
	vlan_register(vlan);
}

static bool vlan_replyHandler(request_t *request, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)pcb; (void)from; 
	vlan_t *vlan = (vlan_t *)userdata;
	reply_t *reply = request_reply(request);
	if (!reply)
		return true;
	reply_item_t *item;
	for(item = reply_firstItem(reply); item; item = reply_nextItem(item)) {
		if (reply_item_type(item) != reply_type_object)
			continue;
		object_t *object = reply_item_object(item);
		unsigned short vlanid = object_parameterUInt16Value(object, "VLANID");
		char vlanpriority = object_parameterInt8Value(object, "VLANPriority");
		if (vlanid != vlan->vlanid)
			vlan_unregister(vlan);
		if (vlanpriority != vlan->vlanpriority) {
			vlan_setprio(vlan, vlanpriority);
		}
		if (vlanid != vlan->vlanid) {
			vlan->vlanid = vlanid;
			vlan_register(vlan);
		}
	}
	return true;
}

static vlan_t *vlan_create(const char *name) {
	vlan_t *vlan = calloc(1, sizeof(vlan_t));
	snprintf(vlan->name, IF_NAMESIZE, "%s", name);
	vlan->llintfquery = nemo_openQuery_luckyIntf(name, ME, "netdev-up", "one level down", vlan_llintfHandler, vlan);
	char path[64];
	snprintf(path, 64, "NeMo.Intf.%s", name);
	vlan->updaterequest = request_create_getObject(path, 0, request_common_path_key_notation | request_notify_values_changed | request_no_object_caching | request_getObject_parameters);
	request_setReplyHandler(vlan->updaterequest, vlan_replyHandler);
	request_setData(vlan->updaterequest, vlan);
	if (!pcb_sendRequest(nemo_client_pcb(), nemo_client_peer(), vlan->updaterequest)) {
		SAH_TRACE_ERROR("Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
	}
	llist_append(&vlans, &vlan->it);
	return vlan;
} 

static vlan_t *vlan_find(const char *name) {
	vlan_t *vlan;
	for (vlan = (vlan_t *)llist_first(&vlans); vlan; vlan = (vlan_t *)llist_iterator_next(&vlan->it)) {
		if (!strcmp(vlan->name, name))
			return vlan;
	}
	return NULL;
}

static void vlan_destroy(vlan_t *vlan) {
	if (!vlan)
		return;
	nemo_closeQuery(vlan->llintfquery);
	request_destroy(vlan->updaterequest);
	vlan_llintfHandler(NULL, vlan);
	llist_iterator_take(&vlan->it);
	free(vlan);
}

static void vlan_rootHandler(const variant_t *result, void *userdata) {
	(void)userdata;
	variant_list_t *intfs = result?variant_da_list(result):NULL;
	variant_list_iterator_t *it;
	vlan_t *vlan, *vlannext;
	for (vlan = (vlan_t *)llist_first(&vlans); vlan; vlan = (vlan_t *)llist_iterator_next(&vlan->it))
		vlan->zombie = true;
	if (intfs) variant_list_for_each(it, intfs) {
		const char *name = string_buffer(variant_da_string(variant_list_iterator_data(it)));
		if (!name)
			continue;
		vlan = vlan_find(name);
		if (!vlan)
			vlan = vlan_create(name);
		vlan->zombie = false;
	}
	for (vlan = (vlan_t *)llist_first(&vlans); vlan; vlan = vlannext) {
		vlannext = (vlan_t *)llist_iterator_next(&vlan->it);
		if (vlan->zombie)
			vlan_destroy(vlan);
	}
}

static bool vlan_start(argument_value_list_t *args __attribute__((unused))) {
	vlanfd = socket(AF_INET, SOCK_STREAM, 0);
	nemo_client_initialize(mtk_getPcb(), mtk_getSysbus());
	vlanquery = nemo_openQuery_getIntfs("lo", ME, "vlan && enabled", "all", vlan_rootHandler, NULL);
	return true;
}

static void vlan_stop() {
	nemo_closeQuery(vlanquery);
	vlan_rootHandler(NULL, NULL);
	nemo_client_cleanup();
	close(vlanfd);
}

static mtk_module_info_t vlan_info = {
	.name  = ME,
	.start = vlan_start,
	.stop  = vlan_stop
};

__attribute__((constructor)) static void vlan_init() {
	mtk_module_register(&vlan_info);
}

