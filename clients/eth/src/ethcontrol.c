/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/*
* Copyright (c) 2018 SoftAtHome
*
* The information and source code contained herein is the exclusive
* property of SoftAtHome and may not be disclosed, examined, or
* reproduced in whole or in part without explicit written authorization
* of the copyright owner.
*
*/
#include <errno.h>
#include <net/if.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>

#include <mtk.h>
#include <nemo/client.h>
#include <debug/sahtrace.h>

#define ME "ethcontrol"

static int ethcontrolfd = -1;
static llist_t ethcontrols = {NULL, NULL};
static nemo_query_t *ethcontrolquery = NULL;

typedef struct _ethcontrol {
    llist_iterator_t it;
    char ifname[IF_NAMESIZE];
    bool zombie;
    nemo_query_t *enable_query;
} ethcontrol_t;

static void ethcontrol_ifup(const char *ifname, bool up) {
    struct ifreq ifreq;
    strncpy(ifreq.ifr_name, ifname, IF_NAMESIZE);
    ifreq.ifr_name[IF_NAMESIZE - 1] = '\0';

    if (ioctl(ethcontrolfd, SIOCGIFFLAGS, &ifreq) == -1) {
        SAH_TRACE_ERROR("Get flags of netdev intf [%s] failed: %d (%s)", ifname, errno, strerror(errno));
        return;
    }

    if (up) {
        SAH_TRACEZ_INFO(ME, "Setting netdev intf [%s] up", ifname);
        ifreq.ifr_flags |= IFF_UP;
    } else {
        SAH_TRACEZ_INFO(ME, "Setting netdev intf [%s] down", ifname);
        ifreq.ifr_flags &= ~IFF_UP;
    }

    if (ioctl(ethcontrolfd, SIOCSIFFLAGS, &ifreq) == -1) {
        SAH_TRACE_ERROR("Set flags of netdev intf [%s] failed: %d (%s)", ifname, errno, strerror(errno));
        return;
    }
}

static void ethcontrol_enableHandler(const variant_t *result, void *userdata) {
    ethcontrol_t *ethcontrol = (ethcontrol_t *)userdata;
    if (!ethcontrol) {
        SAH_TRACEZ_ERROR(ME, "ethcontrol : NULL");
        return;
    }
    ethcontrol_ifup(ethcontrol->ifname, variant_bool(result));
}

static ethcontrol_t *ethcontrol_create(const char *ifname) {
    ethcontrol_t *ethcontrol = calloc(1, sizeof(ethcontrol_t));
    snprintf(ethcontrol->ifname, IF_NAMESIZE, "%s", ifname);
    llist_iterator_initialize(&ethcontrol->it);
    llist_append(&ethcontrols, &ethcontrol->it);
    ethcontrol->enable_query = nemo_openQuery_hasFlag(ifname, ME, "enabled", NULL, "this", ethcontrol_enableHandler, ethcontrol);
    return ethcontrol;
} 

static ethcontrol_t *ethcontrol_find(const char *ifname) {
    ethcontrol_t *ethcontrol;
    for (ethcontrol = (ethcontrol_t *)llist_first(&ethcontrols); ethcontrol; ethcontrol = (ethcontrol_t *)llist_iterator_next(&ethcontrol->it)) {
        if (!strcmp(ethcontrol->ifname, ifname))
            return ethcontrol;
    }
    return NULL;
}

static void ethcontrol_destroy(ethcontrol_t *ethcontrol) {
    if (!ethcontrol)
        return;
    nemo_closeQuery(ethcontrol->enable_query);
    ethcontrol->enable_query = NULL;
    llist_iterator_take(&ethcontrol->it);
    free(ethcontrol);
}

static void ethcontrol_cleanup() {
    llist_iterator_t *it = NULL;
    llist_for_each(it, &ethcontrols) {
        ethcontrol_destroy((ethcontrol_t *)it);
    }
}

static void ethcontrol_rootHandler(const variant_t *result, void *userdata) {
    (void)userdata;
    
    ethcontrol_t *ethcontrol, *ethcontrolnext;
    for (ethcontrol = (ethcontrol_t *)llist_first(&ethcontrols); ethcontrol; ethcontrol = (ethcontrol_t *)llist_iterator_next(&ethcontrol->it)) {
        ethcontrol->zombie = true;
    }

    variant_list_iterator_t *it;
    variant_list_t *intfs = result?variant_da_list(result):NULL;
    if (intfs) {
        variant_list_for_each(it, intfs) {
            const char *name = string_buffer(variant_da_string(variant_list_iterator_data(it)));
            if (!name)
                continue;
            ethcontrol = ethcontrol_find(name);
            if (!ethcontrol)
                ethcontrol = ethcontrol_create(name);
            ethcontrol->zombie = false;
        }
    } 
    for (ethcontrol = (ethcontrol_t *)llist_first(&ethcontrols); ethcontrol; ethcontrol = ethcontrolnext) {
        ethcontrolnext = (ethcontrol_t *)llist_iterator_next(&ethcontrol->it);
        if (ethcontrol->zombie)
            ethcontrol_destroy(ethcontrol);
    }
}

static bool ethcontrol_start(argument_value_list_t *args __attribute__((unused))) {
    SAH_TRACEZ_INFO(ME, "Initializing ethcontrol module");
    ethcontrolfd = socket(AF_INET, SOCK_STREAM, 0);
    nemo_client_initialize(mtk_getPcb(), mtk_getSysbus());

    ethcontrolquery = nemo_openQuery_getIntfs("lo", ME, "eth-control", "all", ethcontrol_rootHandler, NULL);
    return true;
}

static void ethcontrol_stop() {
    SAH_TRACEZ_INFO(ME, "Cleaning up ethcontrol module");
    ethcontrol_cleanup();
    nemo_closeQuery(ethcontrolquery);
    ethcontrolquery = NULL;
    close(ethcontrolfd);
    nemo_client_cleanup();
}

static mtk_module_info_t ethcontrol_info = {
    .name = ME, .start = ethcontrol_start, .stop = ethcontrol_stop};

__attribute__((constructor)) static void ethcontrol_init() {
    mtk_module_register(&ethcontrol_info);
}
