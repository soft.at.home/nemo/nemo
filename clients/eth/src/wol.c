/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/*
* Copyright (c) 2018 SoftAtHome
*
* The information and source code contained herein is the exclusive
* property of SoftAtHome and may not be disclosed, examined, or
* reproduced in whole or in part without explicit written authorization
* of the copyright owner.
*
*/
#include <net/if.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>

#include <linux/netlink.h>
#include <linux/sockios.h>
#include <linux/ethtool.h>

#include <mtk.h>
#include <nemo/client.h>

#define ME "wol"

/* Context for sub-commands */
struct cmd_context {
    const char *devname;	/* net device name */
    int fd;			/* socket suitable for ethtool ioctl */
    struct ifreq ifr;	/* ifreq suitable for ethtool ioctl */
};

static int wolfd = -1;
static llist_t wols = {NULL, NULL};
static nemo_query_t *wolquery = NULL;
struct cmd_context ctx;


typedef struct _wol {
    llist_iterator_t it;
    char ifname[IF_NAMESIZE];
    bool zombie;
} wol_t;


static int send_ioctl(void *cmd) {
    ctx.ifr.ifr_data = cmd;
    return ioctl(ctx.fd, SIOCETHTOOL, &ctx.ifr);
}

static void wol_setmagic(const char *ifname, bool up) {
    struct ethtool_wolinfo wol;
    int err = 0;

    /* Setup interface name */
    ctx.devname = ifname;

    /* Setup our control structures. */
    memset(&ctx.ifr, 0, sizeof(ctx.ifr));
    strcpy(ctx.ifr.ifr_name, ctx.devname);

    /* Try to get the wol settings */
    wol.cmd = ETHTOOL_GWOL;
    err = send_ioctl(&wol);
    if (err < 0) {
        SAH_TRACE_ERROR("Cannot get current wake-on-lan settings");
    }

    if (up) {
        SAH_TRACEZ_INFO(ME, "Setting MAGIC PACKET flag for Intf [%s]", ifname);
        wol.wolopts = WAKE_MAGIC;
    } else {
        SAH_TRACEZ_INFO(ME, "Clearing MAGIC PACKET flag for Intf [%s]", ifname);
        wol.wolopts = 0;
    }

    /* Try to perform the update. */
    wol.cmd = ETHTOOL_SWOL;   
    err = send_ioctl(&wol);
    if (err < 0) {
        SAH_TRACE_ERROR("Cannot set new wake-on-lan settings");
    }
}

static wol_t *wol_create(const char *ifname) {
    wol_t *wol = calloc(1, sizeof(wol_t));
    snprintf(wol->ifname, IF_NAMESIZE, "%s", ifname);
    llist_iterator_initialize(&wol->it);
    llist_append(&wols, &wol->it);
    wol_setmagic(wol->ifname, true);
    return wol;
} 

static wol_t *wol_find(const char *ifname) {
    wol_t *wol;
    for (wol = (wol_t *)llist_first(&wols); wol; wol = (wol_t *)llist_iterator_next(&wol->it)) {
        if (!strcmp(wol->ifname, ifname))
            return wol;
    }
    return NULL;
}

static void wol_destroy(wol_t *wol) {
    if (!wol)
        return;
    wol_setmagic(wol->ifname, false);
    llist_iterator_take(&wol->it);
    free(wol);
}

static void wol_cleanup() {
    llist_iterator_t *it = NULL;
    llist_for_each(it, &wols) {
        wol_destroy((wol_t *)it);
    }
}

static void wol_rootHandler(const variant_t *result, void *userdata) {
    (void)userdata;
    
    wol_t *wol, *wolnext;
    for (wol = (wol_t *)llist_first(&wols); wol; wol = (wol_t *)llist_iterator_next(&wol->it)) {
        wol->zombie = true;
    }

    variant_list_iterator_t *it;
    variant_list_t *intfs = result?variant_da_list(result):NULL;
    if (intfs) {
        variant_list_for_each(it, intfs) {
            const char *name = string_buffer(variant_da_string(variant_list_iterator_data(it)));
            if (!name)
                continue;
            wol = wol_find(name);
            if (!wol)
                wol = wol_create(name);
            wol->zombie = false;
        }
    } 
    for (wol = (wol_t *)llist_first(&wols); wol; wol = wolnext) {
        wolnext = (wol_t *)llist_iterator_next(&wol->it);
        if (wol->zombie)
            wol_destroy(wol);
    }
}

static bool wol_start(argument_value_list_t *args __attribute__((unused))) {
    SAH_TRACEZ_INFO(ME, "Initializing wol module");

    ctx.fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (ctx.fd < 0)
        ctx.fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_GENERIC);
    if (ctx.fd < 0) {
        SAH_TRACE_ERROR("Cannot get control socket");
        return false;
    }

    nemo_client_initialize(mtk_getPcb(), mtk_getSysbus());

    wolquery = nemo_openQuery_getIntfs("lo", ME, "wol", "all", wol_rootHandler, NULL);
    return true;
}

static void wol_stop() {
    SAH_TRACEZ_INFO(ME, "Cleaning up wol module");
    wol_cleanup();
    nemo_closeQuery(wolquery);
    wolquery = NULL;
    close(wolfd);
    nemo_client_cleanup();

}

static mtk_module_info_t wol_info = {
    .name = ME, .start = wol_start, .stop = wol_stop};

__attribute__((constructor)) static void wol_init() {
    mtk_module_register(&wol_info);
}
