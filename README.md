# nemo

## Summary

This component is a container for shared object modules to extend the
NeMo functionality, and for MIBs to extend its data model interfaces.

## Description

This component contains code for shared object modules that can be loaded
in either the `nemo-core` or the `nemo-clients` processes.

The modules for the core will extend the core's functionality directly.
They have full access to the data model and already existing internal API.
Generally, they are accompied by a MIB, an extension to the base NeMo data model.

The modules for the clients need to use the API as exported by `lib_nemo-clients`.
They do not have direct access to the data model, but have to use the RPC's
as provided by the NeMo core.
