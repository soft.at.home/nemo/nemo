/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <debug/sahtrace.h>
#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "nat"

#include <nemo/core.h>

struct _intf_blob {
	nemo_query_t *netdevquery;
	intf_t *intf;
	intf_t *netdev;
};
static int dummy = 0;
static void *blobkey = &dummy;

nemo_query_t *natquery = NULL;

static void nat_handleResult(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	if (blob->netdev) {
		object_t *object = plugin_intf2object(blob->netdev);
		object_parameterSetBoolValue(object, "NATEnabled", false);
		object_commit(object);
	}
	const char *netdev = result?string_buffer(variant_da_string(result)):NULL;
	SAH_TRACEZ_INFO(ME, "blob->intf=%s blob->netdev=%s netdev=%s",
					intf_name(blob->intf), intf_name(blob->netdev), netdev?:"(null)");
	blob->netdev = netdev && *netdev ? intf_find(netdev) : NULL;
	if (blob->netdev) {
		object_t *object = plugin_intf2object(blob->netdev);
		object_parameterSetBoolValue(object, "NATEnabled", true);
		object_commit(object);
	}
}

static void nat_configHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata; (void)flag;
	if (value && flagset_check(intf_flagset(intf), "nat-config") && flagset_check(intf_flagset(intf), "enabled")) {
		intf_blob_t *blob = intf_getBlob(intf, blobkey);
		if (blob)
			return;
		blob = calloc(1, sizeof(intf_blob_t));
		if (!blob) {
			SAH_TRACE_ERROR("nat - calloc failed");
			return;
		}
		SAH_TRACEZ_INFO(ME, "Start nat-config on %s", intf_name(intf));
		blob->intf = intf;
		blob->netdevquery = nemo_openQuery_luckyIntf(intf_name(intf), ME, "netdev", "down", nat_handleResult, blob);
		intf_addBlob(intf, blobkey, blob);
	} else {
		intf_blob_t *blob = intf_getBlob(intf, blobkey);
		if (!blob)
			return;
		SAH_TRACEZ_INFO(ME, "Stop nat-config on %s", intf_name(intf));
		intf_delBlob(intf, blobkey);
		nemo_closeQuery(blob->netdevquery);
		nat_handleResult(NULL, blob);
		free(blob);
	}
}

static void nat_writeNATEnabled(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	if (variant_bool(parameter_getValue(parameter))) {
		flagset_set(intf_flagset(plugin_object2intf(parameter_owner(parameter))), "nat-enabled");
	} else {
		flagset_clear(intf_flagset(plugin_object2intf(parameter_owner(parameter))), "nat-enabled");
	}
}

static void nat_netdevHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)flag; (void)userdata;
	parameter_t *natenabled = object_getParameter(plugin_intf2object(intf), "NATEnabled");
	if (value) {
		plugin_setParameterWriteHandler(natenabled, nat_writeNATEnabled);
		nat_writeNATEnabled(natenabled, NULL);
	} else {
		plugin_setParameterWriteHandler(natenabled, NULL);
		flagset_clear(intf_flagset(intf), "nat-enabled");
	}
}

static bool nat_start(argument_value_list_t *args __attribute__((unused))) {
	SAH_TRACEZ_INFO(ME, "Initializing nat module");
	intf_addFlagListener(NULL, "nat-config", nat_configHandler, NULL, true);
	intf_addFlagListener(NULL, "enabled", nat_configHandler, NULL, true);
	intf_addFlagListener(NULL, "netdev", nat_netdevHandler, NULL, true);
	return true;
}

static void nat_stop() {
	SAH_TRACEZ_INFO(ME, "Cleaning up nat module");
	intf_delFlagListener(NULL, "nat-config", nat_configHandler, NULL, true);
	intf_delFlagListener(NULL, "enabled", nat_configHandler, NULL, true);
	intf_delFlagListener(NULL, "netdev", nat_netdevHandler, NULL, true);
}

static mtk_module_info_t nat_info = {
	.name  = ME,
	.start = nat_start,
	.stop  = nat_stop
};

__attribute__((constructor)) static void nat_init() {
	mtk_module_register(&nat_info);
}

