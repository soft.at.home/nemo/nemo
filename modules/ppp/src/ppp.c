/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <debug/sahtrace.h>
#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "ppp"

#include <nemo/core.h>

struct _intf_blob {
	map_t *map;
	bool enabled;
	char *lowerintf;
	bool pppenable;
	nemo_query_t *lowerintfquery;
	intf_t *intf;
};

static int ppp_dummy;
static void *ppp_blobkey = &ppp_dummy;

static peer_info_t *ppp_dst = NULL;

static void ppp_check(intf_blob_t *blob) {
	bool pppenable = blob->enabled && blob->lowerintf;
	if (pppenable || blob->pppenable) {
		SAH_TRACEZ_INFO(ME, "request_create_setObject(path=%s)", map_dstpath(blob->map));
		request_t *req = request_create_setObject(map_dstpath(blob->map), request_common_path_key_notation);
		variant_t var;
		variant_initialize(&var, variant_type_unknown);
		if (blob->lowerintf) {
			variant_setChar(&var, blob->lowerintf);
			SAH_TRACEZ_INFO(ME, "request_addParameter(path=%s param=%s value=%s)", map_dstpath(blob->map), "LowerIntf", blob->lowerintf);
			request_addParameter(req, "LowerIntf", &var);
		}
		variant_setBool(&var, pppenable);
		SAH_TRACEZ_INFO(ME, "request_addParameter(path=%s param=%s value=%s)", map_dstpath(blob->map), "Enable", pppenable?"true":"false");
		request_addParameter(req, "Enable", &var);
		SAH_TRACEZ_INFO(ME, "pcb_sendRequest(path=%s)", map_dstpath(blob->map));
		if (!pcb_sendRequest(plugin_getPcb(), ppp_dst, req)) {
			SAH_TRACE_ERROR("ppp - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
		}
		request_destroy(req);
		variant_cleanup(&var);
		blob->pppenable = pppenable;
	}
}

static void ppp_intfFlagHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	if (!strcmp(flag, "enabled")) {
		blob->enabled = value;
		ppp_check(blob);
	}
}

static void ppp_llintfHandler(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	free(blob->lowerintf);
	blob->lowerintf = result?variant_char(result):NULL;
	if (blob->lowerintf && !*blob->lowerintf) {
		free(blob->lowerintf);
		blob->lowerintf = NULL;
	}
	if (blob->lowerintf) {
		object_t *object = plugin_intf2object(blob->intf);
		char *xtmvalue = object_parameterCharValue(plugin_intf2object(intf_find(blob->lowerintf)), "LinkType");
		const char *pppvalue = !strcmp(xtmvalue?xtmvalue:"", "PPPoA") ? "PPPoA" : "PPPoE";
		object_parameterSetCharValue(object, "TransportType", pppvalue);
		object_commit(object);
		free(xtmvalue);
	}
	ppp_check(blob);
}

static void ppp_intfMgmtHandler(intf_t *intf, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	map_commit(blob->map);
}

static void ppp_intfMibHandler(intf_t *intf, intf_mibevent_t event, const char *path, void *userdata) {
	(void)userdata;
	if (event != intf_mibevent_updated)
		return;
	if (strcmp(path, "IPv6Disable") && strcmp(path, "IPv6CPEnable"))
		return;
	object_t *object = plugin_intf2object(intf);
	bool ipv6disable = object_parameterBoolValue(object, "IPv6Disable");
	bool ipv6cpenable = object_parameterBoolValue(object, "IPv6CPEnable");
	bool enable = object_parameterBoolValue(object, "Enable");
	if (!enable) {
		SAH_TRACE_NOTICE("ppp - Ignore disabled PPP connection");
		return; 
	}
	if (ipv6disable != ipv6cpenable)
		return;
	if (!strcmp(path, "IPv6Disable")) {
		SAH_TRACE_WARNING("ppp - Discovered invalid combination IPv6Disable=%d IPv6CPEnable=%d -> set IPv6CPEnable to %d",
				ipv6disable, ipv6cpenable, !ipv6disable);
		object_parameterSetBoolValue(object, "IPv6CPEnable", !ipv6disable);
	} else if (!strcmp(path, "IPv6CPEnable")) {
		SAH_TRACE_WARNING("ppp - Discovered invalid combination IPv6Disable=%d IPv6CPEnable=%d -> set IPv6Disable to %d",
				ipv6disable, ipv6cpenable, !ipv6cpenable);
		object_parameterSetBoolValue(object, "IPv6Disable", !ipv6cpenable);
	}
	object_commit(object);
}

static long ppp_uptime() {
	char buf[64], *ptr = NULL;
	FILE *f = fopen("/proc/uptime", "r"); // Remark using sysinfo() would be easier but it's not available in uclibc
	if (f) {
		ptr = fgets(buf, 64, f);
		fclose(f);
	}
	return ptr?strtol(ptr, NULL, 0):0;
}

static bool ppp_readLastChange(parameter_t *parameter, variant_t *value) {
	uint32_t t0 = object_parameterUInt32Value(parameter_owner(parameter), "LastChangeTime");
	if (t0)
		variant_setUInt32(value, ppp_uptime() - t0);
	else
		variant_setUInt32(value, 0);
	return true;
}

static void ppp_writeConnectionStatus(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	unsigned long t = ppp_uptime();
	object_t *object = parameter_owner(parameter);
	intf_t *intf = plugin_object2intf(object);
	const char *value = string_buffer(variant_da_string(parameter_getValue(parameter)));
	if (!strcmp(value?value:"", "Connected")) {
		flagset_set(intf_flagset(intf), "up");
	} else {
		flagset_clear(intf_flagset(intf), "up");
	}
	SAH_TRACEZ_INFO(ME, "object_parameterSetUInt32Value(path=NeMo.Intf.%s parameter=%s value=%lu)",
			intf_name(intf), "LastChangeTime", t);
	object_parameterSetUInt32Value(object, "LastChangeTime", t);
	SAH_TRACEZ_INFO(ME, "object_commit(path=NeMo.Intf.%s)", intf_name(intf));
	object_commit(object);
}

static void ppp_translateDialOnDemand(map_parameter_t *parameter, map_direction_t direction, variant_t *value) {
	(void)parameter;
	switch (direction) {
	case map_from_destination:
		if (variant_bool(value))
			variant_setChar(value, "OnDemand");
		else
			variant_setChar(value, "AlwaysOn");
		break;
	case map_to_destination:
		variant_setBool(value, !strcmp(string_buffer(variant_da_string(value))?:"", "OnDemand"));
		break;
	}
}

static void ppp_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata; (void)flag;
	object_t *object = plugin_intf2object(intf);
	
	if (value) {
		intf_blob_t *blob = calloc(1, sizeof(intf_blob_t));
		if (!blob) {
			SAH_TRACE_ERROR("ppp - calloc failed");
			return;
		}
		intf_addBlob(intf, ppp_blobkey, blob);
		blob->intf = intf;
		
		blob->map = map_create(object, MAP_OWNER, ppp_dst, "PPP.Intf.%s", intf_name(intf));
		map_parameter_create(blob->map, object_getParameter(object, "Name"), NULL, MAP_WRITEONLY | MAP_KEY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Username"), "UserName", MAP_DEFAULT, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Password"), NULL, MAP_DEFAULT, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "ConnectionStatus"), "PPPStatus", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LastConnectionError"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LocalIPAddress"), "ReceivedLocalAddress", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "RemoteIPAddress"), "ReceivedRemoteAddress", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "DNSServers"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "MaxMRUSize"), "MRU", MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "PPPoESessionID"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "PPPoEACName"), NULL, MAP_DEFAULT, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "PPPoEServiceName"), NULL, MAP_DEFAULT, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "TransportType"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LCPEcho"), "Echo", MAP_DEFAULT, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LCPEchoRetry"), "EchoTolerance", MAP_DEFAULT, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "IPCPEnable"), NULL, MAP_DEFAULT, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "IPv6CPEnable"), NULL, MAP_DEFAULT, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "IPv6CPLocalInterfaceIdentifier"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "IPv6CPRemoteInterfaceIdentifier"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "ConnectionTrigger"), "DialOnDemand", MAP_DEFAULT, ppp_translateDialOnDemand, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "IdleDisconnectTime"), "IdleTimeout", MAP_DEFAULT, NULL, NULL);
		map_commit(blob->map);
		
		plugin_setParameterWriteHandler(object_getParameter(object, "ConnectionStatus"), ppp_writeConnectionStatus);
		plugin_setParameterReadHandler(object_getParameter(object, "LastChange"), ppp_readLastChange);
		
		intf_addFlagListener(intf, "enabled", ppp_intfFlagHandler, blob, true);
		intf_addMgmtListener(intf, NULL, ppp_intfMgmtHandler, NULL, blob, false);
		intf_addMibListener(intf, ppp_intfMibHandler, blob);
		ppp_intfMibHandler(intf, intf_mibevent_updated, "IPv6Disable", blob);

		blob->lowerintfquery = nemo_openQuery_luckyIntf(intf_name(intf), ME, "netdev-up || xtm-fd-up", "one level down", ppp_llintfHandler, blob);
		
	} else {
		intf_blob_t *blob = intf_getBlob(intf, ppp_blobkey);
		if (!blob)
			return;

		plugin_setParameterWriteHandler(object_getParameter(object, "ConnectionStatus"), NULL);
		plugin_setParameterReadHandler(object_getParameter(object, "LastChange"), NULL);
		
		intf_delMibListener(intf, ppp_intfMibHandler, blob);
		intf_delFlagListener(intf, "enabled", ppp_intfFlagHandler, blob, false);
		intf_delMgmtListener(intf, NULL, ppp_intfMgmtHandler, NULL, blob, false);
		
		nemo_closeQuery(blob->lowerintfquery);
		ppp_llintfHandler(NULL, blob);
		
		map_destroy(blob->map);
		
		intf_delBlob(intf, ppp_blobkey);

		free(blob);
	}
}

static bool ppp_start(argument_value_list_t *args) {
	SAH_TRACEZ_INFO(ME, "Initializing ppp module");
	char *dsturi = NULL;
	arg_takeChar(&dsturi, args, ARG_BYNAME, "dst");
	if (dsturi && *dsturi) {
		uri_t *uri = NULL;
		ppp_dst = connection_connect(pcb_connection(plugin_getPcb()), dsturi, &uri);
		if (!ppp_dst) {
			SAH_TRACE_ERROR("ppp - Connection to %s failed - fallback to sysbus", dsturi);
			// no fatal error, we still have fallback over the sysbus.
		}
		if (uri)
			uri_destroy(uri);
	}
	free(dsturi);
	if (!ppp_dst)
		ppp_dst = plugin_getPeer();
	intf_addFlagListener(NULL, "ppp", ppp_rootHandler, NULL, true);
	return true;
}

static void ppp_stop() {
	SAH_TRACEZ_INFO(ME, "Cleaning up ppp module");
	intf_delFlagListener(NULL, "ppp", ppp_rootHandler, NULL, true);
	if (ppp_dst != plugin_getPeer())
		peer_destroy(ppp_dst);
}

static mtk_module_info_t ppp_info = {
	.name  = ME,
	.start = ppp_start,
	.stop  = ppp_stop
};

__attribute__((constructor)) static void ppp_init() {
	mtk_module_register(&ppp_info);
}

