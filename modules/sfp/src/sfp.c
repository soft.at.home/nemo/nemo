/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>
#include <pcb/core/function.h>

#include <mtk.h>
#define ME "sfp"

#include <nemo/core.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <endian.h>
#include <math.h>
#include <stdio.h>

struct _sfp_i2c_mib {
    char vendor_name[16+1];
    char vendor_pn[16+1];
    char vendor_sn[16+1];
    char vendor_rev[4+1];
    uint8_t vendor_oui[3];
    uint8_t identifier;
    uint8_t connector;
    uint8_t transceiver[8];
    uint8_t br_nominal;
    uint16_t wavelength;
    uint8_t diagnostic;
    uint8_t options;
    uint8_t compliance;
    char datecode[8+1];
} sfp_i2c_mib;

struct _sfp_i2c_calibration_mib {
    bool needsCalibration;
    float rx_pwr_4;
    float rx_pwr_3;
    float rx_pwr_2;
    float rx_pwr_1;
    float rx_pwr_0;
    float tx_I_slope;
    int16_t tx_I_offset;
    float tx_pwr_slope;
    int16_t tx_pwr_offset;
    float T_slope;
    int16_t T_offset;
    float V_slope;
    int16_t V_offset;
} sfp_i2c_calibration_mib;

struct _intf_blob {
    nemo_query_t *physupquery;
    pcb_timer_t *sfp_monitor;
    intf_t *intf;
};

static int sfp_dummy;
static void *sfp_blobkey = &sfp_dummy;

/* only one SFP is supported */
static long old_SFP_ModDef0, old_SFP_LOS, old_SFP_TXFault;
static bool sfp_supportsDiagnostics;

static char *sfp_moddef0 = "/var/dev/gpio/SFP_ModDef0";
static char *sfp_los = "/var/dev/gpio/SFP_LOS";
static char *sfp_txfault = "/var/dev/gpio/SFP_TXFault";

/* Bits of the field "Diagnostic Monitoring Type" (DMT) */
#define DMT_DIGITAL_DIAGNOSTIC_MONITORING_IMPLEMENTED 0x40
#define DMT_EXTERNALLY_CALIBRATED 0x10

static const char * TRX_FORM_FACTOR         = "/proc/trx/form_factor";
static const char * TRX_TYPE                = "/proc/trx/type";
static const char * TRX_CORR_FACTOR_TX_PWR  = "/proc/trx/corr_factor_tx_pwr";
static const char * TRX_CORR_FACTOR_CURRENT = "/proc/trx/corr_factor_current";

/* Info about correction factor to be applied for 1 measurement, e.g. TX power.
 *
 * Default all fields are false or 0.
 *
 * @proc_file_handled: module sets field to true if it has read the
 *    corresponding proc file, e.g. the proc file for the TX power, or if it has
 *    found out there is no such proc file. The module does not attempt to
 *    access the file again until a new SFP is plugged in (the module sets the
 *    field to false if an SFP is unplugged).
 * @multiplier: multiplier part of the correction factor. Module extracts
 *    the value from the proc file.
 * @divisor: divisor part of the correction factor. Module extracts
 *    the value from the proc file.
 */
struct _sfp_cfactor_info {
    bool     proc_file_handled;
    uint16_t multiplier;
    uint16_t divisor;
};
typedef struct _sfp_cfactor_info sfp_cfactor_info;

static sfp_cfactor_info s_cfactor_tx_pwr;  /* Correction factor for TX power */
static sfp_cfactor_info s_cfactor_current; /* Correction factor for TX bias current */


static long sfp_readGPIO(const char *filename) {
    int value;
    FILE *f = fopen(filename, "r");
    if (!f) {
        SAH_TRACEZ_ERROR(ME, "Error opening '%s' for reading!", filename);
        return -1;
    }
    value = fgetc(f);
    fclose(f);
    if(value == '0')
        return 0;
    if(value == '1')
        return 1;
    SAH_TRACEZ_ERROR(ME, "Error reading '%s'!", filename);
    return -1;
}

static bool sfp_writeGPIO(const char *filename, int value) {
    int ret = (int)value + '0';
    FILE *f = fopen(filename, "w");
    if (!f) {
        SAH_TRACEZ_ERROR(ME, "Error opening '%s' for writing!", filename);
        return false;
    }
    value = fputc(ret, f);
    if (value != ret) {
        SAH_TRACEZ_ERROR(ME, "Error writing to '%s'!", filename);
        ret = false;
    }
    else
        ret = true;
    fclose(f);
    return ret;
}

static int sfp_openI2CDev(uint8_t address) {
    int file = open("/dev/i2c-0", O_RDWR);
    if (file < 0) {
        SAH_TRACEZ_ERROR(ME, "Error opening '/dev/i2c-0'!");
        return file;
    }
    if (ioctl(file, I2C_SLAVE_FORCE, address) < 0) {
        SAH_TRACEZ_ERROR(ME, "Error setting the slave address 0x%x for '/dev/i2c-0'!", address);
        close (file);
        return -1;
    }
    return file;
}

static bool sfp_readI2CByte(int file, uint8_t daddress, uint8_t *value) {
    struct i2c_smbus_ioctl_data cmd;
    union i2c_smbus_data data;

    cmd.read_write = I2C_SMBUS_READ;
    cmd.command = daddress;
    cmd.size = I2C_SMBUS_BYTE_DATA;
    cmd.data = &data;

    if (ioctl(file, I2C_SMBUS, &cmd)) {
        SAH_TRACEZ_ERROR(ME, "Error reading I2C byte at %02x!", daddress);
        return false;
    }

    *value = data.byte;
    return true;
}

static bool sfp_readI2CWord(int file, uint8_t daddress, uint16_t *value) {
    uint8_t tmp;
    if (!sfp_readI2CByte(file, daddress, &tmp))
        return false;
    *value = tmp << 8;
    if (!sfp_readI2CByte(file, daddress+1, &tmp))
        return false;
    *value = *value + tmp;
    return true;
}

static bool sfp_readI2CDoubleWord(int file, uint8_t daddress, uint32_t *value) {
    uint16_t tmp;
    if (!sfp_readI2CWord(file, daddress, &tmp))
        return false;
    *value = tmp << 16;
    if (!sfp_readI2CWord(file, daddress+2, &tmp))
        return false;
    *value = *value + tmp;
    return true;
}

static bool sfp_readI2CChar(int file, uint8_t daddress, char *buffer, uint8_t length) {
    struct i2c_smbus_ioctl_data cmd;
    union i2c_smbus_data data;
    char *end = buffer + length - 1;

    cmd.read_write = I2C_SMBUS_READ;
    cmd.size = I2C_SMBUS_BYTE_DATA;
    cmd.data = &data;

    buffer[--length] = 0;
    daddress += length;

    while (length--) {
        cmd.command = --daddress;
        if (ioctl(file, I2C_SMBUS, &cmd) || data.byte > 0x7f) {
            SAH_TRACEZ_ERROR(ME, "Error reading I2C byte at %02x!", daddress);
            buffer[0] = 0;
            return false;
        }
        buffer[length] = data.byte;
    }

    //Trim whitespaces at the end of the string
    for(; buffer <= end && (*end == ' ' || *end == 0); end--);
    end[1] = 0;

    return true;
}

static bool sfp_readI2CByteArray(int file, uint8_t daddress, uint8_t *buffer, uint8_t length) {
    struct i2c_smbus_ioctl_data cmd;
    union i2c_smbus_data data;

    cmd.read_write = I2C_SMBUS_READ;
    cmd.size = I2C_SMBUS_BYTE_DATA;
    cmd.data = &data;

    daddress += length;

    while (length--) {
        cmd.command = --daddress;
        if (ioctl(file, I2C_SMBUS, &cmd)) {
            SAH_TRACEZ_ERROR(ME, "Error reading I2C byte at %02x!", daddress);
            return false;
        }
        buffer[length] = data.byte;
    }
    return true;
}

static float sfp_getCalibrationFloat(uint32_t value) {
    assert(sizeof(float) == sizeof(uint32_t));
    float result;
    memcpy(&result, &value, sizeof(float));
    return result;
}

static float sfp_getCalibrationSlope(uint16_t value) {
    float result = value >> 8;
    result += (value & 0xFF) / 256.0;
    return result;
}

static int16_t sfp_getCalibrationOffset(uint16_t value) {
    return (int16_t) value;
}

static bool sfp_readI2CDiagnosticCalibration() {
    bool success = false;
    int file = sfp_openI2CDev(0x51);
    if (file < 0) {
        SAH_TRACEZ_ERROR(ME, "Could open I2C device 0x51");
        goto error;
    }
    uint32_t tmp32;
    uint16_t tmp16;
    if (!sfp_readI2CDoubleWord(file, 56, &tmp32))
        goto error;
    sfp_i2c_calibration_mib.rx_pwr_4 = sfp_getCalibrationFloat(tmp32);
    if (!sfp_readI2CDoubleWord(file, 60, &tmp32))
        goto error;
    sfp_i2c_calibration_mib.rx_pwr_3 = sfp_getCalibrationFloat(tmp32);
    if (!sfp_readI2CDoubleWord(file, 64, &tmp32))
        goto error;
    sfp_i2c_calibration_mib.rx_pwr_2 = sfp_getCalibrationFloat(tmp32);
    if (!sfp_readI2CDoubleWord(file, 68, &tmp32))
        goto error;
    sfp_i2c_calibration_mib.rx_pwr_1 = sfp_getCalibrationFloat(tmp32);
    if (!sfp_readI2CDoubleWord(file, 72, &tmp32))
        goto error;
    sfp_i2c_calibration_mib.rx_pwr_0 = sfp_getCalibrationFloat(tmp32);
    if (!sfp_readI2CWord(file, 76, &tmp16))
        goto error;
    sfp_i2c_calibration_mib.tx_I_slope = sfp_getCalibrationSlope(tmp16);
    if (!sfp_readI2CWord(file, 78, &tmp16))
        goto error;
    sfp_i2c_calibration_mib.tx_I_offset = sfp_getCalibrationOffset(tmp16);
    if (!sfp_readI2CWord(file, 80, &tmp16))
        goto error;
    sfp_i2c_calibration_mib.tx_pwr_slope = sfp_getCalibrationSlope(tmp16);
    if (!sfp_readI2CWord(file, 82, &tmp16))
        goto error;
    sfp_i2c_calibration_mib.tx_pwr_offset = sfp_getCalibrationOffset(tmp16);
    if (!sfp_readI2CWord(file, 84, &tmp16))
        goto error;
    sfp_i2c_calibration_mib.T_slope = sfp_getCalibrationSlope(tmp16);
    if (!sfp_readI2CWord(file, 86, &tmp16))
        goto error;
    sfp_i2c_calibration_mib.T_offset = sfp_getCalibrationOffset(tmp16);
    if (!sfp_readI2CWord(file, 88, &tmp16))
        goto error;
    sfp_i2c_calibration_mib.V_slope = sfp_getCalibrationSlope(tmp16);
    if (!sfp_readI2CWord(file, 90, &tmp16))
        goto error;
    sfp_i2c_calibration_mib.V_offset = sfp_getCalibrationOffset(tmp16);
    success = true;
    SAH_TRACEZ_INFO(ME, "calibration rx_pwr_4 %f", sfp_i2c_calibration_mib.rx_pwr_4);
    SAH_TRACEZ_INFO(ME, "calibration rx_pwr_3 %f", sfp_i2c_calibration_mib.rx_pwr_3);
    SAH_TRACEZ_INFO(ME, "calibration rx_pwr_2 %f", sfp_i2c_calibration_mib.rx_pwr_2);
    SAH_TRACEZ_INFO(ME, "calibration rx_pwr_1 %f", sfp_i2c_calibration_mib.rx_pwr_1);
    SAH_TRACEZ_INFO(ME, "calibration rx_pwr_0 %f", sfp_i2c_calibration_mib.rx_pwr_0);
    SAH_TRACEZ_INFO(ME, "calibration tx_I_slope %f", sfp_i2c_calibration_mib.tx_I_slope);
    SAH_TRACEZ_INFO(ME, "calibration tx_I_offset %d", sfp_i2c_calibration_mib.tx_I_offset);
    SAH_TRACEZ_INFO(ME, "calibration tx_pwr_slope %f", sfp_i2c_calibration_mib.tx_pwr_slope);
    SAH_TRACEZ_INFO(ME, "calibration tx_pwr_offset %d", sfp_i2c_calibration_mib.tx_pwr_offset);
    SAH_TRACEZ_INFO(ME, "calibration T_slope %f", sfp_i2c_calibration_mib.T_slope);
    SAH_TRACEZ_INFO(ME, "calibration T_offset %d", sfp_i2c_calibration_mib.T_offset);
    SAH_TRACEZ_INFO(ME, "calibration V_slope %f", sfp_i2c_calibration_mib.V_slope);
    SAH_TRACEZ_INFO(ME, "calibration V_offset %d", sfp_i2c_calibration_mib.V_offset);
error:
    close(file);
    return success;
}

static bool sfp_readI2CMIB () {
    bool success = false;
    int file = sfp_openI2CDev(0x50);
    if (file < 0)
        return false;
    if (!sfp_readI2CChar(file, 20, sfp_i2c_mib.vendor_name, sizeof(sfp_i2c_mib.vendor_name)))
        goto error;
    if (!sfp_readI2CChar(file, 40, sfp_i2c_mib.vendor_pn, sizeof(sfp_i2c_mib.vendor_pn)))
        goto error;
#ifdef CONFIG_SAH_DRIVERS_XDSLPLUGIN_METANOIA_LIB
    if (!strncmp(sfp_i2c_mib.vendor_name, "METANOIA", sizeof("METANOIA")-1) &&
         (!strncmp(sfp_i2c_mib.vendor_pn, "MT2321", sizeof("MT2321")-1) ||
          !strncmp(sfp_i2c_mib.vendor_pn, "MT5321", sizeof("MT5321")-1))  ) {
        int metanoia_eeprom = sfp_openI2CDev(0x53);
        if (metanoia_eeprom >= 0) {
            if (!sfp_readI2CChar(metanoia_eeprom, 0, sfp_i2c_mib.vendor_sn,
                                 sizeof(sfp_i2c_mib.vendor_sn))) {
                sfp_i2c_mib.vendor_sn[0] = 0;
            }
            close(metanoia_eeprom);
        } else {
            sfp_i2c_mib.vendor_sn[0] = 0;
        }
    } else
#endif
    if (!sfp_readI2CChar(file, 68, sfp_i2c_mib.vendor_sn, sizeof(sfp_i2c_mib.vendor_sn)))
        goto error;
    if (!sfp_readI2CChar(file, 56, sfp_i2c_mib.vendor_rev, sizeof(sfp_i2c_mib.vendor_rev)))
        goto error;
    if (!sfp_readI2CByteArray(file, 37, sfp_i2c_mib.vendor_oui, sizeof(sfp_i2c_mib.vendor_oui)))
        goto error;
    if (!sfp_readI2CByte(file, 0, &sfp_i2c_mib.identifier))
        goto error;
    if (!sfp_readI2CByte(file, 2, &sfp_i2c_mib.connector))
        goto error;
    if (!sfp_readI2CByteArray(file, 3, sfp_i2c_mib.transceiver, sizeof(sfp_i2c_mib.transceiver)))
        goto error;
    if (!sfp_readI2CByte(file, 12, &sfp_i2c_mib.br_nominal))
        goto error;
    if (!sfp_readI2CWord(file, 60, &sfp_i2c_mib.wavelength))
        goto error;
    if (!sfp_readI2CByte(file, 92, &sfp_i2c_mib.diagnostic))
        goto error;
    sfp_supportsDiagnostics = sfp_i2c_mib.diagnostic & DMT_DIGITAL_DIAGNOSTIC_MONITORING_IMPLEMENTED;
    if (!sfp_readI2CByte(file, 93, &sfp_i2c_mib.options))
        goto error;
    if (!sfp_readI2CByte(file, 94, &sfp_i2c_mib.compliance))
        goto error;
    if (!sfp_readI2CChar(file, 84, sfp_i2c_mib.datecode, sizeof(sfp_i2c_mib.datecode)))
        goto error;
    if (sfp_supportsDiagnostics && (sfp_i2c_mib.diagnostic & DMT_EXTERNALLY_CALIBRATED)) {
        // diagnostic monitoring is implemented and external calibration is required
        sfp_i2c_calibration_mib.needsCalibration = true;
        if (!sfp_readI2CDiagnosticCalibration()) {
            SAH_TRACEZ_ERROR(ME, "Could not read diagnostic calibration data");
            goto error;
        }
    } else {
        sfp_i2c_calibration_mib.needsCalibration = false;
    }
    success = true;
error:
    close (file);
    return success;
}

static char* bin2hex(const uint8_t *src, char *dest, uint8_t size) {
    uint8_t i;
    for (i = 0; i < size; i++) {
        sprintf(dest + i*2, "%02hhX", src[i]);
    }
    return dest;
}

static void sfp_updateI2CMIB(object_t *object) {
    char buffer[17]; char *ascii_value;
    object_parameterSetCharValue(object, "SFP_VendorName", sfp_i2c_mib.vendor_name);
    object_parameterSetCharValue(object, "SFP_VendorPN", sfp_i2c_mib.vendor_pn);
    object_parameterSetCharValue(object, "SFP_VendorSN", sfp_i2c_mib.vendor_sn);
    object_parameterSetCharValue(object, "SFP_VendorRev", sfp_i2c_mib.vendor_rev);
    object_parameterSetCharValue(object, "SFP_VendorOUI", bin2hex(sfp_i2c_mib.vendor_oui, buffer, sizeof(sfp_i2c_mib.vendor_oui)));
    switch (sfp_i2c_mib.identifier) {
        case 0x00:    ascii_value = "Unknown"; break;
        case 0x01:    ascii_value = "GBIC"; break;
        case 0x02:    ascii_value = "Onboard"; break;
        case 0x03:    ascii_value = "SFP"; break;
        default:    ascii_value = bin2hex(&(sfp_i2c_mib.identifier), buffer, sizeof(sfp_i2c_mib.identifier)); break;
    }
    object_parameterSetCharValue(object, "SFP_Identifier", ascii_value);
    switch (sfp_i2c_mib.connector) {
        case 0x00:    ascii_value = "Unknown"; break;
        case 0x01:    ascii_value = "SC"; break;
        case 0x02:    ascii_value = "Fibre Channel Style 1 copper"; break;
        case 0x03:    ascii_value = "Fibre Channel Style 2 copper"; break;
        case 0x04:    ascii_value = "BNC/TNC"; break;
        case 0x05:    ascii_value = "Fibre Channel coaxial"; break;
        case 0x06:    ascii_value = "FiberJAck"; break;
        case 0x07:    ascii_value = "LC"; break;
        case 0x08:    ascii_value = "MT_RJ"; break;
        case 0x09:    ascii_value = "MU"; break;
        case 0x0a:    ascii_value = "SG"; break;
        case 0x0b:    ascii_value = "Optical pigtail"; break;
        case 0x20:    ascii_value = "HSSDC II"; break;
        case 0x21:    ascii_value = "Copper pigtail"; break;
        case 0x22:    ascii_value = "RJ45"; break;
        default:    ascii_value = bin2hex(&(sfp_i2c_mib.connector), buffer, sizeof(sfp_i2c_mib.connector)); break;
    }
    object_parameterSetCharValue(object, "SFP_Connector", ascii_value);
    object_parameterSetCharValue(object, "SFP_Transceiver", bin2hex(sfp_i2c_mib.transceiver, buffer, sizeof(sfp_i2c_mib.transceiver)));
    object_parameterSetUInt8Value(object, "SFP_BRnominal", sfp_i2c_mib.br_nominal);
    object_parameterSetUInt16Value(object, "SFP_Wavelength", sfp_i2c_mib.wavelength);
    object_parameterSetCharValue(object, "SFP_DiagnosticMonitoringType", bin2hex(&(sfp_i2c_mib.diagnostic), buffer, sizeof(sfp_i2c_mib.diagnostic)));
    object_parameterSetCharValue(object, "SFP_EnhancedOptions", bin2hex(&(sfp_i2c_mib.options), buffer, sizeof(sfp_i2c_mib.options)));
    object_parameterSetCharValue(object, "SFP_SFF8472Compliance", bin2hex(&(sfp_i2c_mib.compliance), buffer, sizeof(sfp_i2c_mib.compliance)));
    object_parameterSetCharValue(object, "SFP_Datecode", sfp_i2c_mib.datecode);
}

/**
 * Convert value in 0.1 uW to value in dBm/1000.
 */
static int32_t uWx10_to_dBmx1000(uint16_t uWx10)
{
    int32_t dBmx1000 = 0x80000000;
    if (uWx10) {
        double dBm = 10*log10(((double)uWx10)/10000);
        dBmx1000 = round(1000*dBm);
    }
    return dBmx1000;
}

/**
 * Apply correction factor to raw value.
 *
 * @param[in] rawValue : measured value
 * @param[in,out] cfactor : info about correction factor to be applied
 * @param[in] path : path to (proc) file with correction factor
 *
 * Do not apply a correction factor if:
 * - the (proc) file does not exist, or
 * - the function fails to open the file.
 *
 * @return rawValue with a correction factor applied or not
 */
static uint16_t sfp_handle_cfactor(uint16_t rawValue, sfp_cfactor_info *cfactor, const char *path)
{
    uint16_t result = rawValue;

    if (!cfactor->proc_file_handled) {
        if (access(path, F_OK) == 0) {
            FILE *file = fopen(path, "r");
            if (!file) {
                SAH_TRACEZ_ERROR(ME, "Failed to open %s: %s", path, strerror(errno));
                goto out;
            }
            uint16_t factor = 0;
            int rc = fscanf(file, "0x%hx", &factor);
            if (rc != 1) {
                SAH_TRACEZ_ERROR(ME, "Failed to parse %s: rc=%d", path, rc);
                fclose(file);
                goto out;
            }
            fclose(file);

            /* MSByte of the uint16_t is the multiplier, LSByte is the divisor. */
            cfactor->multiplier = (factor >> 8) & 0xFF;
            cfactor->divisor = (factor & 0xFF);
        }
    }

out:
    /* Always consider the proc file as handled, also when it's not present, or
     * when the function fails to open it.
     */
    cfactor->proc_file_handled = true;

    if (cfactor->multiplier) {
        result *= cfactor->multiplier;
    }
    if (cfactor->divisor) {
        result /= cfactor->divisor;
    }

    return result;
}


static int16_t sfp_calibrateTemprature(int16_t rawTemperature) {
    if (sfp_i2c_calibration_mib.needsCalibration)
        return sfp_i2c_calibration_mib.T_slope * rawTemperature +
                    sfp_i2c_calibration_mib.T_offset;
    else
        return rawTemperature;
}

static bool sfp_readTemperature(parameter_t *parameter, variant_t *value) {
    (void)parameter;
    int16_t temperature = (int16_t) 0x8000;
#ifdef CONFIG_SAH_DRIVERS_XDSLPLUGIN_METANOIA_LIB
    if (!strncmp(sfp_i2c_mib.vendor_name, "METANOIA", sizeof("METANOIA")-1) &&
         (!strncmp(sfp_i2c_mib.vendor_pn, "MT2321", sizeof("MT2321")-1) ||
          !strncmp(sfp_i2c_mib.vendor_pn, "MT5321", sizeof("MT5321")-1)) &&
        !old_SFP_ModDef0) {
        int file = -1;
        bool success = false;
        if (!success && (file = sfp_openI2CDev(0x1b)) > 0) { // Huawei design
            struct i2c_smbus_ioctl_data cmd;
            union i2c_smbus_data data;

            cmd.read_write = I2C_SMBUS_READ;
            cmd.command = 0x05;
            cmd.size = I2C_SMBUS_I2C_BLOCK_DATA;
            cmd.data = &data;
            data.block[0] = 2;

            if (ioctl(file, I2C_SMBUS, &cmd) >= 0) {
                data.block[1] |= (data.block[1] & 0x10 ? 0x08 : 0); // copy sign bit
                temperature = ((data.block[1] << 8) + data.block[2]) << 4;
                success = true;
            }
            close (file);
        }
        if (!success && (file = sfp_openI2CDev(0x48)) > 0) { // Metanoia reference design
            // we need to do a real word read (2 byte reads at consecutive addresses
            // yield a different result for this device)
            struct i2c_smbus_ioctl_data cmd;
            union i2c_smbus_data data;

            cmd.read_write = I2C_SMBUS_READ;
            cmd.command = 0x00;
            cmd.size = I2C_SMBUS_WORD_DATA;
            cmd.data = &data;

            if (ioctl(file, I2C_SMBUS, &cmd) >= 0) {
                // the I2C driver returns little-endian values when reading words
                temperature = (int16_t)((data.word << 8) + (data.word >> 8));
                success = true;
            }
            close (file);
        }
        if (!success) {
            SAH_TRACEZ_ERROR(ME, "Error reading Temperature!");
        }
    } else
#endif
    if (!old_SFP_ModDef0 && sfp_supportsDiagnostics) {
        int file = sfp_openI2CDev(0x51);
        if (file < 0) {
            SAH_TRACEZ_ERROR(ME, "Error reading Temperature!");
        } else {
            uint16_t temperature_i2c;
            if (!sfp_readI2CWord(file, 96, &temperature_i2c)) {
                SAH_TRACEZ_ERROR(ME, "Error reading Temperature!");
            } else {
                temperature = sfp_calibrateTemprature((int16_t)temperature_i2c);
            }
            close (file);
        }
    }
    variant_setInt16(value, temperature);
    return true;
}

static uint16_t sfp_calibrateVcc(uint16_t rawVcc) {
    if (sfp_i2c_calibration_mib.needsCalibration)
        return sfp_i2c_calibration_mib.V_slope * rawVcc +
                    sfp_i2c_calibration_mib.V_offset;
    else
        return rawVcc;
}

static bool sfp_readVcc(parameter_t *parameter, variant_t *value) {
    (void)parameter;
    uint16_t vcc = 0;
    if (!old_SFP_ModDef0 && sfp_supportsDiagnostics) {
        int file = sfp_openI2CDev(0x51);
        if (file < 0) {
            SAH_TRACEZ_ERROR(ME, "Error reading Voltage!");
        } else {
            uint16_t vcc_i2c;
            if (!sfp_readI2CWord(file, 98, &vcc_i2c)) {
                SAH_TRACEZ_ERROR(ME, "Error reading Voltage!");
            } else {
                vcc = sfp_calibrateVcc(vcc_i2c);
            }
            close (file);
        }
    }
    variant_setUInt16(value, vcc);
    return true;
}

static uint32_t sfp_calibrateCurrent(uint32_t rawCurrent) {
    if (sfp_i2c_calibration_mib.needsCalibration)
        return sfp_i2c_calibration_mib.tx_I_slope * rawCurrent +
                    sfp_i2c_calibration_mib.tx_I_offset;
    else
        return sfp_handle_cfactor(rawCurrent, &s_cfactor_current, TRX_CORR_FACTOR_CURRENT);
}

static bool sfp_readTxBias(parameter_t *parameter, variant_t *value) {
    (void)parameter;
    uint32_t current = 0;
    if (!old_SFP_ModDef0 && sfp_supportsDiagnostics) {
        int file = sfp_openI2CDev(0x51);
        if (file < 0) {
            SAH_TRACEZ_ERROR(ME, "Error reading TxBias!");
        } else {
            uint16_t current_i2c;
            if (!sfp_readI2CWord(file, 100, &current_i2c)) {
                SAH_TRACEZ_ERROR(ME, "Error reading TxBias!");
            } else {
                current = 2 * sfp_calibrateCurrent((uint32_t)current_i2c);
            }
            close (file);
        }
    }
    variant_setUInt32(value, current);
    return true;
}

static uint16_t sfp_calibrateTxPower(uint16_t rawPower) {
    if (sfp_i2c_calibration_mib.needsCalibration)
        return sfp_i2c_calibration_mib.tx_pwr_slope * rawPower +
                    sfp_i2c_calibration_mib.tx_pwr_offset;
    else
        return sfp_handle_cfactor(rawPower, &s_cfactor_tx_pwr, TRX_CORR_FACTOR_TX_PWR);
}

static bool sfp_readTxPower(parameter_t *parameter, variant_t *value) {
    (void)parameter;
    int32_t txpower = 0x80000000;
    if (!old_SFP_ModDef0 && sfp_supportsDiagnostics) {
        int file = sfp_openI2CDev(0x51);
        if (file < 0) {
            SAH_TRACEZ_ERROR(ME, "Error reading Tx Power!");
        } else {
            uint16_t txpower_i2c;
            if (!sfp_readI2CWord(file, 102, &txpower_i2c)) {
                SAH_TRACEZ_ERROR(ME, "Error reading Tx Power!");
            } else {
                txpower_i2c = sfp_calibrateTxPower(txpower_i2c);
                txpower = uWx10_to_dBmx1000(txpower_i2c);
            }
            close (file);
        }
    }
    variant_setInt32(value, txpower);
    return true;
}

static uint16_t sfp_calibrateRxPower(uint16_t rawPower) {
    if (sfp_i2c_calibration_mib.needsCalibration) {
        uint32_t rawPower_2 = rawPower * rawPower;
        uint64_t rawPower_3 = rawPower_2 * rawPower;
        uint64_t rawPower_4 = rawPower_3 * rawPower;
        return (uint16_t)(sfp_i2c_calibration_mib.rx_pwr_4 * rawPower_4) +
               (uint16_t)(sfp_i2c_calibration_mib.rx_pwr_3 * rawPower_3) +
               (uint16_t)(sfp_i2c_calibration_mib.rx_pwr_2 * rawPower_2) +
               (uint16_t)(sfp_i2c_calibration_mib.rx_pwr_1 * rawPower) +
               sfp_i2c_calibration_mib.rx_pwr_0;
    } else {
        return rawPower;
    }
}

static bool sfp_readRxPower(parameter_t *parameter, variant_t *value) {
    (void)parameter;
    int32_t rxpower = 0x80000000;
    if (!old_SFP_ModDef0 && sfp_supportsDiagnostics) {
        int file = sfp_openI2CDev(0x51);
        if (file < 0) {
            SAH_TRACEZ_ERROR(ME, "Error reading Rx Power!");
        } else {
            uint16_t rxpower_i2c;
            if (!sfp_readI2CWord(file, 104, &rxpower_i2c)) {
                SAH_TRACEZ_ERROR(ME, "Error reading Rx Power!");
            } else {
                rxpower_i2c = sfp_calibrateRxPower(rxpower_i2c);
                rxpower = uWx10_to_dBmx1000(rxpower_i2c);
            }
            close (file);
        }
    }
    variant_setInt32(value, rxpower);
    return true;
}

static bool sfp_readType(parameter_t *parameter, variant_t *value) {

    (void)parameter;
    char * result = "Unknown";
    const int BUFFER_LEN = 32;
    char buffer[BUFFER_LEN];

    if (access(TRX_FORM_FACTOR, F_OK) != 0) {
        SAH_TRACEZ_INFO(ME, "%s does not exist", TRX_FORM_FACTOR);
        goto set_sfp_type;
    }
    FILE * file = fopen(TRX_FORM_FACTOR, "r");
    if (!file) {
        SAH_TRACEZ_ERROR(ME, "Failed to open %s: %s", TRX_FORM_FACTOR, strerror(errno));
        goto set_sfp_type;
    }
    if (fgets(buffer, 4, file) == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to read %s", TRX_FORM_FACTOR);
        fclose(file);
        goto set_sfp_type;
    }
    fclose(file);
    if (strncmp(buffer, "SFP", 3)) {
        SAH_TRACEZ_INFO(ME, "Form factor is not SFP");
        goto set_sfp_type;
    }
    if (access(TRX_TYPE, F_OK) != 0) {
        SAH_TRACEZ_ERROR(ME, "%s does not exist", TRX_TYPE);
        goto set_sfp_type;
    }
    file = fopen(TRX_TYPE, "r");
    if (!file) {
        SAH_TRACEZ_ERROR(ME, "Failed to open %s: %s", TRX_TYPE, strerror(errno));
        goto set_sfp_type;
    }
    if (fgets(buffer, BUFFER_LEN, file) == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to read %s", TRX_TYPE);
        fclose(file);
        goto set_sfp_type;
    }
    fclose(file);
    if (strncmp(buffer, "xPON", 4) == 0) {
        result = "xPON";
    }
    else if (strncmp(buffer, "ETHERNET", 8) == 0) {
        result = "Ethernet";
    }

set_sfp_type:
    variant_setChar(value, result);
    /* Caller of this function doesn't check return type. Let's always return true. */
    return true;
}

static function_exec_state_t sfp_writeTXDisable(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
    (void)retval;
    bool value;

    argument_getBool(&value, args, request_attributes(fcall_request(fcall)), "value", false);

    if (!sfp_writeGPIO("/var/dev/gpio/SFP_TXDisable", (int)value)) {
        fcall_addErrorReply(fcall, 1, "Error writing to TXDisable GPIO!", "");
        return function_exec_error;
    }
    return function_exec_done;
}


static void sfp_poll(pcb_timer_t *timer, void *userdata) {
    (void)timer;
    intf_blob_t *sfp = (intf_blob_t *)userdata;
    if (!sfp) {
        SAH_TRACEZ_WARNING(ME, "user data is null");
        return;
    }

    object_t *object = plugin_intf2object(sfp->intf);

    long new_SFP_ModDef0 = sfp_readGPIO(sfp_moddef0);
    long new_SFP_LOS = sfp_readGPIO(sfp_los);
    long new_SFP_TXFault = sfp_readGPIO(sfp_txfault);
    static bool mib_available = false;
    bool mib_needsUpdate = false;

    if (new_SFP_ModDef0 != old_SFP_ModDef0) {
        if (new_SFP_ModDef0) { // SFP was unplugged
            SAH_TRACEZ_INFO(ME, "SFP unplugged");
            memset(&sfp_i2c_mib, 0, sizeof(sfp_i2c_mib));
            mib_available = true;
            mib_needsUpdate = true;
        } else { // SFP was plugged in
            SAH_TRACEZ_INFO(ME, "SFP plugged in");
            mib_available = false;
        }
        memset(&s_cfactor_tx_pwr, 0, sizeof(s_cfactor_tx_pwr));
        memset(&s_cfactor_current, 0, sizeof(s_cfactor_current));
    }
    if (!mib_available && sfp_readI2CMIB()) {
        SAH_TRACEZ_INFO(ME, "MIB read successfully");
        mib_needsUpdate = true;
        mib_available = true;
    }

    if (mib_needsUpdate || (old_SFP_ModDef0 != new_SFP_ModDef0) || (old_SFP_LOS != new_SFP_LOS) || (old_SFP_TXFault != new_SFP_TXFault)) {
        if (old_SFP_ModDef0 != new_SFP_ModDef0) {
            object_parameterSetInt32Value(object, "SFP_ModDef0", new_SFP_ModDef0);
        }
        if (old_SFP_LOS != new_SFP_LOS) {
            object_parameterSetInt32Value(object , "SFP_LOS", new_SFP_LOS);
        }
        if (old_SFP_TXFault != new_SFP_TXFault) {
            object_parameterSetInt32Value(object, "SFP_TXFault", new_SFP_TXFault);
        }

        if (mib_needsUpdate) {
            sfp_updateI2CMIB(object);
        }

        object_commit(object);
    }

    old_SFP_ModDef0 = new_SFP_ModDef0;
    old_SFP_LOS = new_SFP_LOS;
    old_SFP_TXFault = new_SFP_TXFault;
}

static void sfp_setFlag(pcb_timer_t *timer, void *userdata) {
    intf_blob_t *sfp = (intf_blob_t *)userdata;
    if (sfp) {
        const char *name = intf_name(sfp->intf);
        SAH_TRACEZ_NOTICE(ME, "[%s]set sfp flag", name);
        nemo_setFlag(name, "sfp", "!sfp", nemo_traverse_this);
    }
    pcb_timer_destroy(timer);
}

static void sfp_clearFlag(pcb_timer_t *timer, void *userdata) {
    intf_blob_t *sfp = (intf_blob_t *)userdata;
    if (sfp) {
        const char *name = intf_name(sfp->intf);
        SAH_TRACEZ_NOTICE(ME, "[%s]clear sfp flag", name);
        nemo_clearFlag(name, "sfp", "sfp", nemo_traverse_this);
    }
    pcb_timer_destroy(timer);
}

static void sfp_timed_cfgFlag(intf_blob_t *sfp, bool enable) {
    if (!sfp) {
        return;
    }

    pcb_timer_t *t = pcb_timer_create();
    pcb_timer_setUserData(t, sfp);

    if (enable) {
        pcb_timer_setHandler(t, sfp_setFlag);
    }
    else {
        pcb_timer_setHandler(t, sfp_clearFlag);
    }
    //make sure the setFlag, clearFlag does not happen in a query callback.
    pcb_timer_start(t, 0);
}

static void physupHandler(const variant_t *result, void *userdata) {
    intf_blob_t *sfp = (intf_blob_t*)userdata;
    bool physup = variant_bool(result);
    if (!sfp) {
        SAH_TRACEZ_WARNING(ME, "Missing sfp user data");
        return;
    }

    const char *name = intf_name(sfp->intf);
    if (physup) {
        unsigned long detectSFP = sfp_readGPIO(sfp_moddef0);
        if (detectSFP == 0) {
            SAH_TRACEZ_WARNING(ME, "[%s] up, SFP[%lu] detected", name, detectSFP);
            sfp_timed_cfgFlag(sfp, true);
        }
        else {
            SAH_TRACEZ_WARNING(ME, "[%s] up, no SFP[%lu] detected", name, detectSFP);
            sfp_timed_cfgFlag(sfp, false);
        }
    }
    else {
        SAH_TRACEZ_NOTICE(ME, "Intf[%s] goes down", name);
    }
}

static void sfpdetect_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
    (void)userdata; (void)flag;
    intf_blob_t *blob = intf_getBlob(intf, sfp_blobkey);
    if (value) {
        if (!blob) {
            blob = calloc(1, sizeof(intf_blob_t));
            if (!blob) {
                SAH_TRACE_ERROR("sfp - calloc failed");
                return;
            }
            intf_addBlob(intf, sfp_blobkey, blob);
            blob->intf = intf;
        }

        blob->physupquery = nemo_openQuery_isUp(intf_name(intf), ME, "netdev-up", "down", physupHandler, blob);
    }
    else {
        if (!blob) {
            SAH_TRACE_ERROR("sfp - blob is NULL");
            return;
        }

        if (!blob->physupquery) {
            nemo_closeQuery(blob->physupquery);
            blob->physupquery = NULL;
        }

        /* Delete the blob, if both query(sfp) and isupquery(sfp-detect) are down */
        if (!blob->sfp_monitor && !blob->physupquery) {
            intf_delBlob(intf, sfp_blobkey);
            free(blob);
        }
    }
}

static void sfp_registerReadHandlers(object_t *object) {
    char *path = object_pathChar(object, path_attr_key_notation);
    SAH_TRACEZ_WARNING(ME, "Registering SFP read handlers [%s]", path);
    free(path);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_Temperature"), sfp_readTemperature);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_Voltage"), sfp_readVcc);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_TxBias"), sfp_readTxBias);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_TxPower"), sfp_readTxPower);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_RxPower"), sfp_readRxPower);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_Type"), sfp_readType);
    function_setHandler(object_getFunction(object, "setSFP_TXDisable"), sfp_writeTXDisable);
}

static void sfp_unregisterReadHandlers(object_t *object) {
    char *path = object_pathChar(object, path_attr_key_notation);
    SAH_TRACEZ_WARNING(ME, "Unregistering SFP read handlers [%s]", path);
    free(path);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_Temperature"), NULL);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_Voltage"), NULL);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_TxBias"), NULL);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_TxPower"), NULL);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_RxPower"), NULL);
    plugin_setParameterReadHandler(object_getParameter(object, "SFP_Type"), NULL);
    function_setHandler(object_getFunction(object, "setSFP_TXDisable"), NULL);
}

static void sfp_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
    (void)userdata; (void)flag;
    object_t *object = plugin_intf2object(intf);
    intf_blob_t *blob = intf_getBlob(intf, sfp_blobkey);

    if (value) {
        if (!blob) {
            blob = calloc(1, sizeof(intf_blob_t));
            if (!blob) {
                SAH_TRACE_ERROR("sfp - calloc failed");
                return;
            }
            intf_addBlob(intf, sfp_blobkey, blob);
            blob->intf = intf;
        }

        if (!blob->sfp_monitor) {
            blob->sfp_monitor = pcb_timer_create();
#ifndef CONFIG_SAH_SERVICES_NEMO_SFP_POLL_INTERVAL
#define CONFIG_SAH_SERVICES_NEMO_SFP_POLL_INTERVAL 2000
#endif
            pcb_timer_setInterval(blob->sfp_monitor, CONFIG_SAH_SERVICES_NEMO_SFP_POLL_INTERVAL);
            pcb_timer_setUserData(blob->sfp_monitor, blob);
            pcb_timer_setHandler(blob->sfp_monitor, sfp_poll);
        }

        sfp_registerReadHandlers(object);
        object_parameterSetInt32Value(object, "SFP_ModDef0", old_SFP_ModDef0);
        object_parameterSetInt32Value(object, "SFP_LOS", old_SFP_LOS);
        object_parameterSetInt32Value(object, "SFP_TXFault", old_SFP_TXFault);
        object_commit(object);

        old_SFP_ModDef0 = old_SFP_LOS = old_SFP_TXFault = -1;
        pcb_timer_start(blob->sfp_monitor, 0);
    } else {
        if (!blob) {
            SAH_TRACE_WARNING("sfp - no blob");
            return;
        }

        sfp_unregisterReadHandlers(object);

        if (blob->sfp_monitor) {
            pcb_timer_destroy(blob->sfp_monitor);
            blob->sfp_monitor = NULL;
        }

        if (!blob->sfp_monitor && !blob->physupquery) {
            intf_delBlob(intf, sfp_blobkey);
            free(blob);
        }
    }
}

static bool sfp_start(argument_value_list_t *args) {
    (void)args;
    //sahTraceAddZone(400, ME);
    SAH_TRACEZ_INFO(ME, "Initializing module");

    /*
     * Detect if we are using the old API style i.e. "/var/dev/gpio/SFP_ModDef0"
     * or the new style "/var/dev/gpio/SFP_ModDef0/value"
     */
    struct stat st;
    memset(&st, 0, sizeof(st));
    int res = stat(sfp_moddef0, &st);
    if (res == 0) {
        if (S_ISDIR(st.st_mode)) {
            SAH_TRACEZ_NOTICE(ME, "Use new style API");
            sfp_moddef0 = "/var/dev/gpio/SFP_ModDef0/value";
            sfp_los = "/var/dev/gpio/SFP_LOS/value";
            sfp_txfault = "/var/dev/gpio/SFP_TXFault/value";
        }
        else if (S_ISLNK(st.st_mode)) {
            SAH_TRACEZ_NOTICE(ME, "Use old style API");
        }
    }
    else {
        SAH_TRACEZ_ERROR(ME, "Failed stat[%s] (error[%s])", sfp_moddef0, strerror(errno));
    }

    intf_addFlagListener(NULL, "sfp", sfp_rootHandler, NULL, true);
    intf_addFlagListener(NULL, "sfp-detect", sfpdetect_rootHandler, NULL, true);
    return true;
}

static void sfp_stop() {
    SAH_TRACEZ_INFO(ME, "Cleaning up module");
    intf_delFlagListener(NULL, "sfp-detect", sfpdetect_rootHandler, NULL, true);
    intf_delFlagListener(NULL, "sfp", sfp_rootHandler, NULL, true);
}

static mtk_module_info_t sfp_info = {
    .name  = ME,
    .start = sfp_start,
    .stop  = sfp_stop
};

__attribute__((constructor)) static void sfp_init() {
    mtk_module_register(&sfp_info);
}

