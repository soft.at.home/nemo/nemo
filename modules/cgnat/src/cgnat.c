/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>

#include <nemo/core.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>

#define ME            "cgnat"

#define CGNAT_FLAG    "cgnat"

struct _intf_blob {
    intf_t *intf;
    nemo_query_t *query;
};

static int cgnat_dummy;
static void *cgnat_blobkey = &cgnat_dummy;

static void cgnat_setFlag(intf_t *intf, bool set) {
    flagset_t *flagset;

    flagset = intf_flagset(intf);
    if (set) {
        SAH_TRACEZ_INFO(ME, "Setting CGN flag on interface %s", intf_name(intf));
        flagset_set(flagset, CGNAT_FLAG);
    }
    else {
        SAH_TRACEZ_INFO(ME, "Clearing CGN flag on interface %s", intf_name(intf));
        flagset_clear(flagset, CGNAT_FLAG);
    }
}

static void cgnat_queryHandler(const variant_t *result, void *userdata) {
    intf_blob_t *blob;
    blob = (intf_blob_t *)userdata;

    if (!result) {
        SAH_TRACEZ_NOTICE(ME, "No CGN for interface %s", intf_name(blob->intf));
        cgnat_setFlag(blob->intf, false);
        return;
    }

    if (!variant_list_isEmpty(variant_da_list(result))) {
        SAH_TRACEZ_NOTICE(ME, "Carrier Grade NAT IPv4 address detected on interface %s", intf_name(blob->intf));
        cgnat_setFlag(blob->intf, true);
        return;
    }

    cgnat_setFlag(blob->intf, false);
}

static void cgnat_flagHandler(intf_t *intf, const char *flag __attribute__((unused)), bool value, void *userdata __attribute__((unused))) {
    variant_t var;
    argument_value_list_t args;
    intf_blob_t *blob = NULL;

    if (value) {
        SAH_TRACEZ_INFO(ME, "Interface %s has ipv4-up flag set", intf_name(intf));

        blob = calloc(1, sizeof(struct _intf_blob));
        if (!blob) {
            SAH_TRACEZ_ERROR(ME, "Unable to allocate memory for CGN detection");
            return;
        }

        blob->intf = intf;
        intf_addBlob(intf, cgnat_blobkey, blob);
        argument_value_list_initialize(&args);
        variant_initialize(&var, variant_type_string);
        variant_setChar(&var, "@cgn");
        argument_valueAdd(&args, "flag", &var);
        variant_setChar(&var, nemo_traverse_this);
        argument_valueAdd(&args, "traverse", &var);
        variant_cleanup(&var);
        blob->query = nemo_openQuery(intf_name(intf), ME, "getAddrs", &args, request_function_args_by_name, cgnat_queryHandler, blob);
        argument_valueClear(&args);
        if (!blob->query) {
            SAH_TRACEZ_ERROR(ME, "Unable to create NeMo query for CGN detection");
        }
    }
    else {
        SAH_TRACEZ_INFO(ME, "Interface %s has ipv4-up flag cleared", intf_name(intf));

        blob = intf_getBlob(intf, cgnat_blobkey);
        if (blob) {
            cgnat_setFlag(intf, false);
            if (blob->query != NULL) {
               nemo_closeQuery(blob->query);
            }
            intf_delBlob(intf, cgnat_blobkey);
            free(blob);
            blob = NULL;
        }
    }
}

static bool cgnat_start(argument_value_list_t *args __attribute__((unused))) {
    SAH_TRACEZ_INFO(ME, "Initializing cgnat module");
    intf_addFlagListener(NULL, "ipv4-up", cgnat_flagHandler, NULL, true);
    return true;
}

static void cgnat_stop() {
    SAH_TRACEZ_INFO(ME, "Cleaning up cgnat module");
    intf_delFlagListener(NULL, "ipv4-up", cgnat_flagHandler, NULL, true);
}

static mtk_module_info_t cgnat_info = {
    .name  = ME,
    .start = cgnat_start,
    .stop  = cgnat_stop
};

__attribute__((constructor)) static void cgnat_init() {
    mtk_module_register(&cgnat_info);
}

