/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <nemo/core.h>
#include <pcb/core.h>
#include <pcb/utils.h>
#include <mtk.h>

#include "dsl_common.h"

//static peer_info_t *dst = NULL;
static char *xtmroot = NULL;
static char *dslroot = NULL;

static char *get_dsl_ipc(object_t *object) {
    const char *ret = object_da_parameterCharValue(object, "DSLIPC");

    if (!ret || strcmp(ret, "") == 0) {
        return strdup("pcb://ipc:[/var/run/dsl]");
    }

    char *buf = NULL;

    if (asprintf(&buf, "pcb://ipc:[%s]", ret) == -1) {
        SAH_TRACE_ERROR("ptm - asprintf failed");
        return NULL;
    }

    return buf;
}

/* 
 * Check if 'value' is in datamodel AND is not empty
 */
static const char *get_value_by_dm(object_t *object, const char *value) {
    const char *ret = object_da_parameterCharValue(object, value);

    if (!ret || strcmp(ret, "") == 0) {
        return NULL;
    }

    return ret;
}

const char *get_dslroot_by_dm(object_t *object) {
    return get_value_by_dm(object, "DSLPlugin");
}

const char *get_xtmroot_by_dm(object_t *object) {
    return get_value_by_dm(object, "XTMPlugin");
}

const char *get_dslroot(object_t *object) {
    const char *ret = get_dslroot_by_dm(object);

    if (ret) {
        return ret;
    }

    return dslroot;
}

const char *get_xtmroot(object_t *object) {
    const char *ret = get_xtmroot_by_dm(object);

    if (ret) {
        return ret;
    }

    return xtmroot;
}

void set_dslroot_by_args(argument_value_list_t *args) {
    arg_takeChar(&dslroot, args, ARG_BYNAME, "dslroot");
	if (!dslroot || !*dslroot) {
		if (dslroot) free(dslroot);
		dslroot = strdup("DSL_LEGACY");
	}
}

void set_xtmroot_by_args(argument_value_list_t *args) {
    arg_takeChar(&xtmroot, args, ARG_BYNAME, "xtmroot");
	if (!xtmroot || !*xtmroot) {
		if (xtmroot) free(xtmroot);
		xtmroot = strdup("XTM_LEGACY");
	}
}

static peer_info_t *init_destination(object_t *object) {
	char *dsturi = get_dsl_ipc(object);
	uri_t *uri = NULL;
	peer_info_t *peer = NULL;
	if (dsturi && *dsturi) {
		peer = connection_connect(pcb_connection(plugin_getPcb()), dsturi, &uri);
		if (!peer) {
			SAH_TRACE_ERROR("xtm - Connection to %s failed - fallback to sysbus", dsturi);
			// no fatal error, we still have fallback over the sysbus.
		}
		if (uri) {
			uri_destroy(uri);
		}
	}
	free(dsturi);
	if (!peer) {
		peer = connection_connect(pcb_connection(plugin_getPcb()), "pcb://ipc:[/var/run/dsl]", &uri);
		if (!peer) {
			SAH_TRACE_ERROR("xtm - Connection to %s failed - fallback to sysbus", dsturi);
			// no fatal error, we still have fallback over the sysbus.
		}
		if (uri) {
			uri_destroy(uri);
		}

	}
	if (!peer) {
		SAH_TRACE_NOTICE("Connection to %s failed - use sysbus", dsturi);
		peer = plugin_getPeer();
	}
	return peer;
}

peer_info_t *get_dst(object_t *object, peer_info_t **dst) {
    if (*dst) {
        return *dst;
    }

    *dst = init_destination(object);
    return *dst;
}

void start_dsl_common() {
}

void stop_dsl_common() {
    free(dslroot); dslroot = NULL;
    free(xtmroot); xtmroot = NULL;
}
