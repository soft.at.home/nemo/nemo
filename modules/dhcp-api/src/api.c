/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "dhcp-api"

#include <nemo/core.h>

#include <dhcpoption.h>

typedef enum _optiontype {
	optiontype_req = 0,
	optiontype_sent = 1,
	optiontype_req6 = 2,
	optiontype_sent6 = 3,
} optiontype_t;

typedef struct _dhcpoptionCtx {
	variant_t *retval;
	optiontype_t type;
	uint16_t tag;
} dhcpoptionCtx_t;

static const char *dhcp_api_optiontype2str(optiontype_t type) {
	return
		type == optiontype_req ?   "req"   :
		type == optiontype_sent ?  "sent"  :
		type == optiontype_req6 ?  "req6"  :
		type == optiontype_sent6 ? "sent6" :
		"<unknown>";
}

static bool dhcp_api_getDHCPOptionStep(intf_t *intf, void *userdata) {
	dhcpoptionCtx_t *ctx = (dhcpoptionCtx_t *)userdata; 
	
	// Check if this is a dhcp client to be looked at
	if (!flagset_check(intf_flagset(intf), (ctx->type & 2)?"dhcpv6":"dhcp"))
		return false;
	
	// Find the right option template object
	object_t *template = NULL;
	if (ctx->type == optiontype_sent || ctx->type == optiontype_sent6) {
		template = object_getObject(plugin_intf2object(intf), "SentOption", 0, NULL);
	} else if (ctx->type == optiontype_req) {
		template = object_getObject(plugin_intf2object(intf), "ReqOption", 0, NULL);
	} else if (ctx->type == optiontype_req6) {
		template = object_getObject(plugin_intf2object(intf), "ReceivedOption", 0, NULL);
	}
	if (!template) {
		SAH_TRACE_ERROR("dhcp-api - Template object for option type \"%s\" not found", dhcp_api_optiontype2str(ctx->type));
		return false;
	}

	// if option may appear multiple times -> prepare a list to put option instances in
	bool stop = false;
	bool allowMultiple = (ctx->type & 2) && dhcpoption_v6allowMultiple(ctx->tag);
	variant_t tmpval;
	variant_initialize(&tmpval, variant_type_string);
	variant_t *retval = ctx->retval;
	if (allowMultiple) {
		if (variant_type(ctx->retval) != variant_type_array)
			variant_setType(ctx->retval, variant_type_array);
		retval = &tmpval;
	}
	
	// iterate over options
	object_t *object;
	object_for_each_instance(object, template) {
		if (object_parameterUInt8Value(object, "Tag") != ctx->tag || object_state(object) == object_state_deleted)
			continue;
		
		const char *hexval = string_buffer(variant_da_string(object_parameterValue(object, "Value")));
		if (!hexval)
			hexval = "";
		
		// translate from hexadecimal notation to raw binary string
		int len = strlen(hexval) / 2;
		unsigned char *binval = calloc(len+1, 1);
		if (!binval) {
			SAH_TRACE_ERROR("dhcp-api - calloc(%d) failed.", len + 1);
			continue;
		}
		int i;
		for (i=0; i<len; i++)
#define hex2num(c) ((c>='0' && c<='9') ? c-'0' : (c>='a' && c<='f') ? c-'a'+0xa : (c>='A' && c<='F') ? c-'A'+0xA : 0)
			binval[i] = hex2num(hexval[i*2]) << 4 | hex2num(hexval[i*2+1]);

		// actual parsing done by v4option_parse for DHCPv4 options and v6option_parse for DHCPv6 options
		if (ctx->type & 2) {
			dhcpoption_v6parse(retval, ctx->tag, len, binval);
		} else {
			dhcpoption_v4parse(retval, ctx->tag, len, binval);
		}
		
		free(binval);
		
		if (allowMultiple) {
			// add parsed option to list
			variant_list_add(variant_da_list(ctx->retval), &tmpval);
			variant_setType(&tmpval, variant_type_string);
		} else {
			stop = true;
			break;
		}
	}
	variant_cleanup(&tmpval);
	return stop;
}

static void dhcp_api_getDHCPOptionImpl(variant_t *retval, intf_t *intf, optiontype_t type, uint16_t tag, traverse_mode_t traverse) {
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	dhcpoptionCtx_t ctx = {retval, type, tag};
	traverse_tree_walk(tree, dhcp_api_getDHCPOptionStep, &ctx);
	traverse_tree_destroy(tree);
}

static bool dhcp_api_arg_parseDHCPOptionType(optiontype_t *target, const char *string, uint32_t attributes, const char *name) {
	if (!string && (attributes & ARG_MANDATORY)) {
		SAH_TRACE_WARNING("dhcp-api - Mandatory argument %s missing", name?name:"(null)");
		return arg_returnError(pcb_error_invalid_value, "Mandatory argument missing", name);
	}
	if (!string)
		return arg_returnSuccess();
	if (!strcmp(string, "req")) {
		*target = optiontype_req;
	} else if (!strcmp(string, "sent")) {
		*target = optiontype_sent;
	} else if (!strcmp(string, "req6")) {
		*target = optiontype_req6;
	} else if (!strcmp(string, "sent6")) {
		*target = optiontype_sent6;
	} else {
		SAH_TRACE_WARNING("dhcp-api - Not a valid DHCP option type: %s", string);
		return arg_returnError(pcb_error_invalid_value, "Not a valid DHCP option type (choose \"req\", \"sent\", \"req6\" or \"sent6\")", string);
	}
	return arg_returnSuccess();
}

static bool dhcp_api_arg_takeDHCPOptionType(optiontype_t *target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	char *arg = NULL;
	if (!arg_takeChar(&arg, args, attributes, name))
		return false;
	bool ret = dhcp_api_arg_parseDHCPOptionType(target, arg, attributes, name);
	free(arg);
	return ret;
}

static function_exec_state_t dhcp_api_getDHCPOption(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	optiontype_t type = optiontype_req;
	uint16_t tag = 0;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!dhcp_api_arg_takeDHCPOptionType(&type, args, attr | ARG_MANDATORY, "type"))
		goto leave;
	if (!arg_takeUInt16(&tag, args, attr | ARG_MANDATORY, "tag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	variant_setType(retval, variant_type_string);
	dhcp_api_getDHCPOptionImpl(retval, plugin_object2intf(fcall_object(fcall)), type, tag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;	
}

struct _query_args {
	optiontype_t type;
	uint16_t tag;
	traverse_mode_t traverse;
};

static void dhcp_api_query_args_destroy(query_class_t *cl, query_args_t *args) {
	(void)cl;
	free(args);
}

static bool dhcp_api_query_args_convert(query_class_t *cl, query_args_t **args, argument_value_list_t *arglist, uint32_t attributes) {
	*args = calloc(1, sizeof(query_args_t));
	(*args)->traverse = traverse_mode_down;
	if (!dhcp_api_arg_takeDHCPOptionType(&(*args)->type, arglist, attributes | ARG_MANDATORY, "type"))
		goto error;
	if (!arg_takeUInt16(&(*args)->tag, arglist, attributes | ARG_MANDATORY, "tag"))
		goto error;
	if (!arg_takeTraverse(&(*args)->traverse, arglist, attributes, "traverse"))
		goto error;
	return true;
error:
	dhcp_api_query_args_destroy(cl, *args);
	*args = NULL;
	return false;
}

static bool dhcp_api_query_args_compare(query_class_t *cl, query_args_t *args1, query_args_t *args2) {
	(void)cl;
	return (args1->type == args2->type) && (args1->tag == args2->tag) && (args1->traverse == args2->traverse);
}

static char *dhcp_api_query_args_describe(query_class_t *cl, query_args_t *args) {
#define DESCRIPTIONLEN 64
	(void)cl;
	char *description = calloc(DESCRIPTIONLEN, 1);
	if (!description)
		return NULL;
	char *descptr = description;
	int n=0;
	if (DESCRIPTIONLEN > (descptr - description))
		descptr += snprintf(descptr, DESCRIPTIONLEN - (descptr - description), "%stype=\"%s\"", n++?", ":"", dhcp_api_optiontype2str(args->type));
	if (DESCRIPTIONLEN > (descptr - description))
		descptr += snprintf(descptr, DESCRIPTIONLEN - (descptr - description), "%stag=%u", n++?", ":"", args->tag);
	if (DESCRIPTIONLEN > (descptr - description))
		descptr += snprintf(descptr, DESCRIPTIONLEN - (descptr - description), "%straverse=\"%s\"", n++?", ":"", traverse_mode_string(args->traverse));
	(void)descptr; (void)n;
	return description;
}

static void dhcp_api_query_run(query_t *q, variant_t *result) {
	query_args_t *args = (query_args_t *)query_args(q);
	dhcp_api_getDHCPOptionImpl(result, query_intf(q), args->type, args->tag, args->traverse);
}

static query_class_info_t dhcp_api_getDHCPOptionClassInfo = {
	/* name */          "getDHCPOption",
	/* type */          variant_type_string,
	/* args_convert */  dhcp_api_query_args_convert,
	/* args_destroy */  dhcp_api_query_args_destroy,
	/* args_compare */  dhcp_api_query_args_compare,
	/* args_describe */ dhcp_api_query_args_describe,
	/* open */          NULL,
	/* close */         NULL,
	/* run */           dhcp_api_query_run,
	/* userdata */      NULL,
};
static query_class_t *dhcp_api_getDHCPOptionClass = NULL;

static void dhcp_api_intfMibHandler(intf_t *intf, intf_mibevent_t event, const char *path, void *userdata) {
	(void)intf; (void)event; (void)userdata;
	optiontype_t type;
	uint16_t tag = 0;
	if (flagset_check(intf_flagset(intf), "dhcp") && !strncmp("ReqOption.", path, 10))
		type = optiontype_req;
	else if (flagset_check(intf_flagset(intf), "dhcp") && !strncmp("SentOption.", path, 11))
		type = optiontype_sent;
	else if (flagset_check(intf_flagset(intf), "dhcpv6") && !strncmp("ReceivedOption.", path, 15))
		type = optiontype_req6;
	else if (flagset_check(intf_flagset(intf), "dhcpv6") && !strncmp("SentOption.", path, 11))
		type = optiontype_sent6;
	else
		return;
	char *s = strdup(path);
	if (!s) {
		SAH_TRACE_ERROR("dhcp-api - strdup(%s) failed", path);
		return;
	}
	char *parameter = strchr(strchr(s, '.') + 1, '.');
	if (parameter)
		*parameter++ = '\0';
	if (parameter && strcmp(parameter, "Tag"))
		tag = object_parameterUInt16Value(object_getObject(plugin_intf2object(intf), s, path_attr_key_notation, NULL), "Tag");
	free(s);
	query_t *q;
	for (q = query_first(dhcp_api_getDHCPOptionClass); q; q = query_next(q)) {
		query_args_t *args = query_args(q);
		if (args->type == type && (!tag || args->tag == tag)) {
			query_invalidate(q);
		}
	}
}

static void dhcp_api_intfCreateHandler(intf_t *intf, void *userdata) {
	(void)userdata;
	function_setHandler(object_getFunction(plugin_intf2object(intf), "getDHCPOption"), dhcp_api_getDHCPOption);
	query_class_invalidate(dhcp_api_getDHCPOptionClass);
}

static void dhcp_api_intfDestroyHandler(intf_t *intf, void *userdata) {
	(void)userdata;
	function_setHandler(object_getFunction(plugin_intf2object(intf), "getDHCPOption"), NULL);
	query_class_invalidate(dhcp_api_getDHCPOptionClass);
}

static void dhcp_api_intfLinkHandler(intf_t *ulintf, intf_t *llintf, bool value, void *userdata) {
	(void)ulintf; (void)llintf; (void)value; (void)userdata;
	query_class_invalidate(dhcp_api_getDHCPOptionClass);
}

static bool dhcp_api_start(argument_value_list_t *args __attribute__((unused))) {
	SAH_TRACEZ_INFO(ME, "Initializing dhcp-api module");
	dhcp_api_getDHCPOptionClass = query_class_register(&dhcp_api_getDHCPOptionClassInfo);
	intf_addMgmtListener(NULL, dhcp_api_intfCreateHandler, NULL, dhcp_api_intfDestroyHandler, NULL, true);
	intf_addLinkListener(NULL, NULL, dhcp_api_intfLinkHandler, NULL, false);
	intf_addMibListener(NULL, dhcp_api_intfMibHandler, NULL);
	return true;
}

static void dhcp_api_stop() {
	SAH_TRACEZ_INFO(ME, "Cleaning up dhcp-api module");
	intf_delMibListener(NULL, dhcp_api_intfMibHandler, NULL);
	intf_delLinkListener(NULL, NULL, dhcp_api_intfLinkHandler, NULL, false);
	intf_delMgmtListener(NULL, dhcp_api_intfCreateHandler, NULL, dhcp_api_intfDestroyHandler, NULL, true);
	query_class_unregister(dhcp_api_getDHCPOptionClass);
}

static mtk_module_info_t dhcp_api_info = {
	.name  = ME,
	.start = dhcp_api_start,
	.stop  = dhcp_api_stop
};

__attribute__((constructor)) static void dhcp_api_init() {
	mtk_module_register(&dhcp_api_info);
}

