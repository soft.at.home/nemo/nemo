/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "vlan"

#include <nemo/core.h>

static long vlan_uptime() {
	char buf[64], *ptr = NULL;
	FILE *f = fopen("/proc/uptime", "r"); // Remark using sysinfo() would be easier but it's not available in uclibc
	if (f) {
		ptr = fgets(buf, 64, f);
		fclose(f);
	}
	return ptr?strtol(ptr, NULL, 0):0;
}

static void vlan_upHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)flag; (void)userdata; (void)value;
	object_parameterSetUInt32Value(plugin_intf2object(intf), "LastChangeTime", vlan_uptime());
	object_commit(plugin_intf2object(intf));
}

static bool vlan_readLastChange(parameter_t *parameter, variant_t *value) {
	uint32_t t0 = object_parameterUInt32Value(parameter_owner(parameter), "LastChangeTime");
	if (t0)
		variant_setUInt32(value, vlan_uptime() - t0);
	else
		variant_setUInt32(value, 0);
	return true;
}

static void vlan_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata;	(void)flag;
	if (value) {
		intf_addFlagListener(intf, "up", vlan_upHandler, NULL, true);
		plugin_setParameterReadHandler(object_getParameter(plugin_intf2object(intf), "LastChange"), vlan_readLastChange);
	} else {
		intf_delFlagListener(intf, "up", vlan_upHandler, NULL, true);
		plugin_setParameterReadHandler(object_getParameter(plugin_intf2object(intf), "LastChange"), NULL);
	}
}

static bool vlan_start(argument_value_list_t *args __attribute__((unused))) {
	SAH_TRACEZ_INFO(ME, "Initializing vlan module");
	intf_addFlagListener(NULL, "vlan", vlan_rootHandler, NULL, true);
	return true;
}

static void vlan_stop() {
	SAH_TRACEZ_INFO(ME, "Cleaning up vlan module");
	intf_delFlagListener(NULL, "vlan", vlan_rootHandler, NULL, true);
}

static mtk_module_info_t vlan_info = {
	.name  = ME,
	.start = vlan_start,
	.stop  = vlan_stop
};

__attribute__((constructor)) static void vlan_init() {
	mtk_module_register(&vlan_info);
}

