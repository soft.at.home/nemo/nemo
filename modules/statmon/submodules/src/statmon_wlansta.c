/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "statmon"

#include <nemo/core.h>

#include "statmon.h"

#define FLAG "wlansta"

typedef struct _wlansta_data_t {
	llist_iterator_t it;
	int monitorId;
	time_t time;
	char *MACAddress;
	unsigned long long Retransmissions;
	unsigned long long RetransmissionsTotal;
	unsigned long long RxPacketCount;
	unsigned long long RxPacketCountTotal;
	unsigned long long TxPacketCount;
	unsigned long long TxPacketCountTotal;
	unsigned long long TxBytes;
	unsigned long long TxBytesTotal;
	unsigned long long RxBytes;
	unsigned long long RxBytesTotal;
	unsigned long long Rx_Retransmissions;
	unsigned long long Rx_RetransmissionsTotal;
	bool dirty;
} wlansta_data_t;

typedef struct _wlansta_data_buf_t {
	int monitorId;
	time_t time;
	const char *MACAddress;
	unsigned long long Retransmissions;
	unsigned long long RetransmissionsTotal;
	unsigned long long RxPacketCount;
	unsigned long long RxPacketCountTotal;
	unsigned long long TxPacketCount;
	unsigned long long TxPacketCountTotal;
	unsigned long long TxBytes;
	unsigned long long TxBytesTotal;
	unsigned long long RxBytes;
	unsigned long long RxBytesTotal;
	unsigned long long Rx_Retransmissions;
	unsigned long long Rx_RetransmissionsTotal;
	unsigned long long LastDataDownlinkRate;
	unsigned long long LastDataUplinkRate;
	unsigned long long SignalStrength;
	unsigned long long SignalNoiseRatio;
	unsigned long long Noise;
} wlansta_data_buf_t;

static llist_t station_data_list;

static void statmon_wlansta_updateStats(wlansta_data_t *data, wlansta_data_buf_t *tmp) {
	wlansta_data_t *ndData = data;
	SAH_TRACEZ_IN(ME);
	if(!tmp->MACAddress) {
		SAH_TRACEZ_OUT(ME);
		return;
	}
	if(!ndData) {
		ndData = calloc(1,sizeof(wlansta_data_t));
		llist_iterator_initialize(&ndData->it);
		llist_append(&station_data_list, &ndData->it);
		ndData->monitorId = tmp->monitorId;
		ndData->MACAddress = strdup(tmp->MACAddress);
	}

	unsigned long long deltaRxPacketCount = statmon_calculateDifference(ndData->RxPacketCount, tmp->RxPacketCount);
	unsigned long long deltaTxPacketCount = statmon_calculateDifference(ndData->TxPacketCount, tmp->TxPacketCount);
	unsigned long long deltaRetransmissions = statmon_calculateDifference(ndData->Retransmissions, tmp->Retransmissions);
	unsigned long long deltaTxBytes = statmon_calculateDifference(ndData->TxBytes, tmp->TxBytes);
	unsigned long long deltaRxBytes = statmon_calculateDifference(ndData->RxBytes, tmp->RxBytes);
	unsigned long long deltaRx_Retransmissions = statmon_calculateDifference(ndData->Rx_Retransmissions, tmp->Rx_Retransmissions);

	ndData->time = tmp->time;
	ndData->RxPacketCount = tmp->RxPacketCount;
	ndData->TxPacketCount = tmp->TxPacketCount;
	ndData->Retransmissions = tmp->Retransmissions;
	ndData->TxBytes = tmp->TxBytes;
	ndData->RxBytes = tmp->RxBytes;
	ndData->Rx_Retransmissions = tmp->Rx_Retransmissions;
	ndData->dirty = false;

	ndData->RxPacketCountTotal += deltaRxPacketCount;
	ndData->TxPacketCountTotal += deltaTxPacketCount;
	ndData->RetransmissionsTotal += deltaRetransmissions;
	ndData->TxBytesTotal += deltaTxBytes;
	ndData->RxBytesTotal += deltaRxBytes;
	ndData->Rx_RetransmissionsTotal += deltaRx_Retransmissions;

	SAH_TRACEZ_OUT(ME);
}
static wlansta_data_t *statmon_wlansta_findStats(int monitorId, const char *MACAddress) {
	llist_iterator_t *it = NULL;
	wlansta_data_t *data = NULL;
	if(!MACAddress) {
		return NULL;
	}
	llist_for_each(it, &station_data_list) {
		data = llist_item_data(it, wlansta_data_t, it);
		if(data->monitorId==monitorId && strcmp(MACAddress,data->MACAddress)==0) {
			return data;
		}
	}
	return NULL;
}

static void statmon_wlansta_markDirty(int monitorId) {
	llist_iterator_t *it = NULL;
	wlansta_data_t *data = NULL;
	llist_for_each(it, &station_data_list) {
		data = llist_item_data(it, wlansta_data_t, it);
		if(data->monitorId==monitorId ) {
			data->dirty = true;
		}
	}
}
static bool statmon_wlansta_clenaupDirty() {
	llist_iterator_t *it = NULL;
	llist_iterator_t *next = NULL;
	wlansta_data_t *data = NULL;
	bool ret = false;
	it = llist_first(&station_data_list);
	while(it) {
		next = llist_iterator_next(it);
		data = llist_item_data(it, wlansta_data_t, it);
		if(data->dirty == true) {
			free(data->MACAddress);
			llist_iterator_take(&data->it);
			free(data);
			ret = true;
		}
		it = next;
	}
	return ret;
}

static void statmon_wlansta_readPayload(int monitorId, time_t now_t, variant_map_t *map, wlansta_data_buf_t *tmp) {
	tmp->monitorId = monitorId;
	tmp->time = now_t;
	tmp->MACAddress = variant_map_da_findChar(map, "MACAddress");
	tmp->RxPacketCount = variant_map_findUInt64(map, "RxPacketCount");
	tmp->TxPacketCount = variant_map_findUInt64(map, "TxPacketCount");
	tmp->Retransmissions = variant_map_findUInt64(map, "Retransmissions");
	tmp->TxBytes = variant_map_findUInt64(map, "TxBytes");
	tmp->RxBytes = variant_map_findUInt64(map, "RxBytes");
	tmp->Rx_Retransmissions = variant_map_findUInt64(map, "Rx_Retransmissions");
	tmp->LastDataDownlinkRate = variant_map_findUInt64(map, "LastDataDownlinkRate");
	tmp->LastDataUplinkRate = variant_map_findUInt64(map, "LastDataUplinkRate");
	tmp->SignalStrength = variant_map_findUInt64(map, "SignalStrength");
	tmp->SignalNoiseRatio = variant_map_findUInt64(map, "SignalNoiseRatio");
	tmp->Noise = variant_map_findUInt64(map, "Noise");
}

static void statmon_wlansta_addNotificationElement(variant_list_t *notificationList, wlansta_data_t *data, wlansta_data_buf_t *tmp) {
	variant_list_iterator_t *vlit = NULL;
	variant_map_t *notificationMap = NULL;
	struct tm *now_tm = gmtime(&tmp->time);

	if(!notificationList || !data | !tmp)
		return;

	vlit = variant_list_iterator_createMapMove(NULL);
	notificationMap = variant_list_iterator_da_map(vlit);

	unsigned long long deltaRxPacketCount = statmon_calculateDifference(data->RxPacketCount, tmp->RxPacketCount);
	unsigned long long deltaTxPacketCount = statmon_calculateDifference(data->TxPacketCount, tmp->TxPacketCount);
	unsigned long long deltaRetransmissions = statmon_calculateDifference(data->Retransmissions, tmp->Retransmissions);
	unsigned long long deltaTxBytes = statmon_calculateDifference(data->TxBytes, tmp->TxBytes);
	unsigned long long deltaRxBytes = statmon_calculateDifference(data->RxBytes, tmp->RxBytes);
	unsigned long long deltaRx_Retransmissions = statmon_calculateDifference(data->Rx_Retransmissions, tmp->Rx_Retransmissions);

	variant_map_addDateTime(notificationMap, "Timestamp", now_tm);
	variant_map_addChar(notificationMap, "MACAddress", tmp->MACAddress);

	variant_map_addUInt64(notificationMap, "RxPacketCount", data->RxPacketCountTotal + deltaRxPacketCount);
	variant_map_addUInt64(notificationMap, "TxPacketCount", data->TxPacketCountTotal + deltaTxPacketCount);
	variant_map_addUInt64(notificationMap, "Retransmissions", data->RetransmissionsTotal + deltaRetransmissions);
	variant_map_addUInt64(notificationMap, "TxBytes", data->TxBytesTotal + deltaTxBytes);
	variant_map_addUInt64(notificationMap, "RxBytes", data->RxBytesTotal + deltaRxBytes);
	variant_map_addUInt64(notificationMap, "Rx_Retransmissions", data->Rx_Retransmissions + deltaRx_Retransmissions);

	variant_map_addUInt64(notificationMap, "DeltaRxPacketCount", deltaRxPacketCount);
	variant_map_addUInt64(notificationMap, "DeltaTxPacketCount", deltaTxPacketCount);
	variant_map_addUInt64(notificationMap, "DeltaRetransmissions", deltaRetransmissions);
	variant_map_addUInt64(notificationMap, "DeltaTxBytes", deltaTxBytes);
	variant_map_addUInt64(notificationMap, "DeltaRxBytes", deltaRxBytes);
	variant_map_addUInt64(notificationMap, "DeltaRx_Retransmissions", deltaRx_Retransmissions);

	variant_map_addUInt64(notificationMap, "LastDataDownlinkRate", tmp->LastDataDownlinkRate);
	variant_map_addUInt64(notificationMap, "LastDataUplinkRate", tmp->LastDataUplinkRate);
	variant_map_addUInt64(notificationMap, "SignalStrength", tmp->SignalStrength);
	variant_map_addUInt64(notificationMap, "SignalNoiseRatio", tmp->SignalNoiseRatio);
	variant_map_addUInt64(notificationMap, "Noise", tmp->Noise);

	variant_list_append(notificationList, vlit);
}

static void statmon_wlansta_handleStats(int monitorId, const variant_t *payload) {
	variant_list_iterator_t *vit = NULL;
	wlansta_data_t *data = NULL;
	wlansta_data_buf_t tmp;
	variant_t notificationValue;
	variant_list_t *notificationList = NULL;
	bool dirty = false;
	time_t now_t;
	time(&now_t);
	SAH_TRACEZ_IN(ME);

	if(!payload) {
		SAH_TRACEZ_OUT(ME);
		return;
	}

	variant_initialize(&notificationValue, variant_type_array);
	notificationList = variant_da_list(&notificationValue);

	statmon_wlansta_markDirty(monitorId);

	variant_list_for_each(vit, variant_da_list(payload)) {
		variant_map_t *map = variant_list_iterator_da_map(vit);
		statmon_wlansta_readPayload(monitorId, now_t, map, &tmp);
		if(!tmp.MACAddress)
			continue;

		data = statmon_wlansta_findStats(monitorId, tmp.MACAddress);
		if(data) {
			statmon_wlansta_addNotificationElement(notificationList, data, &tmp);
		}

		statmon_wlansta_updateStats(data, &tmp);
	}

	dirty = statmon_wlansta_clenaupDirty();
	//if dirty, send an empty notification to clients to indicate that the device disappeared
	if(variant_list_size(notificationList)>0 || dirty) {
		statmon_monitor_sendNotification(monitorId, &notificationValue);
	}
	variant_cleanup(&notificationValue);

	SAH_TRACEZ_OUT(ME);
}

static bool statmon_wlansta_replyHandler(request_t *req, reply_item_t *item, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)pcb;
	(void)from;
	SAH_TRACEZ_IN(ME);
	int monitorId = (int)userdata;
	if (reply_item_type(item) == reply_type_error) {
		SAH_TRACE_ERROR("Function call error (%d) - %s - %s",
		reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
	} else if (reply_item_type(item) == reply_type_function_return) {
		const variant_t *payload = reply_item_returnValue(item);
		statmon_wlansta_handleStats(monitorId, payload);
	}
	request_destroy(req);
	SAH_TRACEZ_OUT(ME);
	return true;
}

static void statmon_wlansta_timeout(int monitorId, const char *path) {
	request_t *request = NULL;
	char intfpath[64];
	SAH_TRACEZ_IN(ME);

	statmon_getInterfacePathFromSubscriptionPath(path, intfpath, 64) ;
	if(*intfpath == '\0') {
		SAH_TRACEZ_OUT(ME);
		return;
	}

	SAH_TRACEZ_INFO(ME,"Calling %s (%s)", "getStationStats", intfpath);
	request = request_create_executeFunction(intfpath, "getStationStats", request_common_path_key_notation);
	if(!request) {
		SAH_TRACEZ_OUT(ME);
		return;
	}
	request_setReplyItemHandler(request, statmon_wlansta_replyHandler);
	request_setData(request, (void *)monitorId);
	if(!pcb_sendRequest(plugin_getPcb(), plugin_getPeer(), request)) {
		SAH_TRACEZ_ERROR(ME, "Failed to send request");
	}
	SAH_TRACEZ_OUT(ME);
}

static void statmon_wlansta_disabled(int monitorId) {
	SAH_TRACEZ_IN(ME);
	SAH_TRACEZ_INFO(ME, "Disabling monitor %d in station stats", monitorId);
	llist_iterator_t *it = NULL;
	wlansta_data_t *data = NULL;

	llist_for_each(it, &station_data_list) {
		data = llist_item_data(it, wlansta_data_t, it);
		if(data->monitorId!=monitorId) {
			continue;
		}

		llist_iterator_take(&data->it);
		free(data->MACAddress);
		free(data);
		break;
	}
	SAH_TRACEZ_OUT(ME);
}

static void statmon_wlansta_cleanup() {
	SAH_TRACEZ_IN(ME);
	llist_iterator_t *it = llist_first(&station_data_list);
	llist_iterator_t *next = NULL;
	while(it) {
		next = llist_iterator_next(it);
		wlansta_data_t *data = llist_item_data(it, wlansta_data_t, it);
		llist_iterator_take(&data->it);
		free(data->MACAddress);
		free(data);
		it = next;
	}
	llist_cleanup(&station_data_list);
	SAH_TRACEZ_OUT(ME);
}

void statmon_wlansta_initialize() {
	SAH_TRACEZ_IN(ME);
	llist_initialize(&station_data_list);
	statmon_submodule_register(FLAG, statmon_type_station, statmon_wlansta_timeout, statmon_wlansta_disabled, statmon_wlansta_cleanup);
	SAH_TRACEZ_OUT(ME);
}


