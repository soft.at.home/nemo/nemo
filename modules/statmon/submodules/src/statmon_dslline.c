/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "statmon"

#include <nemo/core.h>

#include "statmon.h"

#define FLAG "dslline"

typedef enum {
	dslline_request_stats = 0x01,
	dslline_request_mibs = 0x02
} dslline_request_type_t;

typedef struct _dslline_data_t {
	llist_iterator_t it;
	bool deltasValid;
	int monitorId;
	int completedRequests;
	time_t time;
	uint32_t monotonicTime;
	uint32_t RxPackets;
	uint32_t TxPackets;
	uint32_t RxBytes;
	uint32_t TxBytes;
	uint32_t RxErrors;
	uint32_t TxErrors;
	uint32_t DeltaTime;
	uint32_t DeltaRxPackets;
	uint32_t DeltaTxPackets;
	uint32_t DeltaRxBytes;
	uint32_t DeltaTxBytes;
	uint32_t DeltaRxErrors;
	uint32_t DeltaTxErrors;
	uint32_t DownstreamCurrRate;
	uint32_t UpstreamCurrRate;
} dslline_data_t;

typedef void (*dslline_statsHandler)(dslline_data_t *data, const variant_t *payload);

static llist_t dslline_data_list;

static uint32_t get_monotonic_time() {
	struct timespec ts;
	if (clock_gettime(CLOCK_MONOTONIC, &ts) == -1) {
		SAH_TRACEZ_NOTICE(ME, "Could not get monotonic time [%s]", strerror(errno));
		time_t now;
		time(&now);
		return now;
	}
	return ts.tv_sec;
}

static dslline_data_t *statmon_dslline_findData(int monitorId) {
	llist_iterator_t *it = NULL;
	dslline_data_t *data = NULL;
	llist_for_each(it, &dslline_data_list) {
		data = llist_item_data(it, dslline_data_t, it);
		if(data->monitorId==monitorId) {
			return data;
		}
	}
	return NULL;
}

static dslline_data_t *statmon_dslline_getData(int monitorId) {
	dslline_data_t *data = statmon_dslline_findData(monitorId);
	if (!data) {
		data = calloc(1,sizeof(dslline_data_t));
		if (!data) {
			SAH_TRACEZ_ERROR(ME, "calloc() failed");
			return NULL;
		}
		llist_iterator_initialize(&data->it);
		llist_append(&dslline_data_list, &data->it);
		data->deltasValid = false;
		data->monitorId = monitorId;
	}
	return data;
}

static void statmon_dslline_sendNotification(dslline_data_t *data) {
	if (data->completedRequests != (dslline_request_stats | dslline_request_mibs)) {
		return;
	}
	struct tm *now_tm = gmtime(&data->time);
	variant_t notificationValue;
	variant_map_t *notificationMap = NULL;
	variant_initialize(&notificationValue, variant_type_map);
	notificationMap = variant_da_map(&notificationValue);
	variant_map_addDateTime(notificationMap, "Timestamp", now_tm);
	variant_map_addUInt32(notificationMap, "RxPackets", data->RxPackets);
	variant_map_addUInt32(notificationMap, "TxPackets", data->TxPackets);
	variant_map_addUInt32(notificationMap, "RxBytes", data->RxBytes);
	variant_map_addUInt32(notificationMap, "TxBytes", data->TxBytes);
	variant_map_addUInt32(notificationMap, "RxErrors", data->RxErrors);
	variant_map_addUInt32(notificationMap, "TxErrors", data->TxErrors);
	variant_map_addUInt32(notificationMap, "DeltaTime", data->DeltaTime);
	variant_map_addUInt32(notificationMap, "DeltaRxPackets", data->DeltaRxPackets);
	variant_map_addUInt32(notificationMap, "DeltaTxPackets", data->DeltaTxPackets);
	variant_map_addUInt32(notificationMap, "DeltaRxBytes", data->DeltaRxBytes);
	variant_map_addUInt32(notificationMap, "DeltaTxBytes", data->DeltaTxBytes);
	variant_map_addUInt32(notificationMap, "DeltaRxErrors", data->DeltaRxErrors);
	variant_map_addUInt32(notificationMap, "DeltaTxErrors", data->DeltaTxErrors);
	variant_map_addUInt32(notificationMap, "DownstreamCurrRate", data->DownstreamCurrRate);
	variant_map_addUInt32(notificationMap, "UpstreamCurrRate", data->UpstreamCurrRate);
	statmon_monitor_sendNotification(data->monitorId, &notificationValue);
	variant_cleanup(&notificationValue);
}

static uint32_t getDiff(uint32_t a, uint32_t b) {
	if (a >= b) {
		return a - b;
	} else {
		return 0xffffffff - b + a;
	}
}

static void statmon_dslline_handleStats(dslline_data_t *data, const variant_t *payload) {
	time_t now_t;
	time(&now_t);
	uint32_t monotonicTime = get_monotonic_time();
	SAH_TRACEZ_IN(ME);

	variant_map_t *map = variant_da_map(payload);
	variant_map_t *stats = variant_map_da_findMap(map, "stats");
	uint32_t RxPackets = variant_map_findUInt32(stats, "PacketsReceived");
	uint32_t TxPackets = variant_map_findUInt32(stats, "PacketsSent");
	uint32_t RxBytes   = variant_map_findUInt32(stats, "BytesReceived");
	uint32_t TxBytes   = variant_map_findUInt32(stats, "BytesSent");
	uint32_t RxErrors  = variant_map_findUInt32(stats, "ErrorsReceived");
	uint32_t TxErrors  = variant_map_findUInt32(stats, "ErrorsSent");
	data->DeltaTime = getDiff(monotonicTime, data->monotonicTime);
	data->DeltaRxPackets = getDiff(RxPackets, data->RxPackets);
	data->DeltaTxPackets = getDiff(TxPackets, data->TxPackets);
	data->DeltaRxBytes   = getDiff(RxBytes, data->RxBytes);
	data->DeltaTxBytes   = getDiff(TxBytes, data->TxBytes);
	data->DeltaRxErrors  = getDiff(RxErrors, data->RxErrors);
	data->DeltaTxErrors  = getDiff(TxErrors, data->TxErrors);
	data->RxPackets = RxPackets;
	data->TxPackets = TxPackets;
	data->RxBytes   = RxBytes;
	data->TxBytes   = TxBytes;
	data->RxErrors  = RxErrors;
	data->TxErrors  = TxErrors;
	data->time = now_t;
	data->monotonicTime = monotonicTime;
	if (data->deltasValid) {
		data->completedRequests |= dslline_request_stats;
		statmon_dslline_sendNotification(data);
	} else {
		data->deltasValid = true;
	}

	SAH_TRACEZ_OUT(ME);
}

static void statmon_dslline_handleMib(dslline_data_t *data, const variant_t *payload) {
	variant_map_t *map = variant_da_map(payload);
	variant_map_t *dslline = variant_map_da_findMap(map, "dslline");
	variant_map_t *intf = variant_da_map(variant_map_iterator_data(variant_map_first(dslline)));
	data->DownstreamCurrRate = variant_map_findUInt32(intf, "Line_DownstreamCurrRate");
	data->UpstreamCurrRate = variant_map_findUInt32(intf, "Line_UpstreamCurrRate");
	data->completedRequests |= dslline_request_mibs;
	statmon_dslline_sendNotification(data);
}

static bool statmon_dslline_handleReply(request_t *req, reply_item_t *item, pcb_t *pcb, peer_info_t *from,
										dslline_data_t *data, dslline_statsHandler statsHandler) {
	(void)pcb;
	(void)from;
	SAH_TRACEZ_IN(ME);
	if (reply_item_type(item) == reply_type_error) {
		SAH_TRACE_ERROR("Function call error (%d) - %s - %s",
		reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
	} else if (reply_item_type(item) == reply_type_function_return) {
		const variant_t *payload = reply_item_returnValue(item);
		statsHandler(data, payload);
	}
	request_destroy(req);
	SAH_TRACEZ_OUT(ME);
	return true;
}

static bool statmon_dslline_statsReplyHandler(request_t *req, reply_item_t *item, pcb_t *pcb, peer_info_t *from, void *userdata) {
	int monitorId = (int)userdata;
	dslline_data_t *data = statmon_dslline_findData(monitorId);
	if (!data) {
		SAH_TRACEZ_WARNING(ME, "Monitor %d dsllinestats not found", monitorId);
		request_destroy(req);
		return false;
	}
	return statmon_dslline_handleReply(req, item, pcb, from, data, statmon_dslline_handleStats);
}

static bool statmon_dslline_mibsReplyHandler(request_t *req, reply_item_t *item, pcb_t *pcb, peer_info_t *from, void *userdata) {
	int monitorId = (int)userdata;
	dslline_data_t *data = statmon_dslline_findData(monitorId);
	if (!data) {
		SAH_TRACEZ_WARNING(ME, "Monitor %d dsllinestats not found", monitorId);
		request_destroy(req);
		return false;
	}
	return statmon_dslline_handleReply(req, item, pcb, from, data, statmon_dslline_handleMib);
}

static void statmon_dslline_timeout(int monitorId, const char *path) {
	char intfpath[64];
	SAH_TRACEZ_IN(ME);

	statmon_getInterfacePathFromSubscriptionPath(path, intfpath, 64) ;
	if(*intfpath == '\0') {
		SAH_TRACEZ_OUT(ME);
		return;
	}

	dslline_data_t *data = statmon_dslline_getData(monitorId);
	if (!data) {
		SAH_TRACEZ_ERROR(ME, "Failed to create data object");
		goto exit;
	}
	data->completedRequests = 0;

	SAH_TRACEZ_INFO(ME,"Calling %s (%s)", "getDSLLineStats", intfpath);
	request_t *statsReq = request_create_executeFunction(intfpath, "getDSLLineStats",
			request_common_path_key_notation);
	if(!statsReq)
		goto exit;
	request_setReplyItemHandler(statsReq, statmon_dslline_statsReplyHandler);
	request_setData(statsReq, (void *)monitorId);
	if(!pcb_sendRequest(plugin_getPcb(), plugin_getPeer(), statsReq)) {
		SAH_TRACEZ_ERROR(ME, "Failed to send request");
		goto exit_cleanup_statsReq;
	}

	SAH_TRACEZ_INFO(ME,"Calling %s (%s)", "getMIBs", intfpath);
	request_t *mibReq = request_create_executeFunction(intfpath, "getMIBs",
			request_common_path_key_notation | request_function_args_by_name);
	if(!mibReq)
		goto exit_cleanup_statsReq;
	request_addCharParameter(mibReq, "mibs", "dslline");
	request_addCharParameter(mibReq, "traverse", "this");
	request_setReplyItemHandler(mibReq, statmon_dslline_mibsReplyHandler);
	request_setData(mibReq, (void *)monitorId);
	if(!pcb_sendRequest(plugin_getPcb(), plugin_getPeer(), mibReq)) {
		SAH_TRACEZ_ERROR(ME, "Failed to send request");
		goto exit_cleanup_mibReq;
	}

	SAH_TRACEZ_OUT(ME);
	return;

exit_cleanup_mibReq:
	request_destroy(mibReq);
exit_cleanup_statsReq:
	request_destroy(statsReq);
exit:
	SAH_TRACEZ_OUT(ME);
}

static void statmon_dslline_disabled(int monitorId) {
	llist_iterator_t *it = NULL;
	dslline_data_t *data = NULL;
	SAH_TRACEZ_IN(ME);

	SAH_TRACEZ_INFO(ME, "Disabling monitor %d dsllinestats", monitorId);
	llist_for_each(it, &dslline_data_list) {
		data = llist_item_data(it, dslline_data_t, it);
		if(data->monitorId!=monitorId) {
			continue;
		}

		llist_iterator_take(&data->it);
		free(data);
		break;
	}
	SAH_TRACEZ_OUT(ME);
}


static void statmon_dslline_cleanup() {
	SAH_TRACEZ_IN(ME);
	llist_iterator_t *it = llist_first(&dslline_data_list);
	llist_iterator_t *next = NULL;
	while(it) {
		next = llist_iterator_next(it);
		dslline_data_t *data = llist_item_data(it, dslline_data_t, it);
		llist_iterator_take(&data->it);
		free(data);
		it = next;
	}
	llist_cleanup(&dslline_data_list);
	SAH_TRACEZ_OUT(ME);
}

void statmon_dslline_initialize() {
	SAH_TRACEZ_IN(ME);
	llist_initialize(&dslline_data_list);
	statmon_submodule_register(FLAG, statmon_type_dslline, statmon_dslline_timeout, statmon_dslline_disabled, statmon_dslline_cleanup);
	SAH_TRACEZ_OUT(ME);
}


