/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "statmon"

#include <nemo/core.h>

#include "statmon.h"

#define FLAG "netdev"

typedef struct _netdev_data_t {
	llist_iterator_t it;
	int monitorId;
	time_t time;
	unsigned long long RxPackets;
	unsigned long long RxPacketsTotal;
	unsigned long long TxPackets;
	unsigned long long TxPacketsTotal;
	unsigned long long RxBytes;
	unsigned long long RxBytesTotal;
	unsigned long long TxBytes;
	unsigned long long TxBytesTotal;
	unsigned long long RxErrors;
	unsigned long long RxErrorsTotal;
	unsigned long long TxErrors;
	unsigned long long TxErrorsTotal;
	unsigned long long RxDropped;
	unsigned long long RxDroppedTotal;
	unsigned long long TxDropped;
	unsigned long long TxDroppedTotal;
	unsigned long long Multicast;
	unsigned long long MulticastTotal;
	unsigned long long Collisions;
	unsigned long long CollisionsTotal;
} netdev_data_t;

static llist_t netdev_data_list;

static netdev_data_t *statmon_netdev_findStats(int monitorId) {
	llist_iterator_t *it = NULL;
	netdev_data_t *data = NULL;
	llist_for_each(it, &netdev_data_list) {
		data = llist_item_data(it, netdev_data_t, it);
		if(data->monitorId==monitorId) {
			return data;
		}
	}
	return NULL;
}

static void statmon_netdev_updateStats(netdev_data_t *data, netdev_data_t *tmp) {
	netdev_data_t *ndData = data;
	SAH_TRACEZ_IN(ME);
	if(!ndData) {
		ndData = calloc(1,sizeof(netdev_data_t));
		llist_iterator_initialize(&ndData->it);
		llist_append(&netdev_data_list, &ndData->it);
		ndData->monitorId = tmp->monitorId;
	}

	unsigned long long deltaRxPackets  = statmon_calculateDifference(ndData->RxPackets, tmp->RxPackets);
	unsigned long long deltaTxPackets  = statmon_calculateDifference(ndData->TxPackets, tmp->TxPackets);
	unsigned long long deltaRxBytes    = statmon_calculateDifference(ndData->RxBytes, tmp->RxBytes);
	unsigned long long deltaTxBytes    = statmon_calculateDifference(ndData->TxBytes, tmp->TxBytes);
	unsigned long long deltaRxErrors   = statmon_calculateDifference(ndData->RxErrors, tmp->RxErrors);
	unsigned long long deltaTxErrors   = statmon_calculateDifference(ndData->TxErrors, tmp->TxErrors);
	unsigned long long deltaRxDropped  = statmon_calculateDifference(ndData->RxDropped, tmp->RxDropped);
	unsigned long long deltaTxDropped  = statmon_calculateDifference(ndData->TxDropped, tmp->TxDropped);
	unsigned long long deltaMulticast  = statmon_calculateDifference(ndData->Multicast, tmp->Multicast);
	unsigned long long deltaCollisions = statmon_calculateDifference(ndData->Collisions, tmp->Collisions);

	ndData->time       = tmp->time;
	ndData->RxPackets  = tmp->RxPackets;
	ndData->TxPackets  = tmp->TxPackets;
	ndData->RxBytes	   = tmp->RxBytes;
	ndData->TxBytes	   = tmp->TxBytes;
	ndData->RxErrors   = tmp->RxErrors;
	ndData->TxErrors   = tmp->TxErrors;
	ndData->RxDropped  = tmp->RxDropped;
	ndData->TxDropped  = tmp->TxDropped;
	ndData->Multicast  = tmp->Multicast;
	ndData->Collisions = tmp->Collisions;

	ndData->RxPacketsTotal  += deltaRxPackets;
	ndData->TxPacketsTotal  += deltaTxPackets;
	ndData->RxBytesTotal    += deltaRxBytes;
	ndData->TxBytesTotal    += deltaTxBytes;
	ndData->RxErrorsTotal   += deltaRxErrors;
	ndData->TxErrorsTotal   += deltaTxErrors;
	ndData->RxDroppedTotal  += deltaRxDropped;
	ndData->TxDroppedTotal  += deltaTxDropped;
	ndData->MulticastTotal  += deltaMulticast;
	ndData->CollisionsTotal += deltaCollisions;

	SAH_TRACEZ_OUT(ME);
}

static void statmon_netdev_readPayload(int monitorId, time_t now_t, const variant_t *payload, netdev_data_t *tmp) {
	variant_map_t *map = variant_da_map(payload);
	tmp->monitorId = monitorId;
	tmp->time = now_t;
	tmp->RxPackets  = variant_map_findUInt64(map, "RxPackets");
	tmp->TxPackets  = variant_map_findUInt64(map, "TxPackets");
	tmp->RxBytes	= variant_map_findUInt64(map, "RxBytes");
	tmp->TxBytes	= variant_map_findUInt64(map, "TxBytes");
	tmp->RxErrors   = variant_map_findUInt64(map, "RxErrors");
	tmp->TxErrors   = variant_map_findUInt64(map, "TxErrors");
	tmp->RxDropped  = variant_map_findUInt64(map, "RxDropped");
	tmp->TxDropped  = variant_map_findUInt64(map, "TxDropped");
	tmp->Multicast  = variant_map_findUInt64(map, "Multicast");
	tmp->Collisions = variant_map_findUInt64(map, "Collisions");
}

static void statmon_netdev_sendNotification(int monitorId, netdev_data_t *data, netdev_data_t *tmp) {
	struct tm *now_tm = gmtime(&tmp->time);
	variant_t notificationValue;
	variant_map_t *notificationMap = NULL;
	variant_initialize(&notificationValue, variant_type_map);
	notificationMap = variant_da_map(&notificationValue);

	unsigned long long deltaRxPackets  = statmon_calculateDifference(data->RxPackets, tmp->RxPackets);
	unsigned long long deltaTxPackets  = statmon_calculateDifference(data->TxPackets, tmp->TxPackets);
	unsigned long long deltaRxBytes    = statmon_calculateDifference(data->RxBytes, tmp->RxBytes);
	unsigned long long deltaTxBytes    = statmon_calculateDifference(data->TxBytes, tmp->TxBytes);
	unsigned long long deltaRxErrors   = statmon_calculateDifference(data->RxErrors, tmp->RxErrors);
	unsigned long long deltaTxErrors   = statmon_calculateDifference(data->TxErrors, tmp->TxErrors);
	unsigned long long deltaRxDropped  = statmon_calculateDifference(data->RxDropped, tmp->RxDropped);
	unsigned long long deltaTxDropped  = statmon_calculateDifference(data->TxDropped, tmp->TxDropped);
	unsigned long long deltaMulticast  = statmon_calculateDifference(data->Multicast, tmp->Multicast);
	unsigned long long deltaCollisions = statmon_calculateDifference(data->Collisions, tmp->Collisions);


	variant_map_addDateTime(notificationMap, "Timestamp", now_tm);
	variant_map_addUInt64(notificationMap, "RxPackets",  data->RxPacketsTotal + deltaRxPackets);
	variant_map_addUInt64(notificationMap, "TxPackets",  data->TxPacketsTotal + deltaTxPackets);
	variant_map_addUInt64(notificationMap, "RxBytes",    data->RxBytesTotal + deltaRxBytes);
	variant_map_addUInt64(notificationMap, "TxBytes",    data->TxBytesTotal + deltaTxBytes);
	variant_map_addUInt64(notificationMap, "RxErrors",   data->RxErrorsTotal + deltaRxErrors);
	variant_map_addUInt64(notificationMap, "TxErrors",   data->TxErrorsTotal + deltaTxErrors);
	variant_map_addUInt64(notificationMap, "RxDropped",  data->RxDroppedTotal + deltaRxDropped);
	variant_map_addUInt64(notificationMap, "TxDropped",  data->TxDroppedTotal + deltaTxDropped);
	variant_map_addUInt64(notificationMap, "Multicast",  data->MulticastTotal + deltaMulticast);
	variant_map_addUInt64(notificationMap, "Collisions", data->CollisionsTotal + deltaCollisions);
	variant_map_addUInt64(notificationMap, "DeltaRxPackets",  deltaRxPackets);
	variant_map_addUInt64(notificationMap, "DeltaTxPackets",  deltaTxPackets);
	variant_map_addUInt64(notificationMap, "DeltaRxBytes",    deltaRxBytes);
	variant_map_addUInt64(notificationMap, "DeltaTxBytes",    deltaTxBytes);
	variant_map_addUInt64(notificationMap, "DeltaRxErrors",   deltaRxErrors);
	variant_map_addUInt64(notificationMap, "DeltaTxErrors",   deltaTxErrors);
	variant_map_addUInt64(notificationMap, "DeltaRxDropped",  deltaRxDropped);
	variant_map_addUInt64(notificationMap, "DeltaTxDropped",  deltaTxDropped);
	variant_map_addUInt64(notificationMap, "DeltaMulticast",  deltaMulticast);
	variant_map_addUInt64(notificationMap, "DeltaCollisions", deltaCollisions);
	statmon_monitor_sendNotification(monitorId, &notificationValue);
	variant_cleanup(&notificationValue);
}

static void statmon_netdev_handleStats(int monitorId, const variant_t *payload) {
	netdev_data_t *data = NULL;
	netdev_data_t tmp;
	time_t now_t;
	time(&now_t);
	SAH_TRACEZ_IN(ME);

	statmon_netdev_readPayload(monitorId, now_t, payload, &tmp);
	data = statmon_netdev_findStats(monitorId);
	if(data) {
		 statmon_netdev_sendNotification(monitorId, data, &tmp);
	}

	statmon_netdev_updateStats(data, &tmp);
	SAH_TRACEZ_OUT(ME);
}

static bool statmon_netdev_replyHandler(request_t *req, reply_item_t *item, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)pcb;
	(void)from;
	SAH_TRACEZ_IN(ME);
	int monitorId = (int)userdata;
	if (reply_item_type(item) == reply_type_error) {
		SAH_TRACE_ERROR("Function call error (%d) - %s - %s",
		reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
	} else if (reply_item_type(item) == reply_type_function_return) {
		const variant_t *payload = reply_item_returnValue(item);
		statmon_netdev_handleStats(monitorId, payload);
	}
	request_destroy(req);
	SAH_TRACEZ_OUT(ME);
	return true;
}

static void statmon_netdev_timeout(int monitorId, const char *path) {
	request_t *request = NULL;
	char intfpath[64];
	SAH_TRACEZ_IN(ME);

	statmon_getInterfacePathFromSubscriptionPath(path, intfpath, 64) ;
	if(*intfpath == '\0') {
		SAH_TRACEZ_OUT(ME);
		return;
	}

	SAH_TRACEZ_INFO(ME,"Calling %s (%s)", "getNetDevStats", intfpath);
	request = request_create_executeFunction(intfpath, "getNetDevStats", request_common_path_key_notation);
	if(!request)
		return;
	request_setReplyItemHandler(request, statmon_netdev_replyHandler);
	request_setData(request, (void *)monitorId);
	if(!pcb_sendRequest(plugin_getPcb(), plugin_getPeer(), request)) {
		SAH_TRACEZ_ERROR(ME, "Failed to send request");
	}
	SAH_TRACEZ_OUT(ME);
}

static void statmon_netdev_disabled(int monitorId) {
	llist_iterator_t *it = NULL;
	netdev_data_t *data = NULL;
	SAH_TRACEZ_IN(ME);

	SAH_TRACEZ_INFO(ME, "Disabling monitor %d netdevstats", monitorId);
	llist_for_each(it, &netdev_data_list) {
		data = llist_item_data(it, netdev_data_t, it);
		if(data->monitorId!=monitorId) {
			continue;
		}

		llist_iterator_take(&data->it);
		free(data);
		break;
	}
	SAH_TRACEZ_OUT(ME);
}


static void statmon_netdev_cleanup() {
	SAH_TRACEZ_IN(ME);
	llist_iterator_t *it = llist_first(&netdev_data_list);
	llist_iterator_t *next = NULL;
	while(it) {
		next = llist_iterator_next(it);
		netdev_data_t *data = llist_item_data(it, netdev_data_t, it);
		llist_iterator_take(&data->it);
		free(data);
		it = next;
	}
	llist_cleanup(&netdev_data_list);
	SAH_TRACEZ_OUT(ME);
}

void statmon_netdev_initialize() {
	SAH_TRACEZ_IN(ME);
	llist_initialize(&netdev_data_list);
	statmon_submodule_register(FLAG, statmon_type_netdev, statmon_netdev_timeout, statmon_netdev_disabled, statmon_netdev_cleanup);
	SAH_TRACEZ_OUT(ME);
}

