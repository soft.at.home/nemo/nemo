/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __MOD_STATMON_H__
#define __MOD_STATMON_H__

typedef enum {
	statmon_type_unknown = 0x00000000,
	statmon_type_netdev = 0x00000001,
	statmon_type_station = 0x00000002,
	statmon_type_dslline = 0x00000004
} statmon_type_t;

typedef void (* statmon_submodule_timer_handler_t) (int monitorId, const char *path);
typedef void (* statmon_submodule_monitor_disabled_handler_t) (int monitorId);
typedef void (* statmon_submodule_start_handler_t) ();
typedef void (* statmon_submodule_cleanup_handler_t) ();

unsigned long long statmon_calculateDifference(unsigned long long old, unsigned long long newval);

void statmon_blob_flagHandler(intf_t *intf, const char *flag, bool value, void *userdata);
void statmon_getInterfacePathFromSubscriptionPath(const char * pathIn, char *pathOut, int size);

void statmon_submodule_initialize();
void statmon_submodule_cleanup();
void statmon_submodule_register(const char *flag, statmon_type_t type, statmon_submodule_timer_handler_t timeout,
				statmon_submodule_monitor_disabled_handler_t disabled, statmon_submodule_cleanup_handler_t cleanup);
void statmon_submodule_handleTimeout(int monitorId, statmon_type_t type, const char *path);
void statmon_submodule_handleDisabled(int monitorId, statmon_type_t type);
void statmon_submodule_handleRemoved(int monitorId, statmon_type_t type);
void statmon_submodule_delFlagListener(intf_t *intf, intf_blob_t *blob);
void statmon_submodule_addFlagListener(intf_t *intf, intf_blob_t *blob);
statmon_type_t statmon_submodule_findType(const char *flag);


void statmon_monitor_initialize();
void statmon_monitor_cleanup();
void statmon_monitor_cleanupMonitors(int blobId);
void statmon_monitor_scheduleTimers(int blobId, bool physup, int available_stats);
void statmon_monitor_addOrUpdate(const char *intf, int blobId, statmon_type_t type, const char *name, bool enable, int interval);
void statmon_monitor_remove(int blobId, const char *name);
bool statmon_monitor_sendNotification(int monitorId, const variant_t *data);

void statmon_netdev_initialize();
void statmon_wlansta_initialize();
void statmon_dslline_initialize();

#endif // __MOD_STATMON_H__
