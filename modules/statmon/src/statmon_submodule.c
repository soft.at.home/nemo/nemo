/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "statmon"

#include <nemo/core.h>

#include "statmon.h"


typedef struct _statmon_submodule_t {
	llist_iterator_t it;
	const char *flag;
	statmon_type_t type;
	statmon_submodule_timer_handler_t timeout;
	statmon_submodule_monitor_disabled_handler_t disabled;
	statmon_submodule_cleanup_handler_t cleanup;
} statmon_submodule_t;

static llist_t statmon_submodule_list;

void statmon_submodule_initialize() {
	SAH_TRACEZ_IN(ME);
	llist_initialize(&statmon_submodule_list);
	SAH_TRACEZ_OUT(ME);
}

void statmon_submodule_cleanup() {
	llist_iterator_t *it = NULL;
	llist_iterator_t *next = NULL;
	statmon_submodule_t *plugin = NULL;
	SAH_TRACEZ_IN(ME);

	it = llist_first(&statmon_submodule_list);
	while(it) {
		next = llist_iterator_next(it);
		plugin = llist_item_data(it, statmon_submodule_t, it);
		if(plugin->cleanup)
			plugin->cleanup();
		it = next;
	}

	llist_cleanup(&statmon_submodule_list);
	SAH_TRACEZ_OUT(ME);
}

statmon_type_t statmon_submodule_findType(const char *flag) {
	llist_iterator_t *it = NULL;
	statmon_submodule_t *plugin = NULL;
	if(!flag)
		return statmon_type_unknown;

	llist_for_each(it,&statmon_submodule_list) {
		plugin = llist_item_data(it, statmon_submodule_t, it);
		if(strcmp(flag,plugin->flag)==0) {
			return plugin->type;
		}
	}
	return statmon_type_unknown;
}

void statmon_submodule_handleTimeout(int monitorId, statmon_type_t type, const char *path) {
	llist_iterator_t *it = NULL;
	statmon_submodule_t *plugin = NULL;
	SAH_TRACEZ_IN(ME);
	llist_for_each(it,&statmon_submodule_list) {
		plugin = llist_item_data(it, statmon_submodule_t, it);
		if(plugin->type == type) {
			if(plugin->timeout)
				plugin->timeout(monitorId, path);
		}
	}
	SAH_TRACEZ_OUT(ME);
}

void statmon_submodule_handleDisabled(int monitorId, statmon_type_t type) {
	llist_iterator_t *it = NULL;
	statmon_submodule_t *plugin = NULL;
	SAH_TRACEZ_IN(ME);
	llist_for_each(it,&statmon_submodule_list) {
		plugin = llist_item_data(it, statmon_submodule_t, it);
		if(plugin->type == type) {
			if(plugin->disabled)
				plugin->disabled(monitorId);
		}
	}
	SAH_TRACEZ_OUT(ME);
}

void statmon_submodule_handleRemoved(int monitorId, statmon_type_t type) {
	statmon_submodule_handleDisabled(monitorId, type);
}

void statmon_submodule_register(const char *flag, statmon_type_t type, statmon_submodule_timer_handler_t timeout, 
				statmon_submodule_monitor_disabled_handler_t disabled, statmon_submodule_cleanup_handler_t cleanup) {
	statmon_submodule_t *plugin = NULL;
	SAH_TRACEZ_IN(ME);

	SAH_TRACEZ_INFO(ME,"Submodule for flag %s registered", flag);
	plugin = calloc(1,sizeof(statmon_submodule_t));
	plugin->flag = flag;
	plugin->type = type;
	plugin->timeout = timeout;
	plugin->disabled = disabled;
	plugin->cleanup = cleanup;
	llist_iterator_initialize(&plugin->it);
	llist_append(&statmon_submodule_list, &plugin->it);
	SAH_TRACEZ_OUT(ME);
}


void statmon_submodule_addFlagListener(intf_t *intf, intf_blob_t *blob) {
	llist_iterator_t *it = NULL;
	statmon_submodule_t *plugin = NULL;
	SAH_TRACEZ_IN(ME);
	llist_for_each(it,&statmon_submodule_list) {
		plugin = llist_item_data(it, statmon_submodule_t, it);
		intf_addFlagListener(intf, plugin->flag, statmon_blob_flagHandler, blob, true);
	}
	SAH_TRACEZ_OUT(ME);
}

void statmon_submodule_delFlagListener(intf_t *intf, intf_blob_t *blob) {
	llist_iterator_t *it = NULL;
	statmon_submodule_t *plugin = NULL;
	SAH_TRACEZ_IN(ME);
	llist_for_each(it,&statmon_submodule_list) {
		plugin = llist_item_data(it, statmon_submodule_t, it);
		intf_delFlagListener(intf, plugin->flag, statmon_blob_flagHandler, blob, true);
	}
	SAH_TRACEZ_OUT(ME);
}
