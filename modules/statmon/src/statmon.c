/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "statmon"

#include <nemo/core.h>

#include "statmon.h"

struct _intf_blob {
	intf_t *intf;
	bool physup;
	int id;
	nemo_query_t *physupquery;
	request_t *statMonReq;
	int available_stats;
};


static int statmon_dummy;
static void *statmon_blobkey = &statmon_dummy;

static int unique_blob_id = 0;

unsigned long long statmon_calculateDifference(unsigned long long old, unsigned long long newval) {
	if(newval >= old) {
		return newval - old;
	} else {
		unsigned long long maxreached = old;

		// fill all bits right to the MSB with '1' to calculate the max for this specific driver 
		// e.g. might be a 16, 32 or 64 bits max.
		maxreached = maxreached | (maxreached >> 1);
		maxreached = maxreached | (maxreached >> 2);
		maxreached = maxreached | (maxreached >> 4);
		maxreached = maxreached | (maxreached >> 8);
		maxreached = maxreached | (maxreached >> 16);
		maxreached = maxreached | (maxreached >> 32);
		return newval + (maxreached-old);
	}
}

void statmon_getInterfacePathFromSubscriptionPath(const char * pathIn, char *pathOut, int size) {
	strncpy(pathOut, pathIn, size);
	char *ptr = strstr(pathOut, ".StatMon");
	if(!ptr) {
		*pathOut = '\0';
		return;
	}
	*ptr = '\0';
}

void statmon_blob_flagHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)(intf);
	statmon_type_t type = statmon_type_unknown;
	SAH_TRACEZ_IN(ME);
	intf_blob_t *blob = (intf_blob_t *)userdata;
	if(flag) {
		type = statmon_submodule_findType(flag);
		SAH_TRACEZ_INFO(ME,"Stats available for %s",intf_name(intf));
		if(value) {
			blob->available_stats |= type;
		} else {
			blob->available_stats &= !type;
		}
	}
	SAH_TRACEZ_OUT(ME);
}

static void statmon_blob_physupHandler(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	intf_t *intf = blob->intf;
	bool physup = false;
	SAH_TRACEZ_IN(ME);

	if (!intf) {
		SAH_TRACEZ_WARNING(ME, "Blob->intf is NULL");
		return;
	}

	physup = variant_bool(result);
	if (blob->physup == physup) {
		SAH_TRACEZ_NOTICE(ME, "No state change");
		return;
	}
	blob->physup = physup;
	statmon_monitor_scheduleTimers(blob->id, blob->physup, blob->available_stats);
	SAH_TRACEZ_OUT(ME);
}

static void statmon_blob_createMonitor(intf_blob_t *blob, object_t *object) {
	const char *name = NULL;
	uint32_t interval = 0;
	bool enable;
	const char *typeString = NULL;
	statmon_type_t type = statmon_type_unknown;

	name = object_name(object, path_attr_key_notation);
	enable = object_parameterBoolValue(object, "Enable");
	interval = object_parameterUInt32Value(object, "Interval");
	typeString = object_da_parameterCharValue(object, "Type");
	type = statmon_submodule_findType(typeString);

	statmon_monitor_addOrUpdate(intf_name(blob->intf), blob->id, type, name, enable, interval);
	statmon_monitor_scheduleTimers(blob->id, blob->physup,blob->available_stats);
}

static bool statmon_blob_itemReplyHandler(request_t *req, reply_item_t *item, 
		pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)req; (void)pcb; (void)from;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	const char *name = NULL;
	object_t *object = NULL;
	notification_t *notify = NULL;

	SAH_TRACEZ_IN(ME);

	if(reply_item_type(item) == reply_type_object) {
		object = reply_item_object(item);
		if(object_isTemplate(object)) {
			return true;
		}

		SAH_TRACEZ_INFO(ME, "Create new monitor for blob %d",blob->id);
		statmon_blob_createMonitor(blob, object);
	} else if(reply_item_type(item) == reply_type_notification) {
		notify = reply_item_notification(item);
		name = notification_objectName(notify);
		if (notify && notification_type(notify) == notify_object_added) {
			SAH_TRACEZ_INFO(ME, "Object %s was added", name);
		} else if(notify && notification_type(notify) == notify_object_deleted) {
			SAH_TRACEZ_INFO(ME, "Object %s was removed", name);
			statmon_monitor_remove(blob->id, name);
		}
	}

	SAH_TRACEZ_OUT(ME);
	return true;
}

static intf_blob_t *statmon_blob_create(intf_t *intf) {
	SAH_TRACEZ_IN(ME);
	intf_blob_t *blob = calloc(1, sizeof(intf_blob_t));
	char path[64];

	if (!blob) {
		SAH_TRACEZ_ERROR(ME, "Calloc failed");
		return NULL;
	}
	intf_addBlob(intf, statmon_blobkey, blob);
	blob->intf = intf;
	snprintf(path, 64, "NeMo.Intf.%s.StatMon", intf_name(intf));
	blob->physupquery = nemo_openQuery_isUp(intf_name(intf), ME, NULL, "down", statmon_blob_physupHandler, blob);

	blob->statMonReq = request_create_getObject(path, 1, request_common_path_key_notation | request_notify_values_changed | request_no_object_caching | request_getObject_parameters | request_notify_object_added | request_notify_object_deleted);
	request_setReplyItemHandler(blob->statMonReq, statmon_blob_itemReplyHandler);
	request_setData(blob->statMonReq, blob);
	if (!pcb_sendRequest(plugin_getPcb(), plugin_getPeer(), blob->statMonReq)) {
		SAH_TRACEZ_ERROR(ME, "Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
	}
	blob->id = unique_blob_id++;

	SAH_TRACEZ_INFO(ME, "Created new blob %d",blob->id);
	SAH_TRACEZ_OUT(ME);
	return blob;
}

static void statmon_blob_cleanup(intf_blob_t *blob) {
	SAH_TRACEZ_IN(ME);
	if (!blob) {
		SAH_TRACEZ_OUT(ME);
		return;
	}

	nemo_closeQuery(blob->physupquery);
	blob->physupquery = NULL;
	request_destroy(blob->statMonReq);
	blob->statMonReq = NULL;

	statmon_monitor_cleanupMonitors(blob->id);

	intf_delBlob(blob->intf, statmon_blobkey);
	free(blob);
	SAH_TRACEZ_OUT(ME);
}

static void statmon_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata;	(void)flag;
	intf_blob_t *blob = NULL;
	SAH_TRACEZ_IN(ME);
	if (value) {
		SAH_TRACEZ_INFO(ME,"Adding new blob for %s",intf_name(intf));
		blob = statmon_blob_create(intf);
		statmon_submodule_addFlagListener(intf, blob);
	} else {
		SAH_TRACEZ_INFO(ME,"Removing blob for %s",intf_name(intf));
		blob = intf_getBlob(intf, statmon_blobkey);
		statmon_submodule_delFlagListener(intf, blob);
		statmon_blob_cleanup(blob);
	}
	SAH_TRACEZ_OUT(ME);
}

static bool statmon_start(argument_value_list_t *args __attribute__((unused))) {
	SAH_TRACEZ_IN(ME);
	statmon_submodule_initialize();
	statmon_monitor_initialize();

	SAH_TRACEZ_INFO(ME,"Loading the plugins");
	statmon_netdev_initialize();
	statmon_wlansta_initialize();
	statmon_dslline_initialize();

	SAH_TRACEZ_INFO(ME,"Start working");
	intf_addFlagListener(NULL, "statmon", statmon_rootHandler, NULL, true);
	SAH_TRACEZ_OUT(ME);
	return true;
}

static void statmon_stop() {
	SAH_TRACEZ_IN(ME);
	statmon_submodule_cleanup();
	statmon_monitor_cleanup();

	intf_delFlagListener(NULL, "statmon", statmon_rootHandler, NULL, true);
	SAH_TRACEZ_OUT(ME);
}

static mtk_module_info_t statmon_info = {
	.name = ME,
	.start = statmon_start,
	.stop = statmon_stop
};

__attribute__((constructor)) static void statmon_init() {
	mtk_module_register(&statmon_info);
}

