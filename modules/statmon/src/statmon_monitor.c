/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "statmon"

#include <nemo/core.h>

#include "statmon.h"


typedef struct _statmon_list_entry_t {
	llist_iterator_t it;
	int id;
	int blobId;
	char *name;
	char path[64];
	bool enable;
	bool timerUpdated;
	statmon_type_t type;
	uint32_t interval;
	pcb_timer_t *timer;
} statmon_list_entry_t;

static int unique_id = 0;
static llist_t monitor_list;

static statmon_list_entry_t *statmon_monitor_findByBlobId(int blobId, const char *name) {
	statmon_list_entry_t *monitor = NULL;
	if (!name)
		return NULL;

	llist_iterator_t *it = NULL;
	llist_for_each(it, &monitor_list) {
		monitor = llist_item_data(it, statmon_list_entry_t, it);
		if(monitor->blobId == blobId && strcmp(name,monitor->name)==0) {
			return monitor;
		}
	}
	return NULL;
}

static statmon_list_entry_t *statmon_monitor_findByMonitorId(int monitorId) {
	statmon_list_entry_t *monitor = NULL;

	llist_iterator_t *it = NULL;
	llist_for_each(it, &monitor_list) {
		monitor = llist_item_data(it, statmon_list_entry_t, it);
		if(monitor->id == monitorId) {
			return monitor;
		}
	}
	return NULL;
}

static statmon_list_entry_t *statmon_monitor_findByTimer(pcb_timer_t *timer) {
	statmon_list_entry_t *monitor = NULL;
	if (!timer)
		return NULL;

	llist_iterator_t *it = NULL;
	llist_for_each(it, &monitor_list) {
		monitor = llist_item_data(it, statmon_list_entry_t, it);
		if(monitor->timer == timer) {
			return monitor;
		}
	}
	return NULL;
}


static void statmon_monitor_timerHandler(pcb_timer_t *timer, void *userdata) {
	(void)timer;
	(void)userdata;
	SAH_TRACEZ_IN(ME);
	statmon_list_entry_t *monitor = statmon_monitor_findByTimer(timer);
	if(!monitor) {
		SAH_TRACEZ_ERROR(ME,"Could not find monitor");
		return;
	}

	statmon_submodule_handleTimeout(monitor->id, monitor->type, monitor->path);

	SAH_TRACEZ_OUT(ME);
}

void statmon_monitor_scheduleTimers(int blobId, bool physup, int available_stats) {
	llist_iterator_t *it = NULL;
	statmon_list_entry_t *monitor = NULL;
	SAH_TRACEZ_IN(ME);

	SAH_TRACEZ_INFO(ME, "Scheduling timers for blobId %d, physup %d stats %d", blobId, physup, available_stats);

	llist_for_each(it, &monitor_list) {
		monitor = llist_item_data(it, statmon_list_entry_t, it);
		if(monitor->blobId != blobId)
			continue;

		if(physup && monitor->enable && available_stats&monitor->type) {
			if(!monitor->timer) {
				SAH_TRACEZ_INFO(ME, "Creating new timer for monitor %d", monitor->id);
				monitor->timer = pcb_timer_create();
				pcb_timer_setHandler(monitor->timer, statmon_monitor_timerHandler);
				pcb_timer_setInterval(monitor->timer, monitor->interval);
				pcb_timer_start(monitor->timer, 0);
			} else if(monitor->timerUpdated) {
				SAH_TRACEZ_INFO(ME, "Updating existing timer for monitor %d", monitor->id);
				monitor->timerUpdated = false;
				pcb_timer_start(monitor->timer, 0);
			} else {
				SAH_TRACEZ_INFO(ME, "Do nothing for monitor %d (type=%d)", monitor->id, monitor->type);
			}
		} else if(monitor->timer) {
			SAH_TRACEZ_INFO(ME, "Destroying timer for monitor %d", monitor->id);
			pcb_timer_destroy(monitor->timer);
			monitor->timer = NULL;
			statmon_submodule_handleDisabled(monitor->id, monitor->type);
		} else {
			SAH_TRACEZ_INFO(ME, "Do nothing for monitor %d (type=%d)", monitor->id, monitor->type);
		}
	}
	SAH_TRACEZ_OUT(ME);
}

statmon_list_entry_t *statmon_monitor_create(int blobId, const char *intfname, const char *name, 
					     bool enable, uint32_t interval, statmon_type_t type) {
	statmon_list_entry_t *monitor = NULL;
	SAH_TRACEZ_IN(ME);
	if(!intfname || !name || type == statmon_type_unknown) {
		SAH_TRACEZ_ERROR(ME, "No name or type");
		SAH_TRACEZ_OUT(ME);
		return NULL;
	}
	monitor = calloc(1, sizeof(statmon_list_entry_t));
	if(!monitor) {
		SAH_TRACEZ_ERROR(ME, "statmon - calloc failed");
		SAH_TRACEZ_OUT(ME);
		return NULL;
	}
	llist_iterator_initialize(&monitor->it);
	monitor->name = strdup(name);
	monitor->enable = enable;
	if(interval < 1000) {
		monitor->interval = 1000;
	} else {
		monitor->interval = interval;
	}
	snprintf(monitor->path, 64, "NeMo.Intf.%s.StatMon.%s", intfname, name);
	monitor->type = type;
	monitor->id = unique_id++;
	monitor->blobId = blobId;
	SAH_TRACEZ_INFO(ME, "Creating new monitor %d", monitor->id);

	SAH_TRACEZ_OUT(ME);
	return monitor;
}

static void statmon_monitor_destroy(statmon_list_entry_t *monitor) {
	SAH_TRACEZ_IN(ME);
	if (!monitor) {
		SAH_TRACEZ_OUT(ME);
		return;
	}
	SAH_TRACEZ_INFO(ME, "Destroying monitor %d",monitor->id);
	llist_iterator_take(&monitor->it);
	free(monitor->name);
	if(monitor->timer) {
		pcb_timer_destroy(monitor->timer);
	}
	free(monitor);
	SAH_TRACEZ_OUT(ME);
}

bool statmon_monitor_sendNotification(int monitorId, const variant_t *data) {
	statmon_list_entry_t *monitor = statmon_monitor_findByMonitorId(monitorId);
	SAH_TRACEZ_IN(ME);
	if(!monitor) {
		SAH_TRACEZ_OUT(ME);
		return false;
	}

	SAH_TRACEZ_INFO(ME, "Sending notification for monitor %d",monitorId);
	notification_t *notification = notification_create(200);
	if (!notification) {
		SAH_TRACEZ_ERROR(ME,"Failed to create notification");
		SAH_TRACEZ_OUT(ME);
		return true;
	}

	if (!notification_setObjectPath(notification,monitor->path)) {
		SAH_TRACEZ_ERROR(ME,"Failed to set object path in notification");
		notification_destroy(notification);
		SAH_TRACEZ_OUT(ME);
		return true;
	}
	notification_addParameter(notification,notification_parameter_createVariant("Statistics", data));

	// send notification, to every one
	if (!pcb_sendNotification(plugin_getPcb(), NULL, NULL, notification)) {
		SAH_TRACEZ_ERROR(ME,"Failed to send notification");
		notification_destroy(notification);
		SAH_TRACEZ_OUT(ME);
		return true;
	}

	notification_destroy(notification);
	SAH_TRACEZ_OUT(ME);
	return true;
}

void statmon_monitor_cleanupMonitors(int blobId) {
	SAH_TRACEZ_IN(ME);
	SAH_TRACEZ_INFO(ME, "Cleanup monitors for blob %d",blobId);
	llist_iterator_t *it = llist_first(&monitor_list);
	llist_iterator_t *next = NULL;
	while(it) {
		next = llist_iterator_next(it);
		statmon_list_entry_t *monitor = llist_item_data(it, statmon_list_entry_t, it);
		if(monitor->blobId == blobId) {
			statmon_monitor_destroy(monitor);
		}
		it = next;
	}
	SAH_TRACEZ_OUT(ME);
}

static void statmon_monitor_update(statmon_list_entry_t *monitor, bool enable, uint32_t interval, statmon_type_t type) {
	SAH_TRACEZ_IN(ME);
	if (!monitor) {
		SAH_TRACEZ_OUT(ME);
		return;
	}

	monitor->enable = enable;
	if(monitor->interval != interval) {
		if(interval < 1000) {
			monitor->interval = 1000;
		} else {
			monitor->interval = interval;
		}
		if(monitor->timer) {
			pcb_timer_setInterval(monitor->timer, monitor->interval);
		}
	}
	monitor->type = type;
	monitor->timerUpdated = true;
	SAH_TRACEZ_OUT(ME);
}

void statmon_monitor_addOrUpdate(const char *intf, int blobId, statmon_type_t type, 
				 const char *name, bool enable, int interval) {
	statmon_list_entry_t *monitor = NULL;
	SAH_TRACEZ_IN(ME);
	SAH_TRACEZ_INFO(ME,"Adding or updating blob %d type %d name %s", blobId, type, name);
	if (type == statmon_type_unknown || !name) {
		SAH_TRACEZ_OUT(ME);
		return;
	}

	monitor = statmon_monitor_findByBlobId(blobId, name);
	if(!monitor) {
		monitor = statmon_monitor_create(blobId, intf, name, enable, interval, type);
		llist_append(&monitor_list, &monitor->it);
	} else {
		statmon_monitor_update(monitor, enable, interval, type);
	}
	SAH_TRACEZ_OUT(ME);
}


void statmon_monitor_remove(int blobId, const char *name) {
	SAH_TRACEZ_IN(ME);
	SAH_TRACEZ_INFO(ME, "Removing monitor blob %d name %s", blobId, name);
	statmon_list_entry_t *monitor = NULL;
	if (!name)
		return;

	monitor = statmon_monitor_findByBlobId(blobId, name);
	if(monitor) {
		statmon_submodule_handleRemoved(monitor->id, monitor->type);
		statmon_monitor_destroy(monitor);
	}
	SAH_TRACEZ_OUT(ME);
}

void statmon_monitor_initialize() {
	SAH_TRACEZ_IN(ME);
	llist_initialize(&monitor_list);
	SAH_TRACEZ_OUT(ME);
}

void statmon_monitor_cleanup() {
	SAH_TRACEZ_IN(ME);
	llist_iterator_t *it = llist_first(&monitor_list);
	llist_iterator_t *next = NULL;
	statmon_list_entry_t *monitor = NULL;
	while(it) {
		next = llist_iterator_next(it);
		monitor = llist_item_data(it, statmon_list_entry_t, it);
		statmon_monitor_destroy(monitor);
		it = next;
	}
	llist_cleanup(&monitor_list);
	SAH_TRACEZ_OUT(ME);
}
