/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <pcb/utils.h>
#include <pcb/core.h>
#include <mtk.h>
#include <nemo/core.h>
#include "bonding.h"
#include "dsl_common.h"

#define BONDING "bonding"

static int bonding_dummy;
static void *bonding_blobkey = &bonding_dummy;
static object_t *t_object = NULL;
static int idx = 0;

typedef enum line_status_ {
    LINE_STATUS_DOWN,
    LINE_STATUS_UP_BONDED,
    LINE_STATUS_UP_SINGLELINE,
    LINE_STATUS_ERROR
} line_status_t;

typedef struct dsl_line {
    llist_iterator_t it;
    char *key; /* name of the dsl line */
    int idx; /* indication of line 0 or 1 */
    object_t *object;
    object_t *line_object;
    bool zombie;
    map_t *map;
    nemo_query_t *up_query;
    nemo_query_t *modulation_query; /* modulation might change during up state(ADSL) */
    line_status_t status;
    intf_t *intf; //pointer to bonding intf.
} dsl_line_t;

struct _intf_blob {
    map_t *map;
    map_t *stats_map;
    bool enabled;
    bool dstenabled;
    int32_t uptime;
    bool physup;
    intf_t *intf;
    llist_t lines;
    nemo_query_t *lowerintfquery;
    peer_info_t *dst;
};

static void bonding_enableHandler(intf_t *intf, const char *flag, bool value, void *userdata);
static void bonding_writeGroupStatus(parameter_t *parameter, const variant_t *oldvalue);
static void bonding_check(intf_blob_t *blob);
static void bonding_updateIntf(intf_t *intf, void *userdata);
static dsl_line_t *bonding_line_create(intf_blob_t *blob, const char *key);
static void bonding_lineHandler(const variant_t *result, void *userdata);
static void bonding_modulationHandler(const variant_t *result, void *userdata);
static void bonding_line_destroy(dsl_line_t *line);

static void bonding_line_destroy(dsl_line_t *line) {
    if (line) {
        SAH_TRACEZ_INFO(BONDING, "Destroy DSL line[%s]", line->key);
        free(line->key);
        line->key = NULL;
        llist_iterator_take(&line->it);
        line->intf = NULL;

        line->status = LINE_STATUS_DOWN;
        if (line->up_query) {
            nemo_closeQuery(line->up_query);
            line->up_query = NULL;
        }

        if (line->modulation_query) {
            nemo_closeQuery(line->modulation_query);
            line->modulation_query = NULL;
        }

        if (line->map) {
            map_destroy(line->map);
            line->map = NULL;
        }

        if (line->object) {
            object_setUserData(line->object, NULL);
            object_delete(line->object);
            object_commit(line->object);
            line->object = NULL;
        }

        line->line_object = NULL;
        free(line);
    }
}

static dsl_line_t *bonding_line_create(intf_blob_t *blob, const char *key) {
    dsl_line_t *line = calloc(1, sizeof(dsl_line_t));
    if (!line) {
        SAH_TRACE_ERROR("bonding - calloc failed");
        goto error;
    }

    llist_append(&blob->lines, &line->it);
    line->status = LINE_STATUS_DOWN;
    line->key = strdup(key);
    line->intf = blob->intf;
    line->idx = idx;
    idx++;
    if (!line->key) {
        SAH_TRACE_ERROR("bonding - strdup[%s] failed", key);
        goto error;
    }

    char path[128];
    memset(path, 0, 128);
    snprintf(path, 127, "LLIntf.%s", key);
    object_t *object = plugin_intf2object(blob->intf);
    SAH_TRACEZ_INFO(BONDING, "path[%s]", path);

    line->object = object_getObject(object, path, path_attr_key_notation, NULL);
    if (!line->object) {
        SAH_TRACEZ_WARNING(BONDING, "[%s]: object is NULL [%s]", intf_name(blob->intf), path);
    }

    line->map = map_create(line->object, MAP_OWNER, get_dst(object, &blob->dst), "%s.BondingGroup.%s.Line.%s", get_dslroot(object), intf_name(blob->intf), key);
    if (!line->map) {
        SAH_TRACEZ_WARNING(BONDING, "[%s]: create map [%s] failed", intf_name(blob->intf), key);
        goto error;
    }

    map_parameter_create(line->map, object_getParameter(line->object, "Name"), "Line", MAP_WRITEONLY, NULL, NULL);

    snprintf(path, 127, "NeMo.Intf.%s", key);
    line->line_object = object_getObject(t_object, path, path_attr_key_notation, NULL);

    map_commit(line->map);
    line->up_query = nemo_openQuery_isUp(key, BONDING, "dslline", "this", bonding_lineHandler, line);

    return line;
error:
    bonding_line_destroy(line);
    return NULL;
}

long dsl_uptime(void) {
    char buf[64], *ptr = NULL;
    FILE *f = fopen("/proc/uptime", "r"); // Remark using sysinfo() would be easier but it's not available in uclibc
    if (f) {
        ptr = fgets(buf, 64, f);
        fclose(f);
    }
    return ptr? strtol(ptr, NULL, 0): 0;
}

static void bonding_writeGroupStatus(parameter_t *parameter, const variant_t *oldvalue) {
    (void)oldvalue;
    intf_t *intf = plugin_object2intf(parameter_owner(parameter));
    const char *value = string_buffer(variant_da_string(parameter_getValue(parameter)));
    if (!strcmp(value? value: "", "Up")) {
        SAH_TRACEZ_INFO(BONDING, "Mark interface[%s] (dslbonding)up", intf_name(intf));
        flagset_set(intf_flagset(intf), "dslbonding-up");
    }
    else {
        SAH_TRACEZ_INFO(BONDING, "Remove (dslbonding)up flag from interface[%s]", intf_name(intf));
        flagset_clear(intf_flagset(intf), "dslbonding-up");
    }
    object_commit(parameter_owner(parameter));
}

static void bonding_check(intf_blob_t *blob) {
    if (blob->enabled == blob->dstenabled) {
        SAH_TRACEZ_INFO(BONDING, "Bonding enable is not changed");
        return;
    }

    /* Enable the configured lines */
    llist_iterator_t *it = NULL;
    llist_for_each(it, &blob->lines) {
        dsl_line_t *line = llist_item_data(it, dsl_line_t, it);

        //set line NeMo.Intf.dslX.Enable = true
        if (line->line_object) {
            SAH_TRACEZ_NOTICE(BONDING, "Enable bonding line[%s]", line->key);
            object_parameterSetBoolValue(line->line_object, "Enable", true);
            object_commit(line->line_object);
        }
        else {
            SAH_TRACEZ_NOTICE(BONDING, "No bonding line[%s] object found", line->key);
        }
    }

    request_t *req = request_create_setObject(map_dstpath(blob->map), request_common_path_key_notation);
    variant_t var;
    object_t *object = plugin_intf2object(blob->intf);
    variant_initialize(&var, variant_type_unknown);
    variant_setBool(&var, blob->enabled);
    SAH_TRACEZ_INFO(BONDING, "request_addParameter(path=%s param=%s value=%s)", map_dstpath(blob->map), "Enable", blob->enabled? "true": "false");
    request_addParameter(req, "Enable", &var);
    if (!pcb_sendRequest(plugin_getPcb(), get_dst(object, &blob->dst), req)) {
        SAH_TRACE_ERROR("bonding - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
    }
    request_destroy(req);
    variant_cleanup(&var);
    blob->dstenabled = blob->enabled;
}

static void bonding_enableHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
    (void)intf;
    intf_blob_t *blob = (intf_blob_t *)userdata;
    if (!strcmp(flag, "enabled")) {
        SAH_TRACEZ_INFO(BONDING, "flag(enabled), value[%s]", value? "Enable": "Disabled");
        blob->enabled = value;
        bonding_check(blob);
    }
}

static void bonding_updateIntf(intf_t *intf, void *userdata) {
    (void)intf;
    intf_blob_t *blob = (intf_blob_t *)userdata;
    if (blob) {
        map_commit(blob->map);
        map_commit(blob->stats_map);
    }
}

static void bonding_upHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
    (void)flag;
    (void)userdata;
    (void)value;
    object_t *object = plugin_intf2object(intf);
    object_parameterSetUInt32Value(object, "LastChangeTime", dsl_uptime());
    object_commit(object);
}

static dsl_line_t *bonding_line_find(llist_t *lines, const char *name) {
    dsl_line_t *line = NULL;
    for (line = (dsl_line_t *)llist_first(lines); line; line = (dsl_line_t *)llist_iterator_next(&line->it))
        if (!strcmp(line->key, name))
            return line;
    return NULL;
}

static void bonding_llintfHandler(const variant_t *result, void *userdata) {
    intf_blob_t *blob = (intf_blob_t *)userdata;

    /* Mark all intf zombie */
    dsl_line_t *line = NULL, *linenext = NULL;
    llist_iterator_t *it = NULL;
    llist_for_each (it, &blob->lines) {
        line = llist_item_data(it, dsl_line_t, it);
        line->zombie = true;
    }

    variant_list_t *ar = variant_da_list(result);
    variant_list_iterator_t *v_it = NULL;

    variant_list_for_each(v_it, ar) {
        variant_t *v = variant_list_iterator_data(v_it);
        const char *iface = variant_da_char(v);
        if (iface) {
            line = bonding_line_find(&blob->lines, iface);
            if (line) {
                line->zombie = false;
            }
            else {
                line = bonding_line_create(blob, iface);
            }
        }
    }

    for (line = (dsl_line_t *)llist_first(&blob->lines); line; line = linenext) {
        linenext = (dsl_line_t *)llist_iterator_next(&line->it);
        if (line->zombie) {
            bonding_line_destroy(line);
        }
    }
}

static void bonding_lineHandler(const variant_t *result, void *userdata) {
    dsl_line_t *line = (dsl_line_t *)userdata;

    char line_s[16] = {};
    snprintf(line_s, 15, "line%d-up", line->idx);

    bool up = variant_bool(result);
    if (up) {
        SAH_TRACEZ_WARNING(BONDING, "Line[%s] goes up, start modulation query", line->key);
        line->modulation_query = nemo_openQuery_getFirstParameter(line->key, BONDING, "Line_ModulationType", "dslline", "this", bonding_modulationHandler, line);
    }
    else {
        SAH_TRACEZ_WARNING(BONDING, "Line[%s] goes down", line->key);
        if (line->modulation_query) {
            nemo_closeQuery(line->modulation_query);
            line->modulation_query = NULL;
        }

        line->status = LINE_STATUS_DOWN;
        flagset_clear(intf_flagset(line->intf), line_s);
    }
}

static void bonding_modulationHandler(const variant_t *result, void *userdata) {
    dsl_line_t *line = (dsl_line_t *)userdata;
    const char *modulation = variant_da_char(result);
    char line_s[16] = {};
    snprintf(line_s, 15, "line%d-up", line->idx);

    SAH_TRACEZ_WARNING(BONDING, "Line[%s] is up, modulation[%s]", line->key, modulation);
    if (!modulation)
        modulation = "";
    if (strcmp(modulation, "ADSL") == 0) {
        SAH_TRACEZ_INFO(BONDING, "ADSL Single line[%s]", line->key);
        line->status = LINE_STATUS_UP_SINGLELINE;
        flagset_set(intf_flagset(line->intf), line_s);
    }
    else if (strcmp(modulation, "VDSL") == 0) {
        SAH_TRACEZ_INFO(BONDING, "VDSL Single line[%s]", line->key);
        line->status = LINE_STATUS_UP_SINGLELINE;
        flagset_set(intf_flagset(line->intf), line_s);
    }
    else if (strcmp(modulation, "Bonding_ADSL") == 0) {
        SAH_TRACEZ_INFO(BONDING, "ADSL Bonded line[%s]", line->key);
        line->status = LINE_STATUS_UP_BONDED;
        flagset_clear(intf_flagset(line->intf), line_s);
    }
    else if (strcmp(modulation, "Bonding_VDSL") == 0) {
        SAH_TRACEZ_INFO(BONDING, "VDSL Bonded line[%s]", line->key);
        line->status = LINE_STATUS_UP_BONDED;
        flagset_clear(intf_flagset(line->intf), line_s);
    }
    else {
        SAH_TRACEZ_WARNING(BONDING, "[%s] Unsupported modulation[%s]", line->key, modulation);
        line->status = LINE_STATUS_DOWN;
        flagset_clear(intf_flagset(line->intf), line_s);
    }
}

static bool bonding_readLastChange(parameter_t *parameter, variant_t *value) {
    uint32_t t0 = object_parameterUInt32Value(parameter_owner(parameter), "LastChangeTime");
    if (t0)
        variant_setUInt32(value, dsl_uptime() - t0);
    else
        variant_setUInt32(value, 0);
    return true;
}

static void bonding_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
    (void)userdata;	(void)flag;
    object_t *object = plugin_intf2object(intf);
    if (value) {
        intf_blob_t *blob = calloc(1, sizeof(intf_blob_t));
        if (!blob) {
            SAH_TRACE_ERROR("bonding - calloc failed");
            return;
        }

        llist_initialize(&blob->lines);

        intf_addBlob(intf, bonding_blobkey, blob);
        blob->intf = intf;
        object_t *object = plugin_intf2object(intf);
        blob->map = map_create(object, MAP_OWNER, get_dst(object, &blob->dst), "%s.BondingGroup.%s", get_dslroot(object), intf_name(intf));
        map_parameter_create(blob->map, object_getParameter(object, "BondingStatus"), "Status", MAP_READONLY, NULL, NULL);

        map_commit(blob->map);
        SAH_TRACEZ_NOTICE(BONDING, "Create [%s.BondingGroup.%s]", get_dslroot(object), intf_name(intf));

        blob->stats_map = map_create(object, MAP_OWNER, get_dst(object, &blob->dst), "%s.BondingGroup.%s.Stats.Total", get_dslroot(object), intf_name(intf));
        map_parameter_create(blob->stats_map, object_getParameter(object, "FailureReasons"), NULL, MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->stats_map, object_getParameter(object, "UpstreamRate"), NULL, MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->stats_map, object_getParameter(object, "DownstreamRate"), NULL, MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->stats_map, object_getParameter(object, "UpstreamPacketLoss"), NULL, MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->stats_map, object_getParameter(object, "DownstreamPacketLoss"), NULL, MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->stats_map, object_getParameter(object, "UpstreamDifferentialDelay"), NULL, MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->stats_map, object_getParameter(object, "DownstreamDifferentialDelay"), NULL, MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->stats_map, object_getParameter(object, "ErroredSeconds"), NULL, MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->stats_map, object_getParameter(object, "SeverelyErroredSeconds"), NULL, MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->stats_map, object_getParameter(object, "UnavailableSeconds"), NULL, MAP_READONLY, NULL, NULL);
        map_commit(blob->stats_map);

        plugin_setParameterWriteHandler(object_getParameter(object, "BondingStatus"), bonding_writeGroupStatus);
        plugin_setParameterReadHandler(object_getParameter(object, "LastChange"), bonding_readLastChange);

        intf_addFlagListener(intf, "enabled", bonding_enableHandler, blob, true);
        intf_addMgmtListener(intf, NULL, bonding_updateIntf, NULL, blob, false);
        intf_addFlagListener(intf, "up", bonding_upHandler, NULL, true);
        blob->lowerintfquery = nemo_openQuery_getIntfs(intf_name(intf), BONDING, "dslline", "one level down", bonding_llintfHandler, blob);
    }
    else {
        intf_blob_t *blob = intf_getBlob(intf, bonding_blobkey);
        if (!blob) {
            SAH_TRACEZ_WARNING(BONDING, "No blob found");
            return;
        }

        plugin_setParameterWriteHandler(object_getParameter(object, "GroupStatus"), NULL);
        plugin_setParameterReadHandler(object_getParameter(object, "LastChange"), NULL);
        intf_delFlagListener(intf, "enabled", bonding_enableHandler, blob, false);
        intf_delMgmtListener(intf, NULL, bonding_updateIntf, NULL, blob, false);
        intf_delFlagListener(intf, "up", bonding_upHandler, NULL, true);

        if (blob->map) {
            map_destroy(blob->map);
            blob->map = NULL;
        }

        if (blob->stats_map) {
            map_destroy(blob->stats_map);
            blob->stats_map = NULL;
        }

        intf_delBlob(intf, bonding_blobkey);

        llist_cleanup(&blob->lines);

        nemo_closeQuery(blob->lowerintfquery);
        blob->lowerintfquery = NULL;
        bonding_llintfHandler(NULL, blob);

        free(blob);
    }
}

bool bonding_start(object_t *obj) {
    //sahTraceAddZone(400, BONDING);

    SAH_TRACEZ_INFO(BONDING, "Initializing bonding object");
    t_object = obj;

    pcb_t *pcb = mtk_getPcb();
    t_object = datamodel_root(pcb_datamodel(pcb));

    intf_addFlagListener(NULL, "dslbonding", bonding_rootHandler, NULL, true);
    return true;
}

void bonding_stop(void) {
    SAH_TRACEZ_INFO(BONDING, "Cleaning up bonding module");
    intf_delFlagListener(NULL, "dslbonding", bonding_rootHandler, NULL, true);
}

