/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "dsl.h"
#include "bonding.h"
#include <mtk.h>

#include <nemo/core.h>
#include "dsl_common.h"

#define ME "dsl"

struct _intf_blob {
	bool isBonding;
	map_t *map;
	map_t *line_map;
	bool enabled;
	bool dstenabled;
	int32_t uptime;
	bool physup;
	intf_t *intf;
	peer_info_t *dst;
	nemo_query_t *singleline0_query;
	nemo_query_t *singleline1_query;
};

static int dsl_dummy;
static void *dsl_blobkey = &dsl_dummy;
static object_t *t_object = NULL;

static void dsl_updateIntf(intf_t *intf, void *userdata);
static void dsl_writeLinkStatus(parameter_t *parameter, const variant_t *oldvalue);
static bool dsl_readLastChange(parameter_t *parameter, variant_t *value);
static void map_channel_data(intf_blob_t *blob, object_t *object, const char *name);
static void map_bonding_data(intf_blob_t *blob, object_t *object);

static void dsl_updateIntf(intf_t *intf, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	map_commit(blob->map);
	if (blob->line_map) {
		map_commit(blob->line_map);
	}
}

static void dsl_writeLinkStatus(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	intf_t *intf = plugin_object2intf(parameter_owner(parameter));
	const char *value = string_buffer(variant_da_string(parameter_getValue(parameter)));
	if (!strcmp(value? value: "", "Up")) {
		SAH_TRACEZ_INFO(ME, "Mark interface[%s] up", intf_name(intf));
		flagset_set(intf_flagset(intf), "up");
	}
	else {
		SAH_TRACEZ_INFO(ME, "Remove up flag from interface[%s]", intf_name(intf));
		flagset_clear(intf_flagset(intf), "up");
	}
	object_parameterSetUInt32Value(parameter_owner(parameter), "LastChangeTime", dsl_uptime());
	object_commit(parameter_owner(parameter));
}

static bool dsl_readLastChange(parameter_t *parameter, variant_t *value) {
	uint32_t t0 = object_parameterUInt32Value(parameter_owner(parameter), "LastChangeTime");
	if (t0) {
		//SAH_TRACEZ_INFO(ME, "Set LastChangeTime parameter[%lu]", dsl_uptime() - t0);
		variant_setUInt32(value, dsl_uptime() - t0);
	}
	else {
		//SAH_TRACEZ_INFO(ME, "Set LastChangeTime parameter(0)");
		variant_setUInt32(value, 0);
	}
	return true;
}

static void dsl_upHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata;
	(void)flag;
	(void)value;
	object_t *object = plugin_intf2object(intf);
	object_parameterSetUInt32Value(object, "LastChangeTime", dsl_uptime());
	object_commit(object);
}

static void singlelineHandler(const variant_t *result, void *userdata, int line) {
	bool up = variant_bool(result);
	intf_blob_t *blob = (intf_blob_t *)userdata;

	if (!up) {
		SAH_TRACEZ_NOTICE(ME, "DSL line[%d] goes down", line);
		return;
	}
	SAH_TRACEZ_WARNING(ME, "DSL line[%d] comes up as single line", line);

	if (blob->map) {
		map_destroy(blob->map);
		blob->map = NULL;
	}

	if (blob->line_map) {
		map_destroy(blob->line_map);
		blob->line_map = NULL;
	}

	SAH_TRACEZ_INFO(ME, "Map [%s] to single line config", intf_name(blob->intf));
	object_t *object = plugin_intf2object(blob->intf);
	//TODO better not hardcode the line0/1
	const char *linename = (line == 0)? "line0": "line1";
	map_channel_data(blob, object, linename);
	if (blob->map) {
		map_commit(blob->map);
		if (blob->line_map) {
			map_commit(blob->line_map);
		}
	}
	SAH_TRACEZ_INFO(ME, "Mapping [%s] done", intf_name(blob->intf));
}

static void singleline0Handler(const variant_t *result, void *userdata) {
	singlelineHandler(result, userdata, 0);
}

static void singleline1Handler(const variant_t *result, void *userdata) {
	singlelineHandler(result, userdata, 1);
}

static void map_bonding_data(intf_blob_t *blob, object_t *object) {
	if (!blob) {
		return;
	}
	SAH_TRACEZ_NOTICE(ME, "Map bonding data");
	blob->map = map_create(object, MAP_LEARN, get_dst(object, &blob->dst), "%s.BondingGroup.%s", get_dslroot(object), intf_name(blob->intf));
	map_parameter_create(blob->map, object_getParameter(object, "UpstreamCurrRate"), "AggregatedUpstreamRate", MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->map, object_getParameter(object, "DownstreamCurrRate"), "AggregatedDownstreamRate", MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->map, object_getParameter(object, "LinkStatus"), "Status", MAP_READONLY, NULL, NULL);

	blob->line_map = map_create(object, MAP_LEARN, get_dst(object, &blob->dst), "%s.BondingGroup.%s.Compat", get_dslroot(object), intf_name(blob->intf));
	map_parameter_create(blob->line_map, object_getParameter(object, "UpstreamMaxRate"), "UpstreamMaxBitRate", MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "DownstreamMaxRate"), "DownstreamMaxBitRate", MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "UpstreamNoiseMargin"), NULL, MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "DownstreamNoiseMargin"), NULL, MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "UpstreamPower"), NULL, MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "DownstreamPower"), NULL, MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "StandardsSupported"), NULL, MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "StandardUsed"), NULL, MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "UpstreamAttenuation"), "UpstreamSignalAttenuation", MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "DownstreamAttenuation"), "DownstreamSignalAttenuation", MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "UpstreamLineAttenuation"), "UpstreamLineAttenuation", MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "DownstreamLineAttenuation"), "DownstreamLineAttenuation", MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "FirmwareVersion"), "FirmwareVersion", MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "AllowedProfiles"), NULL, MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "CurrentProfile"), NULL, MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->line_map, object_getParameter(object, "ModulationType"), NULL , MAP_READONLY, NULL, NULL);
	map_parameter_create(blob->map, object_getParameter(object, "ChannelEncapsulationType"), "EncapsulationType" , MAP_READONLY, NULL, NULL);
}

static void map_channel_data(intf_blob_t *blob, object_t *object, const char *name) {
    if (!blob || !object) {
        SAH_TRACEZ_NOTICE(ME, "Missing parameters");
        return;
    }

    SAH_TRACEZ_NOTICE(ME, "Map channel[%s] data", name);
    blob->map = map_create(object, MAP_LEARN, get_dst(object, &blob->dst), "%s.Channel.%s", get_dslroot(object), name);
    if (blob->map) {
        map_parameter_create(blob->map, object_getParameter(object, "UpstreamCurrRate"), "UpstreamBitRate", MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->map, object_getParameter(object, "DownstreamCurrRate"), "DownstreamBitRate", MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->map, object_getParameter(object, "DataPath"), "LPATH", MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->map, object_getParameter(object, "InterleaveDepth"), "INTLVDEPTH", MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->map, object_getParameter(object, "LinkStatus"), "Status", MAP_READONLY, NULL, NULL);
        map_parameter_create(blob->map, object_getParameter(object, "ChannelEncapsulationType"), "LinkEncapsulationUsed", MAP_READONLY, NULL, NULL);

        /* MAP DSL parameters to DSL line parameters in the DSL plugin */
        blob->line_map = map_create(object, MAP_LEARN, get_dst(object, &blob->dst), "%s.Line.%s", get_dslroot(object), name);
        if (blob->line_map) {
            map_parameter_create(blob->line_map, object_getParameter(object, "Name"), NULL, MAP_WRITEONLY | MAP_KEY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "UpstreamMaxRate"), "UpstreamMaxBitRate", MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "DownstreamMaxRate"), "DownstreamMaxBitRate", MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "UpstreamNoiseMargin"), NULL, MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "DownstreamNoiseMargin"), NULL, MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "UpstreamPower"), NULL, MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "DownstreamPower"), NULL, MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "StandardsSupported"), NULL, MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "StandardUsed"), NULL, MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "UpstreamAttenuation"), "UpstreamSignalAttenuation", MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "DownstreamAttenuation"), "DownstreamSignalAttenuation", MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "UpstreamLineAttenuation"), "UpstreamLineAttenuation", MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "DownstreamLineAttenuation"), "DownstreamLineAttenuation", MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "FirmwareVersion"), "FirmwareVersion", MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "AllowedProfiles"), NULL, MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "CurrentProfile"), NULL, MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "ModulationType"), NULL, MAP_READONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "ModulationHint"), NULL, MAP_WRITEONLY, NULL, NULL);
            map_parameter_create(blob->line_map, object_getParameter(object, "UPBOKLE"), NULL, MAP_READONLY, NULL, NULL);
        }
        else {
            SAH_TRACEZ_WARNING(ME, "Map Line[%s] data failed", name);
        }
    }
    else {
        SAH_TRACEZ_WARNING(ME, "Map channel[%s] data failed", name);
    }
}

static void dsl_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata;	(void)flag;

	object_t *object = plugin_intf2object(intf);
	if (value) {
		intf_blob_t *blob = calloc(1, sizeof(intf_blob_t));
		if (!blob) {
			SAH_TRACE_ERROR("dsl - calloc failed");
			return;
		}
		intf_addBlob(intf, dsl_blobkey, blob);
		blob->intf = intf;

		flagset_t *flags = intf_flagset(intf);

		if (flagset_check(flags, "dslbonding")) {
			blob->isBonding = true;
			SAH_TRACEZ_NOTICE(ME, "intf[%s] is a bonding DSL interface", intf_name(intf));
		}
		else {
			blob->isBonding = false;
		}

		if (blob->isBonding) {
			/* by default we assume we are in bonded mode, and we expect a bonded line */
			map_bonding_data(blob, object);

			/* Handle race condition, single line instead of bonding */
			blob->singleline0_query = nemo_openQuery_hasFlag(intf_name(intf), ME, "line0-up", "!dslbonding-up", "this", singleline0Handler, blob);
			blob->singleline1_query = nemo_openQuery_hasFlag(intf_name(intf), ME, "line1-up", "!dslbonding-up", "this", singleline1Handler, blob);
		}
		else {
			map_channel_data(blob, object, intf_name(intf));
		}

		plugin_setParameterWriteHandler(object_getParameter(object, "LinkStatus"), dsl_writeLinkStatus);
		plugin_setParameterReadHandler(object_getParameter(object, "LastChange"), dsl_readLastChange);

		intf_addMgmtListener(intf, NULL, dsl_updateIntf, NULL, blob, false);
		intf_addFlagListener(intf, "up", dsl_upHandler, NULL, true);
		map_commit(blob->map);
		if (blob->line_map) {
			map_commit(blob->line_map);
		}
	}
	else {
		intf_blob_t *blob = intf_getBlob(intf, dsl_blobkey);
		if (!blob) {
			SAH_TRACEZ_WARNING(ME, "No blob found");
			return;
		}

		plugin_setParameterWriteHandler(object_getParameter(object, "LinkStatus"), NULL);
		intf_delMgmtListener(intf, NULL, dsl_updateIntf, NULL, blob, false);
		intf_delFlagListener(intf, "up", dsl_upHandler, NULL, true);
		plugin_setParameterReadHandler(object_getParameter(object, "LastChange"), NULL);

	   	if (blob->map) {
			map_destroy(blob->map);
			blob->map = NULL;
		}

	   	if (blob->line_map) {
			map_destroy(blob->line_map);
			blob->line_map = NULL;
		}

	   	if (blob->singleline0_query) {
			nemo_closeQuery(blob->singleline0_query);
	   		blob->singleline0_query = NULL;
		}
		if (blob->singleline1_query) {
			nemo_closeQuery(blob->singleline1_query);
	   		blob->singleline1_query = NULL;
		}
		intf_delBlob(intf, dsl_blobkey);
		free(blob);
	}
}

bool dsl_start(object_t *obj) {
	//sahTraceAddZone(400, ME);
	t_object = obj;
	intf_addFlagListener(NULL, "dsl", dsl_rootHandler, NULL, true);
	return true;
}

void dsl_stop(void) {
	SAH_TRACEZ_INFO(ME, "Cleaning up dsl module");
	intf_delFlagListener(NULL, "dsl", dsl_rootHandler, NULL, true);
	t_object = NULL;
}

