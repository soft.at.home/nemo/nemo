/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "dsl"

#include <nemo/core.h>
#include "bonding.h"
#include "dsl.h"
#include "dsl_common.h"

struct _intf_blob {
	map_t *map;
	map_t *channel_map;
	bool enabled;
	bool dstenabled;
	int32_t uptime;
	bool physup;
	intf_t *intf;
	peer_info_t *dst;
};

static int dsl_dummy;
static void *dsl_blobkey = &dsl_dummy;
static object_t *t_object;

static void dsl_updateIntf(intf_t *intf, void *userdata);
static void dsl_enableHandler(intf_t *intf, const char *flag, bool value, void *userdata);
static void dsl_check(intf_blob_t *blob);
static void dsl_upHandler(intf_t *intf, const char *flag, bool value, void *userdata);

static function_exec_state_t getDSLLineStats(function_call_t *fcall, argument_value_list_t *args,
		variant_t *retval) {
	(void)args; (void)retval;
	object_t *object = fcall_object(fcall);
	intf_t *intf = plugin_object2intf(object);
	intf_blob_t *blob = intf_getBlob(intf, dsl_blobkey);
	if (!blob) {
		SAH_TRACEZ_WARNING(ME, "No blob found");
		return function_exec_error;
	}

	return map_call_create(blob->map?map_function_findByName(blob->map, "getDSLLineStats"):NULL, fcall, args, retval, NULL)
		? function_exec_executing : function_exec_error;
}

static function_exec_state_t getDSLStats(function_call_t *fcall, argument_value_list_t *args,
		variant_t *retval) {
	(void)args; (void)retval;
	object_t *object = fcall_object(fcall);
	intf_t *intf = plugin_object2intf(object);
	intf_blob_t *blob = intf_getBlob(intf, dsl_blobkey);
	if (!blob) {
		SAH_TRACEZ_WARNING(ME, "No blob found");
		return function_exec_error;
	}

	return map_call_create(blob->map?map_function_findByName(blob->map, "getDSLStats"):NULL, fcall, args, retval, NULL)
		? function_exec_executing : function_exec_error;
}

static function_exec_state_t getXDSLNoiseMeasure(function_call_t *fcall, argument_value_list_t *args,
		variant_t *retval) {
	(void)args; (void)retval;
	object_t *object = fcall_object(fcall);
	intf_t *intf = plugin_object2intf(object);
	intf_blob_t *blob = intf_getBlob(intf, dsl_blobkey);
	if (!blob) {
		SAH_TRACEZ_WARNING(ME, "No blob found");
		return function_exec_error;
	}

	return map_call_create(blob->map?map_function_findByName(blob->map, "getXDSLNoiseMeasure"):NULL, fcall, args, retval, NULL)
		? function_exec_executing : function_exec_error;
}

static function_exec_state_t getDSLChannelStats(function_call_t *fcall, argument_value_list_t *args,
		variant_t *retval) {
	(void)args; (void)retval;
	object_t *object = fcall_object(fcall);
	intf_t *intf = plugin_object2intf(object);
	intf_blob_t *blob = intf_getBlob(intf, dsl_blobkey);
	if (!blob) {
		SAH_TRACEZ_WARNING(ME, "No blob found");
		return function_exec_error;
	}

	return map_call_create(blob->channel_map?map_function_findByName(blob->channel_map, "getDSLChannelStats"):NULL, fcall, args, retval, NULL)
		? function_exec_executing : function_exec_error;
}

static void dsl_writeLinkStatus(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	intf_t *intf = plugin_object2intf(parameter_owner(parameter));
	const char *value = string_buffer(variant_da_string(parameter_getValue(parameter)));
	if (flagset_check(intf_flagset(intf), "dsl") == false ) {
		SAH_TRACEZ_INFO(ME, "value is [%s]", value);
		if (!strcmp(value? value: "", "Up")) {
			SAH_TRACEZ_INFO(ME, "Mark interface[%s] up", intf_name(intf));
			flagset_set(intf_flagset(intf), "up");
		}
		else  {
			//SAH_TRACEZ_INFO(ME, "Remove up flag from interface[%s]", intf_name(intf));
			flagset_clear(intf_flagset(intf), "up");
		}

		object_parameterSetUInt32Value(parameter_owner(parameter), "Line_LastChangeTime", dsl_uptime());
		object_commit(parameter_owner(parameter));
	}
	else  {
		SAH_TRACEZ_INFO(ME, "Skip on interface[%s] in single line", intf_name(intf));
	}
}

static bool dsl_readLastChange(parameter_t *parameter, variant_t *value) {
	uint32_t t0 = object_parameterUInt32Value(parameter_owner(parameter), "Line_LastChangeTime");
	if (t0) {
		//SAH_TRACEZ_INFO(ME, "Set LastChangeTime parameter[%lu]", dsl_uptime() - t0);
		variant_setUInt32(value, dsl_uptime() - t0);
	}
	else {
		//SAH_TRACEZ_INFO(ME, "Set LastChangeTime parameter(0)");
		variant_setUInt32(value, 0);
	}
	return true;
}

static void dsl_check(intf_blob_t *blob) {
	if (blob->enabled == blob->dstenabled) {
		SAH_TRACEZ_INFO(ME, "DSL enable is not changed");
		return;
	}

	object_t *object = plugin_intf2object(blob->intf);

	request_t *req = request_create_setObject(map_dstpath(blob->map), request_common_path_key_notation);
	variant_t var;
	variant_initialize(&var, variant_type_unknown);
	variant_setBool(&var, blob->enabled);
	SAH_TRACEZ_INFO(ME, "request_addParameter(path=%s param=%s value=%s)", map_dstpath(blob->map), "Enable", blob->enabled? "true": "false");
	request_addParameter(req, "Enable", &var);
	if (!pcb_sendRequest(plugin_getPcb(), get_dst(object, &blob->dst), req)) {
		SAH_TRACE_ERROR("dsl - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
	}
	request_destroy(req);
	variant_cleanup(&var);
	blob->dstenabled = blob->enabled;
}

static void dsl_enableHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	if (!strcmp(flag, "enabled")) {
		SAH_TRACEZ_INFO(ME, "flag(enabled), value[%s]", value? "Enable": "Disabled");
		blob->enabled = value;
		dsl_check(blob);
	}
}

static void dsl_updateIntf(intf_t *intf, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	map_commit(blob->map);
	map_commit(blob->channel_map);
}

static void dsl_upHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)flag;
	(void)userdata;
	(void)value;
	object_t *object = plugin_intf2object(intf);
	object_parameterSetUInt32Value(object, "Line_LastChangeTime", dsl_uptime());
	object_commit(object);
}

static void dslline_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata;	(void)flag;
	object_t *object = plugin_intf2object(intf);
	if (value) {
		intf_blob_t *blob = calloc(1, sizeof(intf_blob_t));
		if (!blob) {
			SAH_TRACE_ERROR("dsl - calloc failed");
			return;
		}
		intf_addBlob(intf, dsl_blobkey, blob);
		blob->intf = intf;
		object_t *object = plugin_intf2object(intf);
		blob->map = map_create(object, MAP_OWNER, get_dst(object, &blob->dst), "%s.Line.%s", get_dslroot(object), intf_name(intf));
		map_parameter_create(blob->map, object_getParameter(object, "Name"), NULL, MAP_WRITEONLY | MAP_KEY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_LastChange"), "LastChange", MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_UpstreamMaxRate"), "UpstreamMaxBitRate", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_DownstreamMaxRate"), "DownstreamMaxBitRate", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_UpstreamNoiseMargin"), "UpstreamNoiseMargin", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_DownstreamNoiseMargin"), "DownstreamNoiseMargin", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_UpstreamPower"), "UpstreamPower", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_DownstreamPower"), "DownstreamPower", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_StandardsSupported"), "StandardsSupported", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_StandardUsed"), "StandardUsed", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_UpstreamAttenuation"), "UpstreamSignalAttenuation", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_DownstreamAttenuation"), "DownstreamSignalAttenuation", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_UpstreamLineAttenuation"), "UpstreamLineAttenuation", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_DownstreamLineAttenuation"), "DownstreamLineAttenuation", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_FirmwareVersion"), "FirmwareVersion", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_AllowedProfiles"), "AllowedProfiles", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_CurrentProfile"), "CurrentProfile", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_ModulationType"), "ModulationType", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_XTURVendor"), "XTURVendor", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_XTUCVendor"), "XTUCVendor", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_UPBOKLE"), "UPBOKLE", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_TRELLISds"), "TRELLISds", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_TRELLISus"), "TRELLISus", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_ATN70"), "ATN70", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_CO_Manufacturer"), "CO_Manufacturer", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_FsmVectoring"), "FsmVectoring", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_VCE_MAC"), "VCE_MAC", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_LATNpbds"), "LATNpbds", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_LATNpbus"), "LATNpbus", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_SATNpbds"), "SATNpbds", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_SATNpbus"), "SATNpbus", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_SNRMpbds"), "SNRMpbds", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Line_SNRMpbus"), "SNRMpbus", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LineStatus"), "LinkStatus", MAP_READONLY, NULL, NULL);
		map_function_create(blob->map, object_getFunction(object, "getDSLLineStats"), NULL, NULL, NULL, NULL, NULL);
		map_function_create(blob->map, object_getFunction(object, "getDSLStats"), NULL, NULL, NULL, NULL, NULL);
		map_function_create(blob->map, object_getFunction(object, "getXDSLNoiseMeasure"), NULL, NULL, NULL, NULL, NULL);

		blob->channel_map = map_create(object, MAP_OWNER, get_dst(object, &blob->dst), "%s.Channel.%s", get_dslroot(object), intf_name(intf));
		map_parameter_create(blob->channel_map, object_getParameter(object, "Name"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->channel_map, object_getParameter(object, "Enable"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->channel_map, object_getParameter(object, "Line_UpstreamCurrRate"), "UpstreamBitRate", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->channel_map, object_getParameter(object, "Line_DownstreamCurrRate"), "DownstreamBitRate", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->channel_map, object_getParameter(object, "Line_DataPath"), "LPATH", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->channel_map, object_getParameter(object, "Line_InterleaveDepth"), "INTLVDEPTH", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->channel_map, object_getParameter(object, "Line_ActualInterleavingDelay"), "ActualInterleavingDelay", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->channel_map, object_getParameter(object, "Line_ACTINP"), "ACTINP", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->channel_map, object_getParameter(object, "Line_INPREPORT"), "INPREPORT", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->channel_map, object_getParameter(object, "ChannelEncapsulationType"), "LinkEncapsulationUsed", MAP_READONLY, NULL, NULL);

		map_function_create(blob->channel_map, object_getFunction(object, "getDSLChannelStats"), NULL, NULL, NULL, NULL, NULL);

		plugin_setParameterReadHandler(object_getParameter(object, "Line_LastChange"), dsl_readLastChange);
		plugin_setParameterWriteHandler(object_getParameter(object, "LineStatus"), dsl_writeLinkStatus);
		function_setHandler(object_getFunction(object, "getDSLLineStats"), getDSLLineStats);
		function_setHandler(object_getFunction(object, "getDSLStats"), getDSLStats);
		function_setHandler(object_getFunction(object, "getXDSLNoiseMeasure"), getXDSLNoiseMeasure);
		function_setHandler(object_getFunction(object, "getDSLChannelStats"), getDSLChannelStats);

		map_commit(blob->map);
		SAH_TRACEZ_NOTICE(ME, "Create [%s.Line.%s]",  get_dslroot(object), intf_name(intf));
		map_commit(blob->channel_map);
		SAH_TRACEZ_NOTICE(ME, "Create [%s.Channel.%s]",  get_dslroot(object), intf_name(intf));

		intf_addFlagListener(intf, "enabled", dsl_enableHandler, blob, true);
		intf_addMgmtListener(intf, NULL, dsl_updateIntf, NULL, blob, false);
		intf_addFlagListener(intf, "up", dsl_upHandler, NULL, true);
	}
	else {
		intf_blob_t *blob = intf_getBlob(intf, dsl_blobkey);
		if (!blob) {
			SAH_TRACEZ_WARNING(ME, "No blob found");
			return;
		}

		intf_delFlagListener(intf, "enabled", dsl_enableHandler, blob, false);
		intf_delMgmtListener(intf, NULL, dsl_updateIntf, NULL, blob, false);

		intf_delFlagListener(intf, "up", dsl_upHandler, NULL, true);
		plugin_setParameterReadHandler(object_getParameter(object, "Line_LastChange"), NULL);
		plugin_setParameterWriteHandler(object_getParameter(object, "LineStatus"), NULL);
		function_setHandler(object_getFunction(object, "getDSLLineStats"), NULL );
		function_setHandler(object_getFunction(object, "getDSLStats"), NULL );
		function_setHandler(object_getFunction(object, "getXDSLNoiseMeasure"), NULL);
		function_setHandler(object_getFunction(object, "getDSLChannelStats"), NULL);

		if (blob->map) {
			map_destroy(blob->map);
			blob->map = NULL;
		}
		if (blob->channel_map) {
			map_destroy(blob->channel_map);
			blob->channel_map = NULL;
		}

		intf_delBlob(intf, dsl_blobkey);

		free(blob);
	}
}

static bool dslline_start(argument_value_list_t *args) {
	const char *env = getenv(ME);
	if (env) {
		sahTraceAddZone(atol(env), ME);
	}

	pcb_t *pcb = mtk_getPcb();
	t_object = datamodel_root(pcb_datamodel(pcb));

	SAH_TRACEZ_INFO(ME, "Initializing dsl module");

	set_dslroot_by_args(args);

	intf_addFlagListener(NULL, "dslline", dslline_rootHandler, NULL, true);

	bonding_start(t_object);
	dsl_start(t_object);
	return true;
}

static void dslline_stop() {
	SAH_TRACEZ_INFO(ME, "Cleaning up dsl module");
	intf_delFlagListener(NULL, "dslline", dslline_rootHandler, NULL, true);

	bonding_stop();
	dsl_stop();

	/** TODO
	  if (dsldst != plugin_getPeer()) {
	  peer_destroy(dsldst);
	  }
	 **/
}

static mtk_module_info_t dsl_info = {
	.name  = ME,
	.start = dslline_start,
	.stop  = dslline_stop
};

__attribute__((constructor)) static void dsl_init() {
	mtk_module_register(&dsl_info);
}

