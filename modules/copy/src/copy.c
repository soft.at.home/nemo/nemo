/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "copy"

#include <nemo/core.h>

static void copy_objcopy(object_t *dst, object_t *src) {
	object_t *srcobj, *dstobj;
	object_for_each_child(srcobj, src) {
		dstobj = object_getObject(dst, object_name(srcobj, path_attr_key_notation), path_attr_key_notation, NULL);
		if (!dstobj) {
			SAH_TRACE_ERROR("copy - object_getObject(parent=%s, object=%s) failed"
					, object_name(dst, path_attr_key_notation), object_name(srcobj, path_attr_key_notation));
			continue;
		}
		copy_objcopy(dstobj, srcobj);
	}

	if (object_hasAcl(src)) {
		const llist_t *srcobj_acl = object_getACL(src);
		object_setACL(dst, srcobj_acl);
	}

	parameter_t *srcpar, *dstpar;
	object_for_each_parameter(srcpar, src) {
		dstpar = object_getParameter(dst, parameter_name(srcpar));
		if (!dstpar) {
			SAH_TRACE_ERROR("copy - object_getParameter(object=%s, name=%s) failed"
					, object_name(dst, path_attr_key_notation), parameter_name(srcpar));
			continue;
		}

		if (!parameter_isReadOnly(dstpar)) {
			parameter_setValue(dstpar, parameter_getValue(srcpar));
		}

		if (parameter_hasAcl(srcpar)) {
			const llist_t *srcpar_acl = parameter_getACL(srcpar);
			parameter_setACL(dstpar, srcpar_acl);
		}
	}

	function_t *srcfunc = NULL, *dstfunc = NULL;
	object_for_each_function(srcfunc, src) {
		const llist_t *func_acl = function_getACL(srcfunc);
		dstfunc = object_getFunction(dst, function_name(srcfunc));
		function_setACL(dstfunc, func_acl);
	}

	if (!object_isReadOnly(dst)) {
		object_for_each_instance(srcobj, src) {
			dstobj = object_getObject(dst, object_name(srcobj, path_attr_key_notation), path_attr_key_notation, NULL);
			if (!dstobj) {
				dstobj = object_createInstance(dst, 0, object_name(srcobj, path_attr_key_notation));
			}
			if (!dstobj) {
				SAH_TRACE_ERROR("copy - object_getObject/createInstance(parent=%s, key=%s) failed"
						, object_name(dst, path_attr_key_notation), object_name(srcobj, path_attr_key_notation));
				continue;
			}
			copy_objcopy(dstobj, srcobj);
		}
	}
}

static function_exec_state_t copy_funcHandler(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	uint32_t attr = request_attributes(fcall_request(fcall));
	object_t *srcobj = fcall_object(fcall);
	intf_t *src = plugin_object2intf(srcobj);
	char *dstname = NULL;
	if (!arg_takeChar(&dstname, args, attr, "name"))
		goto leave;

	intf_t *dst = intf_find(dstname);
	if (!dst)
		dst = intf_create(dstname);
	object_t *dstobj = plugin_intf2object(dst);

	flagset_union(intf_flagset(dst), intf_flagset(src));

	copy_objcopy(dstobj, srcobj);

	object_commit(dstobj);

	ok = true;
leave:
	free(dstname);
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

static void copy_intfCreated(intf_t *intf, void *userdata) {
	(void)userdata;
	function_setHandler(object_getFunction(plugin_intf2object(intf), "copy"), copy_funcHandler);
}

static void copy_intfDestroyed(intf_t *intf, void *userdata) {
	(void)userdata;
	function_setHandler(object_getFunction(plugin_intf2object(intf), "copy"), NULL);
}

static bool copy_start(argument_value_list_t *args __attribute__((unused))) {
	SAH_TRACEZ_INFO(ME, "Initializing copy module");
	intf_addMgmtListener(NULL, copy_intfCreated, NULL, copy_intfDestroyed, NULL, true);
	return true;
}

static void copy_stop() {
	SAH_TRACEZ_INFO(ME, "Cleaning up copy module");
	intf_delMgmtListener(NULL, copy_intfCreated, NULL, copy_intfDestroyed, NULL, true);
}

static mtk_module_info_t copy_info = {
	.name  = ME,
	.start = copy_start,
	.stop  = copy_stop
};

__attribute__((constructor)) static void copy_init() {
	mtk_module_register(&copy_info);
}

