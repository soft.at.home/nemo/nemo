/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "xtm"

#include <nemo/core.h>

#include "dsl_common.h"

/* user specific data structure, is reference as _intf_blob_t (defined in sah/lib/nemo/include/nemo/core/intf.h) */
struct _intf_blob {
	map_t *map;
	map_t *qosmap; /* only applicable for ADSL */
	bool enabled;
	bool dstenabled;
	int32_t uptime;
	int32_t boundtime;
	nemo_query_t *physupquery;
	nemo_query_t *bondingquery;
	nemo_query_t *linkupquery;
	nemo_query_t *lowerintfquery;
	bool physup;
	bool isBondedCfg;
	intf_t *intf;
	peer_info_t *dst;
};

static peer_info_t *dst = NULL;
static int atm_dummy;
static void *atm_blobkey = &atm_dummy;
static int ptm_dummy;
static void *ptm_blobkey = &ptm_dummy;
static object_t *t_object;

static void atm_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata);
static void ptm_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata);
static void xtm_check(intf_blob_t *blob);
static void xtm_updateIntf(intf_t *intf, void *userdata);
static void xtm_physupHandler(const variant_t *result, void *userdata);
static void xtm_llintfHandler(const variant_t *result, void *userdata);
static void xtm_enableHandler(intf_t *intf, const char *flag, bool value, void *userdata);
static void xtm_setLowerLayer(const char *path, intf_blob_t *blob);
static void xtm_bondingHandler_refer(const variant_t *result, void *userdata);

static function_exec_state_t oamPing(function_call_t *fcall, argument_value_list_t *args,
		variant_t *retval) {
	(void)args;
	variant_setType(retval, variant_type_bool);
	object_t *object = fcall_object(fcall);
	intf_t *intf = plugin_object2intf(object);
	intf_blob_t *blob = intf_getBlob(intf, atm_blobkey);
	if (!blob) {
		SAH_TRACEZ_WARNING(ME, "No blob found");
		return function_exec_error;
	}

	return map_call_create(blob->map? map_function_findByName(blob->map, "oamPing"): NULL, fcall, args, retval, NULL)
		? function_exec_executing : function_exec_error;
}

static void xtm_writeStatus(parameter_t *parameter, void *key);
static void atm_writeStatus(parameter_t *parameter, const variant_t *oldvalue);
static void ptm_writeStatus(parameter_t *parameter, const variant_t *oldvalue);

/* qosmap is only defined in ATM mode, true if ATM, false is PTM */
static bool isATM(intf_blob_t *blob) {
	return (blob->qosmap)? true: false;
}

static long xtm_uptime(void) {
	char buf[64], *ptr = NULL;
	FILE *f = fopen("/proc/uptime", "r"); // Remark using sysinfo() would be easier but it's not available in uclibc
	if (f) {
		ptr = fgets(buf, 64, f);
		fclose(f);
	}
	return ptr?strtol(ptr, NULL, 0):0;
}

static void xtm_upHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)flag; (void)userdata; (void)value;
	object_parameterSetUInt32Value(plugin_intf2object(intf), "LastChangeTime", xtm_uptime());
	object_commit(plugin_intf2object(intf));
}

static bool xtm_readLastChange(parameter_t *parameter, variant_t *value) {
	uint32_t t0 = object_parameterUInt32Value(parameter_owner(parameter), "LastChangeTime");
	if (t0) {
		variant_setUInt32(value, xtm_uptime() - t0);
	}
	else {
		variant_setUInt32(value, 0);
	}
	return true;
}

static void xtm_writeStatus(parameter_t *parameter, void *key) {
	const variant_t *var = parameter_getValue(parameter);
	const char *value = variant_da_char(var);
	intf_t *intf = plugin_object2intf(parameter_owner(parameter));
	SAH_TRACEZ_INFO(ME, "[%s]XTM Status[%s]", intf_name(intf), value);
	if (!value) {
		value = "";
	}

	if (!strcmp(value, "Up")) {
		intf_blob_t *blob = intf_getBlob(intf, key);
		long t = xtm_uptime();
		blob->boundtime = t;
		if (!flagset_check(intf_flagset(intf), "up")) {
			blob->uptime = t;
		}
		flagset_set(intf_flagset(intf), "up");
	}
	else if (flagset_check(intf_flagset(intf), "up")) {
		flagset_clear(intf_flagset(intf), "up");
	}
}

static void atm_writeStatus(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	xtm_writeStatus(parameter, atm_blobkey);
}

static void ptm_writeStatus(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	xtm_writeStatus(parameter, ptm_blobkey);
}

static void xtm_check(intf_blob_t *blob) {
	bool dstenabled = blob->enabled && blob->physup;
	SAH_TRACEZ_INFO(ME, "[%s] dstenabled[%d], blob->enabled[%d], physup[%d]", intf_name(blob->intf), dstenabled, blob->enabled, blob->physup);
	if (dstenabled == blob->dstenabled) {
		SAH_TRACEZ_INFO(ME, "[%s] Enable intf is not changed", intf_name(blob->intf));
		return;
	}

	object_t *object = plugin_intf2object(blob->intf);

	request_t *req = request_create_setObject(map_dstpath(blob->map), request_common_path_key_notation);
	variant_t var;
	variant_initialize(&var, variant_type_unknown);
	variant_setBool(&var, dstenabled);
	SAH_TRACEZ_INFO(ME, "request_addParameter(path=%s param=%s value=%s)", map_dstpath(blob->map), "Enable", dstenabled?"true":"false");
	request_addParameter(req, "Enable", &var);
	if (!pcb_sendRequest(plugin_getPcb(), get_dst(object, &blob->dst), req)) {
		SAH_TRACE_ERROR("dhcp - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
	}
	request_destroy(req);
	variant_cleanup(&var);
	blob->dstenabled = dstenabled;
}

static void xtm_enableHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	if (!strcmp(flag, "enabled")) {
		SAH_TRACEZ_INFO(ME, "flag(enabled) is set[%s]", value? "Enabled": "Disabled");
		blob->enabled = value;
		xtm_check(blob);
	}
}

static void xtm_updateIntf(intf_t *intf, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	map_commit(blob->map);
	if (blob->qosmap) {
		map_commit(blob->qosmap);
	}
}

static void xtm_linkupHandler(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	bool up = variant_bool(result);
	SAH_TRACEZ_WARNING(ME, "[%s] Physical bonded interface [%s]", intf_name(blob->intf), up? "Up": "Down");
	blob->physup = up;
	xtm_check(blob);
}

static void xtm_bondingHandler_up(void *userdata) {
        xtm_bondingHandler_refer(variant_true, userdata); 
}

static void xtm_bondingHandler_down(void *userdata) {
        xtm_bondingHandler_refer(variant_false, userdata); 
}

static void xtm_bondingHandler(const variant_t *result, void *userdata){
        intf_blob_t *blob = (intf_blob_t *)userdata;
        bool up = variant_bool(result);
        if (up) {
            pcb_timer_defer( xtm_bondingHandler_up, blob, 1000);
        }
        else {
            pcb_timer_defer( xtm_bondingHandler_down, blob, 1000);
        }
}

static void xtm_bondingHandler_refer(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	object_t *object = plugin_intf2object(blob->intf);
	bool up = variant_bool(result);
	char path[64] = {};
	if (up) {
		if (blob->linkupquery) {
			nemo_closeQuery(blob->linkupquery);
			blob->linkupquery = NULL;
		}
		blob->physup = up;
		SAH_TRACEZ_WARNING(ME, "DSL bonding is up");
		snprintf(path, 63, "%s.BondingGroup.dsl0", get_dslroot(object));
		xtm_setLowerLayer(path, blob);
	}
	else {
		bool isSingleLine = nemo_hasFlag(intf_name(blob->intf), "line0-up", "dslbonding", "down");
		if (isSingleLine) {
			SAH_TRACEZ_WARNING(ME, "DSL Single line0 is up");
			snprintf(path, 63, "%s.Channel.line0", get_dslroot(object));
			xtm_setLowerLayer(path, blob);
			if (!blob->linkupquery) {
				blob->linkupquery = nemo_openQuery_isUp(intf_name(blob->intf), ME, "dslline", "down exclusive", xtm_linkupHandler, blob);
			}
		}
		else {
			isSingleLine = nemo_hasFlag(intf_name(blob->intf), "line1-up", "dslbonding", "down");
			if (isSingleLine) {
				SAH_TRACEZ_WARNING(ME, "DSL Single line1 is up");
				snprintf(path, 63, "%s.Channel.line1", get_dslroot(object));
				xtm_setLowerLayer(path, blob);
				if (!blob->linkupquery) {
					blob->linkupquery = nemo_openQuery_isUp(intf_name(blob->intf), ME, "dslline", "down exclusive", xtm_linkupHandler, blob);
				}
			}
			else {
				SAH_TRACEZ_WARNING(ME, "No bonding is up, no DSL Single line is up");
				blob->physup = up;
				if (blob->linkupquery) {
					nemo_closeQuery(blob->linkupquery);
					blob->linkupquery = NULL;
				}
			}
		}
	}
	xtm_check(blob);
}

static void xtm_physupHandler(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	bool up = variant_bool(result);
	if (up) {
		SAH_TRACEZ_WARNING(ME, "[%s] Physical interface up", intf_name(blob->intf));
		blob->isBondedCfg = nemo_hasFlag(intf_name(blob->intf), "dslbonding", "dsl", "down");
		if (blob->isBondedCfg) {
			/* start query to see when dslbonding-up flag is set.
			 * For ATM/ADSL we sometimes see flags changing after the up flag is set */
			blob->bondingquery = nemo_openQuery_hasFlag(intf_name(blob->intf), ME, "dslbonding-up", "dslbonding", "down", xtm_bondingHandler, blob);
		}
		else {
			SAH_TRACEZ_WARNING(ME, "[%s] DSL single line is up", intf_name(blob->intf));
			blob->physup = up;

			if (blob->bondingquery) {
				nemo_closeQuery(blob->bondingquery);
				blob->bondingquery = NULL;
			}
			xtm_check(blob);
		}
	}
	else {
		blob->physup = up;
		if (blob->isBondedCfg) {
			if (!isATM(blob)) {
				SAH_TRACEZ_NOTICE(ME, "Bring PTM down if Physical interface goes down");
				xtm_check(blob);
			}
		}
		else {
			SAH_TRACEZ_NOTICE(ME, "Do not bring ATM/PTM down if Physical interface goes down");
		}
		if (blob->bondingquery) {
			nemo_closeQuery(blob->bondingquery);
			blob->bondingquery = NULL;
		}
	}
}

static void xtm_setLowerLayer(const char *path, intf_blob_t *blob) {
	object_t *object = plugin_intf2object(blob->intf);
	request_t *req = request_create_setObject(map_dstpath(blob->map), request_common_path_key_notation);
	if (req) {
		request_addCharParameter(req, "LowerLayer", path);
		if (!pcb_sendRequest(plugin_getPcb(), get_dst(object, &blob->dst), req)) {
			SAH_TRACEZ_ERROR(ME, "Failed to send request:[%s]", error_string(pcb_error));
		}

		SAH_TRACEZ_NOTICE(ME, "Set LowerLayer parameter[%s][%s]", map_dstpath(blob->map), path);
		request_destroy(req);
	}
}

static void xtm_llintfHandler(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	char path[64];

	object_t *object = plugin_intf2object(blob->intf);
	variant_list_t *ar = variant_da_list(result);
	variant_list_iterator_t *v_it = NULL;
	variant_list_for_each(v_it, ar) {
		memset(path, 0, 64);
		variant_t *v = variant_list_iterator_data(v_it);
		const char *iface = variant_da_char(v);
		intf_t *llintf = intf_find(iface);
		if (!llintf) {
			SAH_TRACEZ_WARNING(ME, "No LLIntf[NeMo.Intf.%s] found", iface);
			continue;
		}
		flagset_t *flags = intf_flagset(llintf);

		if (flagset_check(flags, "dslbonding")) {
			SAH_TRACEZ_NOTICE(ME, "XTM LLIntf is a bonding DSL interface");
			snprintf(path, 64, "%s.BondingGroup.%s.", get_dslroot(object), iface);
			xtm_setLowerLayer(path, blob);
		}
		else if (flagset_check(flags, "dslline")) {
			SAH_TRACEZ_NOTICE(ME, "XTM LLIntf is a single line DSL interface");
			snprintf(path, 64, "%s.Channel.%s.", get_dslroot(object), iface);
			xtm_setLowerLayer(path, blob);
		}
		else if (flagset_check(flags, "dsl")) {
			SAH_TRACEZ_WARNING(ME, "XTM LLIntf is a DSL interface (no dslline or dslbonding flag)");
		}
		else {
			SAH_TRACEZ_WARNING(ME, "XTM LLIntf is not a DSL interface");
		}
	}
}

static void atm_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata;	(void)flag;
	object_t *object = plugin_intf2object(intf);
	if (value) {
		intf_blob_t *blob = calloc(1, sizeof(intf_blob_t));
		if (!blob) {
			SAH_TRACE_ERROR("atm - calloc failed");
			return;
		}
		intf_addBlob(intf, atm_blobkey, blob);
		blob->intf = intf;

		blob->qosmap = map_create(object, MAP_WRITEONLY, get_dst(object, &blob->dst), "%s.ATM.Link.%s.QoS", get_xtmroot(object), intf_name(intf));
		map_parameter_create(blob->qosmap, object_getParameter(object, "QoSClass"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->qosmap, object_getParameter(object, "PeakCellRate"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->qosmap, object_getParameter(object, "MaximumBurstSize"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->qosmap, object_getParameter(object, "SustainableCellRate"), NULL, MAP_WRITEONLY, NULL, NULL);

		blob->map = map_create(object, MAP_OWNER, get_dst(object, &blob->dst), "%s.ATM.Link.%s", get_xtmroot(object), intf_name(intf));
		map_parameter_create(blob->map, object_getParameter(object, "Name"), NULL, MAP_WRITEONLY | MAP_KEY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LinkStatus"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LinkType"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "DestinationAddress"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Encapsulation"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Enable"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_function_create(blob->map, object_getFunction(object, "oamPing"), NULL, NULL, NULL, NULL, NULL);

		plugin_setParameterWriteHandler(object_getParameter(object, "LinkStatus"), atm_writeStatus);
		function_setHandler(object_getFunction(object, "oamPing"), oamPing);

		intf_addFlagListener(intf, "enabled", xtm_enableHandler, blob, true);
		intf_addMgmtListener(intf, NULL, xtm_updateIntf, NULL, blob, false);

		map_commit(blob->map);
		map_commit(blob->qosmap);

		blob->physupquery = nemo_openQuery_isUp(intf_name(intf), ME, NULL, "down exclusive", xtm_physupHandler, blob);

		intf_addFlagListener(intf, "up", xtm_upHandler, NULL, true);
		plugin_setParameterReadHandler(object_getParameter(object, "LastChange"), xtm_readLastChange);
		blob->lowerintfquery = nemo_openQuery_getIntfs(intf_name(intf), ME, "dsl", "one level down", xtm_llintfHandler, blob);
	}
	else {
		intf_blob_t *blob = intf_getBlob(intf, atm_blobkey);
		if (!blob) {
			return;
		}

		plugin_setParameterWriteHandler(object_getParameter(object, "LinkStatus"), NULL);

		intf_delFlagListener(intf, "enabled", xtm_enableHandler, blob, false);
		intf_delMgmtListener(intf, NULL, xtm_updateIntf, NULL, blob, false);

		intf_delFlagListener(intf, "up", xtm_upHandler, NULL, true);
		plugin_setParameterReadHandler(object_getParameter(object, "LastChange"), NULL);
		function_setHandler(object_getFunction(object, "oamPing"), NULL );

		if (blob->physupquery) {
			nemo_closeQuery(blob->physupquery);
			blob->physupquery = NULL;
		}

		if (blob->bondingquery) {
			nemo_closeQuery(blob->bondingquery);
			blob->bondingquery = NULL;
		}

		if (blob->linkupquery) {
			nemo_closeQuery(blob->linkupquery);
			blob->linkupquery = NULL;
		}

		if (blob->lowerintfquery) {
			nemo_closeQuery(blob->lowerintfquery);
			blob->lowerintfquery = NULL;
		}

		if (blob->map) {
			map_destroy(blob->map);
			blob->map = NULL;
		}

		if (blob->qosmap) {
			map_destroy(blob->qosmap);
			blob->qosmap = NULL;
		}

		intf_delBlob(intf, atm_blobkey);

		free(blob);
	}
}

static void ptm_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata;	(void)flag;
	object_t *object = plugin_intf2object(intf);
	if (value) {
		intf_blob_t *blob = calloc(1, sizeof(intf_blob_t));
		if (!blob) {
			SAH_TRACE_ERROR("ptm - calloc failed");
			return;
		}
		intf_addBlob(intf, ptm_blobkey, blob);
		blob->intf = intf;

		blob->map = map_create(object, MAP_OWNER, get_dst(object, &blob->dst), "%s.PTM.Link.%s", get_xtmroot(object), intf_name(intf));
		map_parameter_create(blob->map, object_getParameter(object, "Name"), NULL, MAP_WRITEONLY | MAP_KEY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Status"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LinkStatus"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LLAddress"), "MACAddress", MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Enable"), NULL, MAP_WRITEONLY, NULL, NULL);
		plugin_setParameterWriteHandler(object_getParameter(object, "LinkStatus"), ptm_writeStatus);

		intf_addFlagListener(intf, "enabled", xtm_enableHandler, blob, true);
		intf_addMgmtListener(intf, NULL, xtm_updateIntf, NULL, blob, false);
		map_commit(blob->map);

		blob->physupquery = nemo_openQuery_isUp(intf_name(intf), ME, NULL, "down exclusive", xtm_physupHandler, blob);

		intf_addFlagListener(intf, "up", xtm_upHandler, NULL, true);
		plugin_setParameterReadHandler(object_getParameter(object, "LastChange"), xtm_readLastChange);
		blob->lowerintfquery = nemo_openQuery_getIntfs(intf_name(intf), ME, "dsl", "one level down", xtm_llintfHandler, blob);
	}
	else {
		intf_blob_t *blob = intf_getBlob(intf, ptm_blobkey);
		if (!blob) {
			return;
		}

		plugin_setParameterWriteHandler(object_getParameter(object, "LinkStatus"), NULL);

		intf_delFlagListener(intf, "enabled", xtm_enableHandler, blob, false);
		intf_delMgmtListener(intf, NULL, xtm_updateIntf, NULL, blob, false);

		intf_delFlagListener(intf, "up", xtm_upHandler, NULL, true);
		plugin_setParameterReadHandler(object_getParameter(object, "LastChange"), NULL);

		if (blob->physupquery) {
			nemo_closeQuery(blob->physupquery);
			blob->physupquery = NULL;
		}

		if (blob->bondingquery) {
			nemo_closeQuery(blob->bondingquery);
			blob->bondingquery = NULL;
		}

		if (blob->linkupquery) {
			nemo_closeQuery(blob->linkupquery);
			blob->linkupquery = NULL;
		}

		if (blob->lowerintfquery) {
			nemo_closeQuery(blob->lowerintfquery);
			blob->lowerintfquery = NULL;
		}

		if (blob->map) {
			map_destroy(blob->map);
			blob->map = NULL;
		}

		intf_delBlob(intf, ptm_blobkey);

		free(blob);
	}
}

static bool xtm_start(argument_value_list_t *args) {
	//sahTraceAddZone(400, ME);

	pcb_t *pcb = mtk_getPcb();
	t_object = datamodel_root(pcb_datamodel(pcb));

	SAH_TRACEZ_INFO(ME, "Initializing xtm module");

	start_dsl_common();
	set_xtmroot_by_args(args);
	set_dslroot_by_args(args);

	intf_addFlagListener(NULL, "atm", atm_rootHandler, NULL, true);
	intf_addFlagListener(NULL, "ptm", ptm_rootHandler, NULL, true);
	return true;
}

static void xtm_stop() {
	SAH_TRACEZ_INFO(ME, "Cleaning up xtm module");
	intf_delFlagListener(NULL, "atm", atm_rootHandler, NULL, true);
	intf_delFlagListener(NULL, "ptm", ptm_rootHandler, NULL, true);
	if (dst != plugin_getPeer()) {
		peer_destroy(dst);
	}

	stop_dsl_common();
}

static mtk_module_info_t xtm_info = {
	.name  = ME,
	.start = xtm_start,
	.stop  = xtm_stop
};

__attribute__((constructor)) static void xtm_init() {
	mtk_module_register(&xtm_info);
}
