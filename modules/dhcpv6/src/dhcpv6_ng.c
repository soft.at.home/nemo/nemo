/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "dhcpv6"

#include <nemo/core.h>

struct _intf_blob {
	map_t *map;
	bool enabled;
	bool dstenabled;
	char *lowerintf;
	int32_t uptime;
	int32_t boundtime;
	nemo_query_t *lowerintfquery;
	nemo_query_t *physupquery;
	pcb_timer_t *physdowntimer;
	bool physup;
	intf_t *intf;
};

static long dhcpv6_uptime();

static peer_info_t *dhcpv6_dst = NULL;

static int dhcpv6_dummy;
static void *dhcpv6_blobkey = &dhcpv6_dummy;

static void dhcpv6_check(intf_blob_t *blob) {
	pcb_timer_state_t timerstate = pcb_timer_getState(blob->physdowntimer);
	bool timer = timerstate == pcb_timer_running || timerstate == pcb_timer_started;
	bool dstenabled = blob->enabled && blob->lowerintf && (blob->physup || (blob->dstenabled && timer));
	if (dstenabled == blob->dstenabled)
		return;

	SAH_TRACEZ_INFO(ME, "request_create_setObject(path=%s)", map_dstpath(blob->map));
	request_t *req = request_create_setObject(map_dstpath(blob->map), request_common_path_key_notation);
	variant_t var;
	variant_initialize(&var, variant_type_unknown);
	variant_setBool(&var, dstenabled);
	SAH_TRACEZ_INFO(ME, "request_addParameter(path=%s param=%s value=%s)", map_dstpath(blob->map), "Enable", dstenabled?"true":"false");

	request_addParameter(req, "Enable", &var);
	SAH_TRACEZ_INFO(ME, "pcb_sendRequest(path=%s)", map_dstpath(blob->map));
	if (!pcb_sendRequest(plugin_getPcb(), dhcpv6_dst, req)) {
		SAH_TRACE_ERROR("dhcpv6 - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
	}

	request_destroy(req);
	variant_cleanup(&var);
	blob->dstenabled = dstenabled;
}

static void dhcpv6_llintfHandler(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	free(blob->lowerintf);
	blob->lowerintf = result?variant_char(result):NULL;
	if (blob->lowerintf && !*blob->lowerintf) {
		free(blob->lowerintf);
		blob->lowerintf = NULL;
	}

	if (blob->lowerintf) {
		SAH_TRACEZ_INFO(ME, "request_create_setObject(path=%s)", map_dstpath(blob->map));
		request_t *req = request_create_setObject(map_dstpath(blob->map), request_common_path_key_notation);
		variant_t var;
		variant_initialize(&var, variant_type_unknown);
		variant_setChar(&var, blob->lowerintf);

		request_addParameter(req, "IPIntf", &var);
		SAH_TRACEZ_INFO(ME, "pcb_sendRequest(path=%s)", map_dstpath(blob->map));
		if (!pcb_sendRequest(plugin_getPcb(), dhcpv6_dst, req)) {
			SAH_TRACE_ERROR("dhcp - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
		}
		request_destroy(req);
	}
	dhcpv6_check(blob);
}

static void dhcpv6_writeDHCPStatus(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	intf_t *intf = plugin_object2intf(parameter_owner(parameter));
	intf_blob_t *blob = intf_getBlob(intf, dhcpv6_blobkey);
	const char *value = string_buffer(variant_da_string(parameter_getValue(parameter)));
	if (!value)
		value = "";
	if (!strcmp(value, "Bound")) {
		long t = dhcpv6_uptime();
		blob->boundtime = t;
		if (!flagset_check(intf_flagset(intf), "up"))
			blob->uptime = t;
	}
	if (!strcmp(value, "Bound") || !strcmp(value, "Renew") || !strcmp(value, "Rebind")) {
		flagset_set(intf_flagset(intf), "up");
	}
	else if (flagset_check(intf_flagset(intf), "up")) {
		flagset_clear(intf_flagset(intf), "up");
	}
}

static long dhcpv6_uptime() {
	char buf[64], *ptr = NULL;
	FILE *f = fopen("/proc/uptime", "r"); // Remark using sysinfo() would be easier but it's not available in uclibc
	if (f) {
		ptr = fgets(buf, 64, f);
		fclose(f);
	}
	return ptr?strtol(ptr, NULL, 0):0;
}

static bool dhcpv6_readUptime(parameter_t *parameter, variant_t *value) {
	intf_t *intf = plugin_object2intf(parameter_owner(parameter));
	if (!flagset_check(intf_flagset(intf), "up")) {
		variant_setInt32(value, 0);
		return true;
	}
	intf_blob_t *blob = intf_getBlob(intf, dhcpv6_blobkey);
	int32_t now = dhcpv6_uptime();
	variant_setUInt32(value, (uint32_t)(now - blob->uptime));
	return true;
}

static void dhcpv6_enableHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	if (!strcmp(flag, "enabled")) {
		blob->enabled = value;
		dhcpv6_check(blob);
	}
}

static void dhcpv6_writeOption(object_t *object) {
	if (object_isTemplate(object)) {
		return;
	}
	map_t *map = (map_t *)object_getUserData(object);
	if (map && map_busy(map)) {
		return;
	}

	const char *alias = string_buffer(variant_da_string(object_parameterValue(object, "Alias")));
	if (!(alias && *alias)) {
		string_t s;
		string_initialize(&s, 64);
		string_appendFormat(&s, "cpe-%s", object_name(object, path_attr_key_notation));
		object_parameterSetStringValue(object, "Alias", &s);
		object_commit(object);
		string_cleanup(&s);
		return;
	}

	unsigned short tag = object_parameterUInt16Value(object, "Tag");
	int key = atoi(object_name(object, path_attr_key_notation));
	if (!tag && key > 0 && key < 0xffff) {
		object_parameterSetUInt16Value(object, "Tag", key);
		object_commit(object);
		return;
	}

	if (!map) {
		object_t *parent = object_parent(object);
		intf_blob_t *blob = (intf_blob_t *)object_getUserData(parent);
		int valueattr = !strcmp(object_name(parent, 0), "SentOption") ? MAP_WRITEONLY : MAP_READONLY;
		map = map_create(object, MAP_OWNER, dhcpv6_dst, "DHCPv6Client.Intf.%s.%s.%u", intf_name(blob->intf),
				valueattr == MAP_WRITEONLY ? "SentOption" : "ReceivedOption",
				object_parameterUInt8Value(object, "Tag"));
		if (map) {
			//map_parameter_create(map, object_getParameter(object, "Enable"), NULL, MAP_WRITEONLY, NULL, NULL);
			map_parameter_create(map, object_getParameter(object, "Tag"), NULL, MAP_WRITEONLY, NULL, NULL);
			map_parameter_create(map, object_getParameter(object, "Value"), NULL, valueattr, NULL, NULL);
			object_setUserData(object, map);
		}
		else {
			SAH_TRACEZ_NOTICE(ME, "Create map failed.");
		}
	}
	map_commit(map);
}

static bool dhcpv6_delOption(object_t *object, object_t *instance) {
	(void)object;
	map_t *map = (map_t *)object_getUserData(instance);
	if (map) {
		map_destroy(map);
		object_setUserData(instance, NULL);
	}
	return true;
}

static void dhcpv6_physupHandler(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	bool physup = variant_bool(result);
	int32_t timeout = object_parameterInt32Value(plugin_intf2object(blob->intf), "ResetOnPhysDownTimeout");
	if (blob->physup && timeout < 0)
		return;

	blob->physup = physup;
	if (!blob->physup && timeout > 0) {
		pcb_timer_start(blob->physdowntimer, timeout * 1000);
	} else {
		pcb_timer_stop(blob->physdowntimer);
		dhcpv6_check(blob);
	}
}

static void dhcpv6_physdownTimeout(pcb_timer_t *timer, void *userdata) {
	(void)timer;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	dhcpv6_check(blob);
}

static void dhcpv6_updateIntf(intf_t *intf, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	map_commit(blob->map);
}

static void dhcpv6_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata; (void)flag;
	object_t *object = plugin_intf2object(intf);

	if (value) {
		intf_blob_t *blob = calloc(1, sizeof(intf_blob_t));
		intf_addBlob(intf, dhcpv6_blobkey, blob);
		blob->intf = intf;

		SAH_TRACEZ_NOTICE(ME, "DHCPv6 roothandler[%s].", intf_name(intf));

		blob->map = map_create(object, MAP_OWNER, dhcpv6_dst, "DHCPv6Client.Intf.%s", intf_name(intf));
		map_parameter_create(blob->map, object_getParameter(object, "Name"), NULL, MAP_WRITEONLY | MAP_KEY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "DHCPStatus"), "DHCPv6Status", MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "DUID"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "DSCPMark"), "TrafficClass", MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "RequestAddresses"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "RequestPrefixes"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "RapidCommit"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "IAID"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "SuggestedT1"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "SuggestedT2"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "SupportedOptions"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "RequestedOptions"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Renew"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "CheckAuthentication"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "AuthenticationInfo"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "RetryOnFailedAuth"), NULL, MAP_WRITEONLY, NULL, NULL);
		plugin_setParameterWriteHandler(object_getParameter(object, "DHCPStatus"), dhcpv6_writeDHCPStatus);
		plugin_setParameterReadHandler(object_getParameter(object, "Uptime"), dhcpv6_readUptime);

		intf_addFlagListener(intf, "enabled", dhcpv6_enableHandler, blob, true);
		intf_addMgmtListener(intf, NULL, dhcpv6_updateIntf, NULL, blob, false);

		blob->lowerintfquery = nemo_openQuery_luckyIntf(intf_name(intf), ME, "netdev-up && ipv6-up", "one level down", dhcpv6_llintfHandler, blob);

		blob->physdowntimer = pcb_timer_create();
		pcb_timer_setHandler(blob->physdowntimer, dhcpv6_physdownTimeout);
		pcb_timer_setUserData(blob->physdowntimer, blob);
		blob->physupquery = nemo_openQuery_isUp(intf_name(intf), ME, NULL, "down exclusive", dhcpv6_physupHandler, blob);
		map_commit(blob->map);	
		/* Configure Sent Option mapping. */
		object_t *o = NULL;
		object_t *option = object_getObject(object, "SentOption", 0, NULL);
		object_setUserData(option, blob);
		object_setWriteHandler(option, dhcpv6_writeOption);
		object_for_each_instance(o, option) {
			object_setWriteHandler(o, dhcpv6_writeOption);
			dhcpv6_writeOption(o);
		}

		/* Configure received Option mapping. */
		option = object_getObject(object, "ReceivedOption", 0, NULL);
		object_setUserData(option, blob);
		object_setWriteHandler(option, dhcpv6_writeOption);
		object_setInstanceDelHandler(option, dhcpv6_delOption);
		object_for_each_instance(o, option) {
			object_setWriteHandler(o, dhcpv6_writeOption);
			dhcpv6_writeOption(o);
		}
	} else {
		intf_blob_t *blob = intf_getBlob(intf, dhcpv6_blobkey);
		if (!blob)
			return;

		/* Remove sent option mapping */
		object_t *o = NULL;
		object_t *option = object_getObject(object, "SentOption", 0, NULL);
		object_setWriteHandler(option, NULL);
		object_for_each_instance(o, option) {
			object_setWriteHandler(o, NULL);
		}
		object_setUserData(option, NULL);

		/* Remove received option mapping */
		option = object_getObject(object, "ReceivedOption", 0, NULL);
		object_setWriteHandler(option, NULL);
		object_setInstanceDelHandler(option, NULL);
		object_for_each_instance(o, option) {
			object_setWriteHandler(o, NULL);
			dhcpv6_delOption(NULL, o);
		}
		object_setUserData(option, NULL);

		plugin_setParameterWriteHandler(object_getParameter(object, "DHCPStatus"), NULL);
		plugin_setParameterReadHandler(object_getParameter(object, "Uptime"), NULL);

		intf_delFlagListener(intf, "enabled", dhcpv6_enableHandler, blob, false);
		intf_delMgmtListener(intf, NULL, dhcpv6_updateIntf, NULL, blob, false);

		nemo_closeQuery(blob->physupquery);
		blob->physupquery = NULL;
		pcb_timer_destroy(blob->physdowntimer);
		blob->physdowntimer = NULL;

		nemo_closeQuery(blob->lowerintfquery);
		blob->lowerintfquery = NULL;
		dhcpv6_llintfHandler(NULL, blob);

		map_destroy(blob->map);
		intf_delBlob(intf, dhcpv6_blobkey);

		free(blob);
	}
}

static bool dhcpv6_start(argument_value_list_t *args __attribute__((unused))) {
	sahTraceAddZone(200, ME);
	SAH_TRACEZ_INFO(ME, "Initializing dhcpv6_ng module");
	char *dsturi = NULL;
	arg_takeChar(&dsturi, args, ARG_BYNAME, "dst");
	if (dsturi && *dsturi) {
		uri_t *uri = NULL;
		dhcpv6_dst = connection_connect(pcb_connection(plugin_getPcb()), dsturi, &uri);
		if (!dhcpv6_dst) {
			SAH_TRACE_ERROR("dhcpv6 - Connection to %s failed - fallback to sysbus", dsturi);
			// no fatal error, we still have fallback over the sysbus.
		}
		if (uri)
			uri_destroy(uri);
	}
	free(dsturi);
	if (!dhcpv6_dst)
		dhcpv6_dst = plugin_getPeer();

	intf_addFlagListener(NULL, "dhcpv6", dhcpv6_rootHandler, NULL, true);
	return true;
}

static void dhcpv6_stop() {
	SAH_TRACEZ_INFO(ME, "Cleaning up dhcpv6_ng module");
	intf_delFlagListener(NULL, "dhcpv6", dhcpv6_rootHandler, NULL, true);

	if (dhcpv6_dst != plugin_getPeer()) {
		peer_destroy(dhcpv6_dst);
		dhcpv6_dst = NULL;
	}
}

static mtk_module_info_t dhcpv6_info = {
	.name  = ME,
	.start = dhcpv6_start,
	.stop  = dhcpv6_stop
};

__attribute__((constructor)) static void dhcpv6_init() {
	mtk_module_register(&dhcpv6_info);
}

