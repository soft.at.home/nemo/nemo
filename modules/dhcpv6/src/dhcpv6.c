/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "dhcpv6"

#include <nemo/core.h>

static bool dhcpv6_readRenew(parameter_t *parameter, variant_t *value) {
	(void)parameter;
	variant_setBool(value, false);
	return true;
}

static void dhcpv6_writeOption(object_t *object) {
	if (object_isTemplate(object))
		return;
	const char *alias = string_buffer(variant_da_string(object_parameterValue(object, "Alias")));
	if (!(alias && *alias)) {
		string_t s;
		string_initialize(&s, 64);
		string_appendFormat(&s, "cpe-%s", object_name(object, path_attr_key_notation));
		object_parameterSetStringValue(object, "Alias", &s);
		object_commit(object);
		string_cleanup(&s);
		return;
	}
	unsigned char tag = object_parameterUInt8Value(object, "Tag");
	int key = atoi(object_name(object, path_attr_key_notation));
	if (!tag && key > 0 && key < 0xffff) {
		object_parameterSetUInt8Value(object, "Tag", key);
		object_commit(object);
		return;
	}
}

static void dhcpv6_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata; (void)flag;
	object_t *object = plugin_intf2object(intf);
	
	if (value) {
		plugin_setParameterReadHandler(object_getParameter(object, "Renew"), dhcpv6_readRenew);
		object_t *option = object_getObject(object, "SentOption", 0, NULL);
		object_setWriteHandler(option, dhcpv6_writeOption);
		object_for_each_instance(option, option) {
			object_setWriteHandler(option, dhcpv6_writeOption);
			dhcpv6_writeOption(option);
		}
	} else {
		object_t *option = object_getObject(object, "SentOption", 0, NULL);
		object_setWriteHandler(option, NULL);
		object_for_each_instance(option, option) {
			object_setWriteHandler(option, NULL);
		}
		plugin_setParameterReadHandler(object_getParameter(object, "Renew"), NULL);
	}
}

static bool dhcpv6_start(argument_value_list_t *args __attribute__((unused))) {
	SAH_TRACEZ_INFO(ME, "Initializing dhcpv6 module");
	intf_addFlagListener(NULL, "dhcpv6", dhcpv6_rootHandler, NULL, true);	
	return true;
}

static void dhcpv6_stop() {
	SAH_TRACEZ_INFO(ME, "Cleaning up dhcpv6 module");
	intf_delFlagListener(NULL, "dhcpv6", dhcpv6_rootHandler, NULL, true);
}

static mtk_module_info_t dhcpv6_info = {
	.name  = ME,
	.start = dhcpv6_start,
	.stop  = dhcpv6_stop
};

__attribute__((constructor)) static void dhcpv6_init() {
	mtk_module_register(&dhcpv6_info);
}

