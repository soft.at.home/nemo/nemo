/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>
#define ME "dhcp"

#include <nemo/core.h>

struct _intf_blob {
	map_t *map;
	bool enabled;
	bool dstenabled;
	char *lowerintf;
	int32_t uptime;
	int32_t boundtime;
	nemo_query_t *lowerintfquery;
	nemo_query_t *physupquery;
	pcb_timer_t *physdowntimer;
	bool physup;
	intf_t *intf;
};

static int dhcp_dummy;
static void *dhcp_blobkey = &dhcp_dummy;

static peer_info_t *dhcp_dst = NULL;

static long dhcp_uptime() {
	char buf[64], *ptr = NULL;
	FILE *f = fopen("/proc/uptime", "r"); // Remark using sysinfo() would be easier but it's not available in uclibc
	if (f) {
		ptr = fgets(buf, 64, f);
		fclose(f);
	}
	return ptr?strtol(ptr, NULL, 0):0;
}

static void dhcp_check(intf_blob_t *blob) {
	pcb_timer_state_t timerstate = pcb_timer_getState(blob->physdowntimer);
	bool timer = timerstate == pcb_timer_running || timerstate == pcb_timer_started;
	bool dstenabled = blob->enabled && blob->lowerintf && (blob->physup || (blob->dstenabled && timer));
	if (dstenabled == blob->dstenabled)
		return;
	SAH_TRACEZ_INFO(ME, "request_create_setObject(path=%s)", map_dstpath(blob->map));
	request_t *req = request_create_setObject(map_dstpath(blob->map), request_common_path_key_notation);
	variant_t var;
	variant_initialize(&var, variant_type_unknown);
	variant_setBool(&var, dstenabled);
	SAH_TRACEZ_INFO(ME, "request_addParameter(path=%s param=%s value=%s)", map_dstpath(blob->map), "Enable", dstenabled?"true":"false");
	request_addParameter(req, "Enable", &var);
	SAH_TRACEZ_INFO(ME, "pcb_sendRequest(path=%s)", map_dstpath(blob->map));
	if (!pcb_sendRequest(plugin_getPcb(), dhcp_dst, req)) {
		SAH_TRACE_ERROR("dhcp - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
	}
	request_destroy(req);
	variant_cleanup(&var);
	blob->dstenabled = dstenabled;
}

static void dhcp_llintfHandler(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	free(blob->lowerintf);
	blob->lowerintf = result?variant_char(result):NULL;
	if (blob->lowerintf && !*blob->lowerintf) {
		free(blob->lowerintf);
		blob->lowerintf = NULL;
	}
	if (blob->lowerintf) {
		SAH_TRACEZ_INFO(ME, "request_create_setObject(path=%s)", map_dstpath(blob->map));
		request_t *req = request_create_setObject(map_dstpath(blob->map), request_common_path_key_notation);
		variant_t var;
		variant_initialize(&var, variant_type_unknown);
		variant_setChar(&var, blob->lowerintf);
		SAH_TRACEZ_INFO(ME, "request_addParameter(path=%s param=%s value=%s)", map_dstpath(blob->map), "IPIntf", blob->lowerintf);
		request_addParameter(req, "IPIntf", &var);
		SAH_TRACEZ_INFO(ME, "pcb_sendRequest(path=%s)", map_dstpath(blob->map));
		if (!pcb_sendRequest(plugin_getPcb(), dhcp_dst, req)) {
			SAH_TRACE_ERROR("dhcp - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
		}
		request_destroy(req);
	}
	dhcp_check(blob);
}

static void dhcp_enableHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	if (!strcmp(flag, "enabled")) {
		blob->enabled = value;
		dhcp_check(blob);
	}
}

static void dhcp_physupHandler(const variant_t *result, void *userdata) {
	intf_blob_t *blob = (intf_blob_t *)userdata;
	bool physup = variant_bool(result);
	int32_t timeout = object_parameterInt32Value(plugin_intf2object(blob->intf), "ResetOnPhysDownTimeout");
	if (blob->physup && timeout < 0)
		return;
	blob->physup = physup;
	if (!blob->physup && timeout > 0) {
		pcb_timer_start(blob->physdowntimer, timeout * 1000);
	} else {
		pcb_timer_stop(blob->physdowntimer);
		dhcp_check(blob);
	}
}

static void dhcp_physdownTimeout(pcb_timer_t *timer, void *userdata) {
	(void)timer;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	dhcp_check(blob);
}

static void dhcp_updateIntf(intf_t *intf, void *userdata) {
	(void)intf;
	intf_blob_t *blob = (intf_blob_t *)userdata;
	map_commit(blob->map);
}

static bool dhcp_readRenew(parameter_t *parameter, variant_t *value) {
	(void)parameter;
	variant_setBool(value, false);
	return true;
}

static bool dhcp_readLeaseTimeRemaining(parameter_t *parameter, variant_t *value) {
	intf_t *intf = plugin_object2intf(parameter_owner(parameter));
	if (!flagset_check(intf_flagset(intf), "up")) {
		variant_setInt32(value, 0);
		return true;
	}
	intf_blob_t *blob = intf_getBlob(intf, dhcp_blobkey);
	int32_t leasetime = object_parameterInt32Value(parameter_owner(parameter), "LeaseTime");
	int32_t now = dhcp_uptime();
	if (leasetime < 0)
		variant_setUInt32(value, leasetime);
	else if (blob->boundtime + leasetime > now)
		variant_setInt32(value, blob->boundtime + leasetime - now);
	else
		variant_setUInt32(value, 0);
	return true;
}

static bool dhcp_readUptime(parameter_t *parameter, variant_t *value) {
	intf_t *intf = plugin_object2intf(parameter_owner(parameter));
	if (!flagset_check(intf_flagset(intf), "up")) {
		variant_setInt32(value, 0);
		return true;
	}
	intf_blob_t *blob = intf_getBlob(intf, dhcp_blobkey);
	int32_t now = dhcp_uptime();
	variant_setUInt32(value, (uint32_t)(now - blob->uptime));
	return true;
}

static void dhcp_writeDHCPStatus(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	intf_t *intf = plugin_object2intf(parameter_owner(parameter));
	intf_blob_t *blob = intf_getBlob(intf, dhcp_blobkey);
	const char *value = string_buffer(variant_da_string(parameter_getValue(parameter)));
	if (!value)
		value = "";
	if (!strcmp(value, "Bound")) {
		long t = dhcp_uptime();
		blob->boundtime = t;
		if (!flagset_check(intf_flagset(intf), "up"))
			blob->uptime = t;
	}
	if (!strcmp(value, "Bound") || !strcmp(value, "Renewing") || !strcmp(value, "Rebinding")) {
		flagset_set(intf_flagset(intf), "up");
	} else if (flagset_check(intf_flagset(intf), "up")) {
		flagset_clear(intf_flagset(intf), "up");
	}
}

static void dhcp_writeOption(object_t *object) {
	if (object_isTemplate(object))
		return;
	map_t *map = (map_t *)object_getUserData(object);
	if (map && map_busy(map))
		return;
	const char *alias = string_buffer(variant_da_string(object_parameterValue(object, "Alias")));
	if (!(alias && *alias)) {
		string_t s;
		string_initialize(&s, 64);
		string_appendFormat(&s, "cpe-%s", object_name(object, path_attr_key_notation));
		object_parameterSetStringValue(object, "Alias", &s);
		object_commit(object);
		string_cleanup(&s);
		return;
	}
	unsigned char tag = object_parameterUInt8Value(object, "Tag");
	int key = atoi(object_name(object, path_attr_key_notation));
	if (!tag && key > 0 && key < 256) {
		object_parameterSetUInt8Value(object, "Tag", key);
		object_commit(object);
		return;
	}
	if (!map) {
		object_t *parent = object_parent(object);
 		intf_blob_t *blob = (intf_blob_t *)object_getUserData(parent);
		int valueattr = !strcmp(object_name(parent, 0), "SentOption") ? MAP_WRITEONLY : MAP_READONLY;
		map = map_create(object, MAP_OWNER, dhcp_dst, "DHCPv4.Client.Intf.%s.%s.%u", intf_name(blob->intf),
					valueattr == MAP_WRITEONLY ? "SentOption" : "ReqOption", object_parameterUInt8Value(object, "Tag"));
		map_parameter_create(map, object_getParameter(object, "Enable"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(map, object_getParameter(object, "Tag"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(map, object_getParameter(object, "Value"), NULL, valueattr, NULL, NULL);
		object_setUserData(object, map);
	}
	map_commit(map);
}

static bool dhcp_delOption(object_t *object, object_t *instance) {
	(void)object;
	map_t *map = (map_t *)object_getUserData(instance);
	if (map) {
		map_destroy(map);
		object_setUserData(instance, NULL);
	}
	return true;
}

static function_exec_state_t dhcp_renew(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	int numberOfRenew = -1;
	int delay = -1;
	bool ok = false;

	uint32_t attr = request_attributes(fcall_request(fcall));

	argument_getInt32(&numberOfRenew, args, attr, "numberOfRenew", -1);
	argument_getInt32(&delay, args, attr, "delay", -1);

	intf_t *intf = plugin_object2intf(fcall_object(fcall));
	if (!flagset_check(intf_flagset(intf), "up")) {
		reply_addError(request_reply(fcall_request(fcall)), pcb_error_wrong_state, "DHCP client not up", "");
		goto leave;
	}
	intf_blob_t *blob = intf_getBlob(intf, dhcp_blobkey);
	SAH_TRACE_WARNING("dhcp - %s.renew()", map_dstpath(blob->map));
	request_t *request = request_create_executeFunction(map_dstpath(blob->map), "renew", request_common_path_key_notation);
	if (request) {
		variant_t val;
		if (numberOfRenew > 0) {
			variant_initialize(&val, variant_type_unknown);
			variant_setInt32(&val, numberOfRenew);
			request_addParameter(request, "numberOfRenew", &val);
		}
		if (delay > 0) {
			variant_initialize(&val, variant_type_unknown);
			variant_setInt32(&val, delay);
			request_addParameter(request, "delay", &val);
		}

		if (!pcb_sendRequest(plugin_getPcb(), dhcp_dst, request)) {
			SAH_TRACE_ERROR("dhcp - Failed to send request: renew()");
		}
		request_destroy(request);
	}
	else {
		SAH_TRACE_ERROR("dhcp - renew() request could not be created.");
	}

	ok = true;
leave:
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

static function_exec_state_t dhcp_rebind(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	int numberOfRebind = -1;
	int delay = -1;

	uint32_t attr = request_attributes(fcall_request(fcall));

	argument_getInt32(&numberOfRebind, args, attr, "numberOfRebind", -1);
	argument_getInt32(&delay, args, attr, "delay", -1);

	intf_t *intf = plugin_object2intf(fcall_object(fcall));
	if (!flagset_check(intf_flagset(intf), "up")) {
		reply_addError(request_reply(fcall_request(fcall)), pcb_error_wrong_state, "DHCP client not up", "");
		goto leave;
	}
	intf_blob_t *blob = intf_getBlob(intf, dhcp_blobkey);
	SAH_TRACE_WARNING("dhcp - %s.rebind()", map_dstpath(blob->map));
	request_t *request = request_create_executeFunction(map_dstpath(blob->map), "rebind", request_common_path_key_notation);
	if (request) {
		variant_t val;
		if (numberOfRebind > 0) {
			variant_initialize(&val, variant_type_unknown);
			variant_setInt32(&val, numberOfRebind);
			request_addParameter(request, "numberOfRebind", &val);
		}
		if (delay > 0) {
			variant_initialize(&val, variant_type_unknown);
			variant_setInt32(&val, delay);
			request_addParameter(request, "delay", &val);
		}
		if (!pcb_sendRequest(plugin_getPcb(), dhcp_dst, request)) {
			SAH_TRACE_ERROR("dhcp - Failed to send request: rebind()");
		}
		request_destroy(request);
	}

	else {
		SAH_TRACE_ERROR("dhcp - rebind() request could not be created.");
	}
	ok = true;
leave:
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

static void dhcp_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata; (void)flag;
	static const char *templates[2] = {"SentOption", "ReqOption"};
	object_t *object = plugin_intf2object(intf);

	if (value) {
		intf_blob_t *blob = calloc(1, sizeof(intf_blob_t));
		if (!blob) {
			SAH_TRACE_ERROR("dhcp - calloc failed");
			return;
		}
		intf_addBlob(intf, dhcp_blobkey, blob);
		blob->intf = intf;

		blob->map = map_create(object, MAP_OWNER, dhcp_dst, "DHCPv4.Client.Intf.%s", intf_name(intf));
		map_parameter_create(blob->map, object_getParameter(object, "Name"), NULL, MAP_WRITEONLY | MAP_KEY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "DHCPStatus"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LastConnectionError"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "IPAddress"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "SubnetMask"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Renew"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "DSCPMark"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "PriorityMark"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "Formal"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "BroadcastFlag"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "CheckAuthentication"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "AuthenticationInformation"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "RetransmissionStrategy"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "RetransmissionRenewTimeout"), NULL, MAP_WRITEONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "IPRouters"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "DNSServers"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "LeaseTime"), NULL, MAP_READONLY, NULL, NULL);
		map_parameter_create(blob->map, object_getParameter(object, "DHCPServer"), NULL, MAP_READONLY, NULL, NULL);
		map_commit(blob->map);

		plugin_setParameterWriteHandler(object_getParameter(object, "DHCPStatus"), dhcp_writeDHCPStatus);
		plugin_setParameterReadHandler(object_getParameter(object, "LeaseTimeRemaining"), dhcp_readLeaseTimeRemaining);
		plugin_setParameterReadHandler(object_getParameter(object, "Uptime"), dhcp_readUptime);
		plugin_setParameterReadHandler(object_getParameter(object, "Renew"), dhcp_readRenew);
		function_setHandler(object_getFunction(object, "renew"), dhcp_renew);
		function_setHandler(object_getFunction(object, "rebind"), dhcp_rebind);

		intf_addFlagListener(intf, "enabled", dhcp_enableHandler, blob, true);
		intf_addMgmtListener(intf, NULL, dhcp_updateIntf, NULL, blob, false);

		blob->lowerintfquery = nemo_openQuery_luckyIntf(intf_name(intf), ME, "netdev-up", "one level down", dhcp_llintfHandler, blob);

		blob->physdowntimer = pcb_timer_create();
		pcb_timer_setHandler(blob->physdowntimer, dhcp_physdownTimeout);
		pcb_timer_setUserData(blob->physdowntimer, blob);
		blob->physupquery = nemo_openQuery_isUp(intf_name(intf), ME, NULL, "down exclusive", dhcp_physupHandler, blob);

		int i;
		for (i = 0; i < 2; i++) {
			object_t *option = object_getObject(object, templates[i], 0, NULL);
			object_t *o = NULL;
			object_setUserData(option, blob);
			object_setWriteHandler(option, dhcp_writeOption);
			object_setInstanceDelHandler(option, dhcp_delOption);
			object_for_each_instance(o, option) {
				object_setWriteHandler(o, dhcp_writeOption);
				dhcp_writeOption(o);
			}
		}
	} else {
		intf_blob_t *blob = intf_getBlob(intf, dhcp_blobkey);
		if (!blob)
			return;

		int i;
		for (i = 0; i < 2; i++) {
			object_t *option = object_getObject(object, templates[i], 0, NULL);
			object_t *o = NULL;
			object_setWriteHandler(option, NULL);
			object_setInstanceDelHandler(option, NULL);
			object_for_each_instance(o, option) {
				object_setWriteHandler(o, NULL);
				dhcp_delOption(NULL, o);
			}
			object_setUserData(option, NULL);
		}

		plugin_setParameterWriteHandler(object_getParameter(object, "DHCPStatus"), NULL);
		plugin_setParameterReadHandler(object_getParameter(object, "LeaseTimeRemaining"), NULL);
		plugin_setParameterReadHandler(object_getParameter(object, "Uptime"), NULL);
		plugin_setParameterReadHandler(object_getParameter(object, "Renew"), NULL);
		function_setHandler(object_getFunction(object, "renew"), NULL);
		function_setHandler(object_getFunction(object, "rebind"), NULL);

		intf_delFlagListener(intf, "enabled", dhcp_enableHandler, blob, false);
		intf_delMgmtListener(intf, NULL, dhcp_updateIntf, NULL, blob, false);

		nemo_closeQuery(blob->physupquery);
		pcb_timer_destroy(blob->physdowntimer);

		nemo_closeQuery(blob->lowerintfquery);
		dhcp_llintfHandler(NULL, blob);

		map_destroy(blob->map);

		intf_delBlob(intf, dhcp_blobkey);

		free(blob);
	}
}

static bool dhcp_start(argument_value_list_t *args) {
	SAH_TRACEZ_INFO(ME, "Initializing dhcp module");
	char *dsturi = NULL;
	arg_takeChar(&dsturi, args, ARG_BYNAME, "dst");
	if (dsturi && *dsturi) {
		uri_t *uri = NULL;
		dhcp_dst = connection_connect(pcb_connection(plugin_getPcb()), dsturi, &uri);
		if (!dhcp_dst) {
			SAH_TRACE_ERROR("dhcp - Connection to %s failed - fallback to sysbus", dsturi);
			// no fatal error, we still have fallback over the sysbus.
		}
		if (uri)
			uri_destroy(uri);
	}
	free(dsturi);
	if (!dhcp_dst)
		dhcp_dst = plugin_getPeer();
	intf_addFlagListener(NULL, "dhcp", dhcp_rootHandler, NULL, true);
	return true;
}

static void dhcp_stop() {
	SAH_TRACEZ_INFO(ME, "Cleaning up dhcp module");
	intf_delFlagListener(NULL, "dhcp", dhcp_rootHandler, NULL, true);
	if (dhcp_dst != plugin_getPeer())
		peer_destroy(dhcp_dst);
}

static mtk_module_info_t dhcp_info = {
	.name  = ME,
	.start = dhcp_start,
	.stop  = dhcp_stop
};

__attribute__((constructor)) static void dhcp_init() {
	mtk_module_register(&dhcp_info);
}

